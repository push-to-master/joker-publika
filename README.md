# Joker publika

## Deployed
  * Test: [Heroku](https://jokerpublika.herokuapp.com/jokerpublika/)

## Documentation:
  * [v1.0](doc/OPP_2019_PushToMaster_v1_0.pdf) (2019-11-15)
  * [v2.0](doc/OPP_2019_PushToMaster_v2_0.pdf) (2020-01-17)
