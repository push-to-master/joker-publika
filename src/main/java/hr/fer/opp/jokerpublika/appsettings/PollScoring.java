package hr.fer.opp.jokerpublika.appsettings;

import hr.fer.opp.jokerpublika.model.Poll;

public class PollScoring {

  private static final double durationCoefficient = 0.25;

  public static double getPollScore(Poll poll) {
    double score = poll.getAnswer1().getVoteCount() + poll.getAnswer2().getVoteCount();
    long remainingDuration = poll.getTimeout().getTime() - System.currentTimeMillis();
    remainingDuration /= 60_000; // Remaining duration in minutes
    score += remainingDuration * durationCoefficient;
    return score;
  }
}
