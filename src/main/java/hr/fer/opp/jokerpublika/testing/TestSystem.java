package hr.fer.opp.jokerpublika.testing;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("DuplicatedCode")
public class TestSystem {

  public static final String TEST_EMAIL = "test@fer.hr";

  public static final String TEST_USERNAME = "test";

  public static final String TEST_PASSWORD = "testpassword";

  public static final int DEFAULT_TIMEOUT_TIME = 5;

  public final String baseUrl = "https://jokerpublika.herokuapp.com/jokerpublika/login";

  public final String dropdownWithSettings = "/html[1]/body[1]/div[1]/div[1]/div[1]/nav[1]/div[3]/div[1]/a[1]/span[1]";

  public WebDriver driver;

  @BeforeClass
  public void setup() {
    System.setProperty("webdriver.chrome.driver", "src/main/resources/testing/chromedriver.exe");
    System.setProperty("phantomjs.binary.path", "src/main/resources/testing/phantomjs.exe");
    System.setProperty("webdriver.opera.driver", "src/main/resources/testing/operadriver.exe");
  }

  @BeforeMethod
  public void beforeEach() {
    this.driver = new ChromeDriver();
    this.driver.manage().timeouts().implicitlyWait(DEFAULT_TIMEOUT_TIME, TimeUnit.SECONDS);
    this.driver.get(this.baseUrl);
  }

  /**
   * Metoda provjerava funkciju prijave na stranicu
   */
  @Test
  public void testLogin() {
    this.logMeIn(TEST_USERNAME, TEST_PASSWORD);
    //span[@class='mr-3 d-none d-sm-inline text-white font-italic']
    System.out.println(this.driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/nav[1]/div[3]/div[1]/a[1]/span[1]")).getText());
Assert.assertEquals(this.driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/nav[1]/div[3]/div[1]/a[1]/span[1]")).getText(),TEST_USERNAME);
    System.out.println(this.driver.findElement(By.cssSelector("div.content-wrapper.d-flex.flex-column.vh-100 div.content nav.shadow.bg-gradient-violet.navbar.navbar-expand.navbar-light div:nth-child(3) div.no-arrow.dropdown a.nav-link.px-0.dropdown-toggle > span.mr-3.d-none.d-sm-inline.text-white.font-italic")).getText());
    Assert.assertEquals(this.driver.findElement(By.cssSelector("div.content-wrapper.d-flex.flex-column.vh-100 div.content nav.shadow.bg-gradient-violet.navbar.navbar-expand.navbar-light div:nth-child(3) div.no-arrow.dropdown a.nav-link.px-0.dropdown-toggle > span.mr-3.d-none.d-sm-inline.text-white.font-italic")).getText(),TEST_USERNAME);
    System.out.println(this.driver.findElement(By.cssSelector("a[role='none'] > .d-none.d-sm-inline.font-italic.mr-3.text-white")).getText());
    Assert.assertEquals(this.driver.findElement(By.cssSelector("a[role='none'] > .d-none.d-sm-inline.font-italic.mr-3.text-white")).getText(),TEST_USERNAME);
    System.out.println(this.driver.findElement(By.cssSelector("[class='mr-3 d-none d-sm-inline text-white font-italic']")).getText());
    Assert.assertEquals(this.driver.findElement(By.cssSelector("[class='mr-3 d-none d-sm-inline text-white font-italic']")).getText(),TEST_USERNAME);
    System.out.println(this.driver.findElement(By.xpath("/html//div[@id='react']//nav//a[@role='none']/span[.='"+TEST_USERNAME+"']")).getText());
    Assert.assertEquals(this.driver.findElement(By.xpath("/html//div[@id='react']//nav//a[@role='none']/span[.='"+TEST_USERNAME+"']")).getText(),TEST_USERNAME);
    //prikazan je element u kojem piše ime prijavlenog korisnika i u tom elementu je ime prijavljenog korisnika
    /*Assert.assertTrue(this.driver.findElement(By.xpath("//div[@id='react']/div[@class='content-wrapper d-flex " +
                                                        "flex-columnvh-100']//nav//a[@role='none']/span[.='"+TEST_USERNAME+"']")).isDisplayed
                                                        ());*/
    
//    Assert.assertEquals(this.driver.findElement(By.id("username")).getText(), TEST_USERNAME);
  }

  /**
   * Pomoćna metoda za prijavu korisnika s danim korisničkim imenom i lozinkom
   * @param username korisničko ime
   * @param password zaporka
   */
  private void logMeIn(String username, String password) {
    this.driver.get("https://jokerpublika.herokuapp.com/jokerpublika/login");
    this.driver.findElement(By.id("username")).sendKeys(username);
    this.driver.findElement(By.id("password")).sendKeys(password);
    this.driver.findElement(By.xpath("//div[@id='react']//form/button[@type='submit']/span[@class='MuiButton-label']")).click();
  }

  @Test
  public void registerAndLogin() {
    this.driver.findElement(By.linkText("Register")).click();
    GenerateRandomUser newRandomUser = new GenerateRandomUser().get();
    String randomUsername = newRandomUser.getRandomUsername();
    String email = newRandomUser.getEmail();
    String randomPassword = newRandomUser.getRandomPassword();
    System.out.println("user: " + randomUsername + " email: " + email + " random password: " + randomPassword);
    //REGISTRACIJA
    this.registerMe(randomUsername, email, randomPassword);

    //LOGIN ; brisanje kolačića jer nas stranica automatski ulogira
    this.driver.manage().deleteAllCookies();
    this.driver.navigate().refresh();
    this.logMeIn(randomUsername, randomPassword);

    //    new WebDriverWait(this.driver, 15)
    //        .until(ExpectedConditions.visibilityOfElementLocated(
    //            By.xpath(this.dropdownWithSettings)));

    Assert.assertEquals(this.driver.findElement(By.xpath(this.dropdownWithSettings)).getText(),
        randomUsername);
  }

  //pretpostavka => nalazimo se na stranici register
  private void registerMe(String randomUsername, String email, String randomPassword) {
    this.driver.findElement(By.id("username")).sendKeys(randomUsername);
    this.driver.findElement(By.id("email")).sendKeys(email);
    this.driver.findElement(By.id("password")).sendKeys(randomPassword);
    this.driver.findElement(By.id("confirm-password")).sendKeys(randomPassword);
    this.driver.findElement(By.xpath("//div[@id='react']//form/button[@type='submit']/span[@class='MuiButton-label']")).click();
  }

  /**
   * Provjera je li se moguće ulogirati s deaktiviranim računom
   */
  @Test
  public void deactivate() {
    this.driver.findElement(By.linkText("Register")).click();
    GenerateRandomUser newRandomUser = new GenerateRandomUser().get();
    String randomUsername = newRandomUser.getRandomUsername();
    String email = newRandomUser.getEmail();
    String randomPassword = newRandomUser.getRandomPassword();
    System.out.println("user: " + randomUsername + " email: " + email + " random password: " + randomPassword);
    //REGISTRACIJA
    this.registerMe(randomUsername, email, randomPassword);

    //LOGIN
    this.driver.get("https://jokerpublika.herokuapp.com/jokerpublika/login");
    this.logMeIn(randomUsername, randomPassword);

    //DEAKTIVACIJA
    //klik da se pokaže menu

    this.driver.findElement(By.xpath(this.dropdownWithSettings)).click();
    this.driver.findElement(By.xpath("//span[contains(text(),'Settings')]")).click();
    this.driver.findElement(By.xpath("//button[contains(text(),'DEACTIVATE')]")).click();

    //PONOVNI POKUSAJ LOGIRANJA
    this.driver.get("https://jokerpublika.herokuapp.com/jokerpublika/login");
    this.logMeIn(randomUsername, randomPassword);


    //POJAVILA SE GRESKA
    Assert.assertTrue(this.driver.findElement(By.xpath("//div[@class='alert alert-danger custom-login-alert']")).isDisplayed());
  }

  /**
   * Provjera nalazi se zapravo pitanje na stranici dostupnih pitanja kada ju mi objavimo
   */
  @Test
  public void postPoll() {
    this.logMeIn(TEST_USERNAME, TEST_PASSWORD);
    //stisni sidebar
    this.driver.findElement(By.xpath("//div[@id='react']//nav/div[@class='d-flex']")).click();
    this.driver.findElement(By.linkText("New Poll")).click();
    this.driver.findElement(By.xpath("/html//textarea[@id='question']")).sendKeys("Blue or red scarf?");
    this.driver.findElement(By.cssSelector("#answer1")).sendKeys("Blue");
    this.driver.findElement(By.cssSelector("#answer2")).sendKeys("Red");
    this.driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[5]/div[1]/button[1]")).click();
    this.driver.findElement(By.xpath("//div[@id='react']//nav/div[@class='d-flex']")).click();
    this.driver.findElement(By.xpath("//span[contains(text(),'My Active Polls')]")).click();
    //TODO actually provjeriti da postoji
    try {
      Thread.sleep(5000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  /**
   * Nakon kraja svakog testa zatvara web preglednik.
   */
  @AfterMethod
  public void endIt() {
    this.driver.quit();
  }

  private static class GenerateRandomUser {
    private String randomUsername;

    private String email;

    private String randomPassword;

    public String getRandomUsername() {
      return this.randomUsername;
    }

    public String getEmail() {
      return this.email;
    }

    public String getRandomPassword() {
      return this.randomPassword;
    }

    public GenerateRandomUser get() {
      this.randomUsername = "user" + (long) (Math.random() * 1000000000000000000L);
      this.email = this.randomUsername + "@email.com";
      this.randomPassword = UUID.randomUUID().toString();
      return this;
    }
  }
}
