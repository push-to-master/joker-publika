package hr.fer.opp.jokerpublika;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JokerPublikaApplication {

  public static void main(String[] args) {
    SpringApplication.run(JokerPublikaApplication.class, args);
  }
}
