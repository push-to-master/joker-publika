package hr.fer.opp.jokerpublika.model;


import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Objects;

@Entity
public class FriendGroup {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotEmpty
  @Size(max = 128)
  private String name;

  @ManyToOne
  @NotNull
  private User owner;

  @ManyToMany
  @JoinTable(
      name = "FRIEND_GROUP_USERS",
      joinColumns = @JoinColumn(name = "FRIEND_GROUP_ID"),
      inverseJoinColumns = @JoinColumn(name = "USER_ID")
  )
  @Size(min = 2)
  private List<User> users;

  public FriendGroup() {
  }

  public FriendGroup(String name, User owner) {
    this.name = name;
    this.owner = owner;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public User getOwner() {
    return owner;
  }

  public void removeUser(User user) {
    this.users.remove(user);
  }

  public void setOwner(User owner) {
    this.owner = owner;
  }

  public List<User> getUsers() {
    return users;
  }

  public void setUsers(List<User> users) {
    this.users = users;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    FriendGroup that = (FriendGroup) o;
    return id.equals(that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
