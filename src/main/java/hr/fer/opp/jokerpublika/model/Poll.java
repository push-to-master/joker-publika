package hr.fer.opp.jokerpublika.model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.List;

@Entity
public class Poll {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne
  @NotNull
  private User user;

  @NotEmpty
  @Size(max = 512)
  private String question;

  @OneToOne(cascade = CascadeType.ALL)
  @NotNull
  private Answer answer1;

  @OneToOne(cascade = CascadeType.ALL)
  @NotNull
  private Answer answer2;

  @NotNull
  private Timestamp postTime = new Timestamp(System.currentTimeMillis());

  @NotNull
  private Timestamp timeout;

  @Enumerated(EnumType.STRING)
  @NotNull
  private PollTypeEnum pollType;

  @Lob
  private String image1;

  @Lob
  private String image2;

  @ManyToOne
  private FriendGroup friendGroup;

  @OneToMany(mappedBy = "poll")
  private List<AnswerHistory> answerHistory;

  @NotNull
  private Boolean deleted = false;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public String getQuestion() {
    return question;
  }

  public void setQuestion(String question) {
    this.question = question;
  }

  public Answer getAnswer1() {
    return answer1;
  }

  public void setAnswer1(Answer answer1) {
    this.answer1 = answer1;
  }

  public Answer getAnswer2() {
    return answer2;
  }

  public void setAnswer2(Answer answer2) {
    this.answer2 = answer2;
  }

  public Timestamp getPostTime() {
    return postTime;
  }

  public void setPostTime(Timestamp postTime) {
    this.postTime = postTime;
  }

  public Timestamp getTimeout() {
    return timeout;
  }

  public void setTimeout(Timestamp timeout) {
    this.timeout = timeout;
  }

  public PollTypeEnum getPollType() {
    return pollType;
  }

  public void setPollType(PollTypeEnum pollType) {
    this.pollType = pollType;
  }

  public String getImage1() {
    return image1;
  }

  public void setImage1(String image1) {
    this.image1 = image1;
  }

  public String getImage2() {
    return image2;
  }

  public void setImage2(String image2) {
    this.image2 = image2;
  }

  public FriendGroup getFriendGroup() {
    return friendGroup;
  }

  public void setFriendGroup(FriendGroup friendGroup) {
    this.friendGroup = friendGroup;
  }

  public List<AnswerHistory> getAnswerHistory() {
    return answerHistory;
  }

  public void setAnswerHistory(List<AnswerHistory> answerHistory) {
    this.answerHistory = answerHistory;
  }

  public Boolean getDeleted() {
    return deleted;
  }

  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }

  public boolean vote1() {
    return vote(answer1);
  }

  public boolean vote2() {
    return vote(answer2);
  }

  private boolean vote(Answer answer) {
    // Adds a fifteen second window after timeout to account for unfortunate delays
    if (timeout.after(new Timestamp(System.currentTimeMillis() - 15_000))) {
      answer.addVote();
      return true;
    }
    return false;
  }
}
