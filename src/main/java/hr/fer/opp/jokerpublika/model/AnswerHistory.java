package hr.fer.opp.jokerpublika.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Entity
public class AnswerHistory {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne
  @NotNull
  private User user;

  @ManyToOne
  @NotNull
  private Poll poll;

  @ManyToOne
  private Answer answer;

  @NotNull
  private Timestamp answerTime = new Timestamp(System.currentTimeMillis());

  public AnswerHistory() {
  }

  public AnswerHistory(User user, Poll poll, Answer answer) {
    this.user = user;
    this.poll = poll;
    this.answer = answer;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Poll getPoll() {
    return poll;
  }

  public void setPoll(Poll poll) {
    this.poll = poll;
  }

  public Answer getAnswer() {
    return answer;
  }

  public void setAnswer(Answer answer) {
    this.answer = answer;
  }

  public Timestamp getAnswerTime() {
    return answerTime;
  }

  public void setAnswerTime(Timestamp answerTime) {
    this.answerTime = answerTime;
  }
}
