package hr.fer.opp.jokerpublika.model.util;

import hr.fer.opp.jokerpublika.model.*;
import hr.fer.opp.jokerpublika.repositories.FriendGroupRepository;
import hr.fer.opp.jokerpublika.repositories.PollDurationRepository;
import hr.fer.opp.jokerpublika.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DatabaseLoader implements CommandLineRunner {

  private final Logger log = LoggerFactory.getLogger(DatabaseLoader.class);

  private final PasswordEncoder encoder;

  private final UserRepository userRepository;

  private FriendGroupRepository friendGroupRepository;

  private PollDurationRepository intervalRepository;

  public DatabaseLoader(PasswordEncoder encoder,
                        UserRepository userRepository,
                        FriendGroupRepository friendGroupRepository,
                        PollDurationRepository intervalRepository) {
    this.encoder = encoder;
    this.userRepository = userRepository;
    this.friendGroupRepository = friendGroupRepository;
    this.intervalRepository = intervalRepository;
  }

  @Override
  public void run(String... strings) {
    List<UserRoleEnum> userRole = new ArrayList<>(List.of(UserRoleEnum.USER));
    List<UserRoleEnum> adminRole = new ArrayList<>(List.of(UserRoleEnum.ADMIN));

    try {
      User iskoric = new User("iskoric", "ivan.skoric@fer.hr",
          encoder.encode("pass"), false, userRole);
      User psankovic = new User("psankovic", "paulo.sankovic@fer.hr",
          encoder.encode("pass"), false, userRole);
      iskoric.setDefaultPollDuration(60L * 60_000); // 1 h
      iskoric.setDefaultPollDuration(5 * 60L * 60_000); // 5 h
      psankovic.setDefaultPollType(PollTypeEnum.ANONYMOUS);
      userRepository.save(iskoric);
      userRepository.save(psankovic);
      FriendGroup pushToMaster = new FriendGroup();
      pushToMaster.setName("PushToMaster");
      pushToMaster.setOwner(iskoric);
      pushToMaster.setUsers(new ArrayList<>(List.of(iskoric, psankovic)));
      friendGroupRepository.save(pushToMaster);

      this.userRepository.save(new User("fmajetic", "filip.majetic@fer.hr", encoder.encode("pass"), false, userRole));
      this.userRepository.save(new User("admin", "admin@fer.hr", encoder.encode("pass"), false, adminRole));
      this.userRepository.save(new User("dan", "danijel.stracenski@fer.hr", encoder.encode("pass"), true, userRole));
    } catch (Exception ex) {
      log.warn("Users/groups already exist: " + ex.getMessage());
    }

    try {
      this.intervalRepository.save(new PollDuration(5L * 60_000)); // 5 min
      this.intervalRepository.save(new PollDuration(15L * 60_000)); // 15 min
      this.intervalRepository.save(new PollDuration(30L * 60_000)); // 30 min
      this.intervalRepository.save(new PollDuration(60L * 60_000)); // 1 h
      this.intervalRepository.save(new PollDuration(5 * 60L * 60_000)); // 5 h
      this.intervalRepository.save(new PollDuration(10 * 60L * 60_000)); // 10 h
      this.intervalRepository.save(new PollDuration(24 * 60L * 60_000)); // 24 h
    } catch (Exception ex) {
      log.warn("Interval already exist: " + ex.getMessage());
    }

    /*
     * PLEASE PUT NEW DATA IN SEPARATE TRY-CATCH BLOCKS WITH A PROPER LOG MESSAGE.
     */
  }
}
