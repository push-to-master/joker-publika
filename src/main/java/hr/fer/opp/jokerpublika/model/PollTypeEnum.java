package hr.fer.opp.jokerpublika.model;

public enum PollTypeEnum {
  PUBLIC,
  ANONYMOUS,
  FRIEND_GROUP
}
