package hr.fer.opp.jokerpublika.model.util;

import hr.fer.opp.jokerpublika.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UserDetails implements org.springframework.security.core.userdetails.UserDetails {

  private static final String ROLE_PREFIX = "ROLE_";

  private User user;

  public UserDetails(User user) {
    this.user = user;
  }

  public static User getCurrentUser() {
    return ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
  }

  public User getUser() {
    return user;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    List<GrantedAuthority> authorities = new ArrayList<>();
    user.getRoles().forEach(role -> authorities.add(new SimpleGrantedAuthority(ROLE_PREFIX + role)));
    return authorities;
  }

  @Override
  public String getPassword() {
    return user.getPasswordHash();
  }

  @Override
  public String getUsername() {
    return user.getUsername();
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return (user.getBanned() ? false : true);
//    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return !user.getDeactivated();
  }
}
