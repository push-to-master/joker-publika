package hr.fer.opp.jokerpublika.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * IVANE SKORIĆU, NE BRIŠI TABLE ANOTACIJU, MOLIM TE.
 * HEROKU IMA POSTGRES BAZU, HIBERNATE STAVI DEFAULT IME TABLICE "user" ŠTO SRUŠI POSTGRES.
 */
@Entity
@Table(name = "APP_USER")
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(unique = true)
  @NotEmpty
  @Size(min = 2, max = 32)
  private String username;


  @Column(unique = true)
  @NotEmpty
  @Email
  @Size(max = 256)
  private String email;

  @NotEmpty
  private String passwordHash;

  @NotNull
  private Boolean anonymous;

  @ElementCollection(targetClass = UserRoleEnum.class, fetch = FetchType.EAGER)
  @JoinTable(name = "USER_ROLES", joinColumns = @JoinColumn(name = "USER_ID"))
  @Column(name = "ROLE", nullable = false)
  @Enumerated(EnumType.STRING)
  @NotEmpty
  private List<UserRoleEnum> roles;

  @Enumerated(EnumType.STRING)
  @NotNull
  private PollTypeEnum defaultPollType = PollTypeEnum.PUBLIC;

  @NotNull
  @Positive
  private Long defaultPollDuration = 24 * 60L * 60_000;

  @NotNull
  private Boolean deactivated = false;

  @JsonIgnore
  private Timestamp currentBanStartTime;

  @JsonIgnore
  private Timestamp currentBanEndTime;

  public User() {
  }

  public User(String username, String email, String passwordHash, boolean anonymous, List<UserRoleEnum> roles) {
    this.username = username;
    this.email = email;
    this.passwordHash = passwordHash;
    this.anonymous = anonymous;
    this.roles = new ArrayList<>(roles);
    if (anonymous) {
      this.defaultPollType = PollTypeEnum.ANONYMOUS;
    }
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPasswordHash() {
    return passwordHash;
  }

  public void setPasswordHash(String passwordHash) {
    this.passwordHash = passwordHash;
  }

  public Boolean getAnonymous() {
    return anonymous;
  }

  public void setAnonymous(Boolean anonymous) {
    this.anonymous = anonymous;
  }

  public List<UserRoleEnum> getRoles() {
    return roles;
  }

  public void setRoles(List<UserRoleEnum> roles) {
    this.roles = roles;
  }

  public PollTypeEnum getDefaultPollType() {
    return defaultPollType;
  }

  public void setDefaultPollType(PollTypeEnum defaultPollType) {
    this.defaultPollType = defaultPollType;
  }

  public Long getDefaultPollDuration() {
    return defaultPollDuration;
  }

  public void setDefaultPollDuration(Long defaultPollDuration) {
    this.defaultPollDuration = defaultPollDuration;
  }

  public Boolean getDeactivated() {
    return deactivated;
  }

  public void setDeactivated(Boolean deactivated) {
    this.deactivated = deactivated;
  }

  public Timestamp getCurrentBanStartTime() {
    return currentBanStartTime;
  }

  public void setCurrentBanStartTime(Timestamp currentBanStartTime) {
    this.currentBanStartTime = currentBanStartTime;
  }

  //  needs to be public, IntelliJ false warning
  public Timestamp getCurrentBanEndTime() {
    return currentBanEndTime;
  }

  public void setCurrentBanEndTime(Timestamp currentBanEndTime) {
    this.currentBanEndTime = currentBanEndTime;
  }

  public Boolean getBanned() {
    if (getCurrentBanStartTime() == null) return false;
    if (getCurrentBanEndTime() == null) return true;
    Timestamp myTime = new Timestamp(System.currentTimeMillis());
    if (myTime.after(this.currentBanStartTime) && myTime.before(this.currentBanEndTime)) {
      return true;
    }
    return false;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    User user = (User) o;
    return Objects.equals(id, user.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
