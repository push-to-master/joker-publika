package hr.fer.opp.jokerpublika.model;

public enum UserRoleEnum {
  ADMIN,
  USER
}
