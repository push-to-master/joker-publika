package hr.fer.opp.jokerpublika.web.controllers;

import hr.fer.opp.jokerpublika.model.PollDuration;
import hr.fer.opp.jokerpublika.services.PollDurationService;
import hr.fer.opp.jokerpublika.web.dto.PollDurationDto;
import org.modelmapper.ModelMapper;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/poll/duration")
public class PollDurationController {

  private ModelMapper mapper;

  private PollDurationService pollDurationService;

  public PollDurationController(ModelMapper mapper, PollDurationService pollDurationService) {
    this.mapper = mapper;
    this.pollDurationService = pollDurationService;
  }

  @GetMapping
  public List<PollDurationDto> getAllDurations() {
    List<PollDuration> durations = pollDurationService.getAll();
    List<PollDurationDto> durationDtos = durations.stream()
        .map(duration -> mapper.map(duration, PollDurationDto.class))
        .collect(Collectors.toList());
    durationDtos.forEach(PollDurationDto::createLabel);
    return durationDtos;
  }

  @DeleteMapping
  @Secured("ROLE_ADMIN")
  public void removeDuration(@RequestParam Long duration) {
    pollDurationService.deleteByValue(duration);
  }

  @PostMapping
  @Secured("ROLE_ADMIN")
  public void addDuration(@RequestBody Long duration) {
    pollDurationService.save(new PollDuration(duration));
  }
}
