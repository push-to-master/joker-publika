package hr.fer.opp.jokerpublika.web.dto;

import hr.fer.opp.jokerpublika.model.PollTypeEnum;
import hr.fer.opp.jokerpublika.model.UserRoleEnum;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

public class UserRegistrationDto {

  @NotEmpty
  @Size(min = 2, max = 32)
  private String username;

  @NotEmpty
  @Email
  @Size(max = 255)
  private String email;

  @NotEmpty
  @Size(min = 4, max = 256)
  private String password;

  @NotEmpty
  @Size(min = 4, max = 256)
  private String confirmPassword;

  @NotNull
  private Boolean anonymous;

  private List<UserRoleEnum> roles;

  private PollTypeEnum defaultPollType;

  private Long defaultPollDuration;

  private Boolean deactivated;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getConfirmPassword() {
    return confirmPassword;
  }

  public void setConfirmPassword(String confirmPassword) {
    this.confirmPassword = confirmPassword;
  }

  public Boolean getAnonymous() {
    return anonymous;
  }

  public void setAnonymous(Boolean anonymous) {
    this.anonymous = anonymous;
  }

  public List<UserRoleEnum> getRoles() {
    return roles;
  }

  public void setRoles(List<UserRoleEnum> roles) {
    this.roles = roles;
  }

  public PollTypeEnum getDefaultPollType() {
    return defaultPollType;
  }

  public void setDefaultPollType(PollTypeEnum defaultPollType) {
    this.defaultPollType = defaultPollType;
  }

  public Long getDefaultPollDuration() {
    return defaultPollDuration;
  }

  public void setDefaultPollDuration(Long defaultPollDuration) {
    this.defaultPollDuration = defaultPollDuration;
  }

  public Boolean getDeactivated() {
    return deactivated;
  }

  public void setDeactivated(Boolean deactivated) {
    this.deactivated = deactivated;
  }

  public void addRole(UserRoleEnum role) {
    if (roles == null) {
      roles = new ArrayList<>();
    }
    roles.add(role);
  }

}
