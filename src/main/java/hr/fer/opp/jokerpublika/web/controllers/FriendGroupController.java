package hr.fer.opp.jokerpublika.web.controllers;

import hr.fer.opp.jokerpublika.model.FriendGroup;
import hr.fer.opp.jokerpublika.model.User;
import hr.fer.opp.jokerpublika.model.util.UserDetails;
import hr.fer.opp.jokerpublika.services.FriendGroupService;
import hr.fer.opp.jokerpublika.services.UserService;
import hr.fer.opp.jokerpublika.web.dto.FriendGroupDto;
import hr.fer.opp.jokerpublika.web.exceptions.AddingExistingMemberToGroupException;
import hr.fer.opp.jokerpublika.web.exceptions.AnonymousUserGroupException;
import hr.fer.opp.jokerpublika.web.exceptions.UnauthorizedGroupChangesException;
import hr.fer.opp.jokerpublika.web.exceptions.UserNotGroupMemberException;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/group")
public class FriendGroupController {

  private final Logger log = LoggerFactory.getLogger(FriendGroupController.class);

  private ModelMapper mapper;

  private FriendGroupService friendGroupService;

  private UserService userService;

  public FriendGroupController(ModelMapper mapper,
                               FriendGroupService friendGroupService,
                               UserService userService) {
    this.mapper = mapper;
    this.friendGroupService = friendGroupService;
    this.userService = userService;
  }

  @GetMapping("/user")
  public List<FriendGroupDto> getAllGroupsByUser(@RequestParam Long userId) {
    User user = userService.getById(userId);
    List<FriendGroup> groups = friendGroupService.getAllByUser(user);
    return groups.stream().map(group -> mapper.map(group, FriendGroupDto.class)).collect(Collectors.toList());
  }

  @PostMapping
  public void createFriendGroup(@RequestBody FriendGroupDto friendGroupDto) {
    FriendGroup friendGroup = mapper.map(friendGroupDto, FriendGroup.class);
    friendGroupService.save(friendGroup);
  }

  @GetMapping("/rename")
  public void deleteFriendGroup(@RequestParam Long groupId, @RequestParam String name) {
    User user = UserDetails.getCurrentUser();
    FriendGroup group = friendGroupService.getById(groupId);

    if (!user.equals(group.getOwner())) {
      UnauthorizedGroupChangesException exc = new UnauthorizedGroupChangesException();
      log.warn(String.format("%nAttempt to rename group by someone other than group owner:" +
          "%nGROUP_ID = %d", groupId), exc);
      throw exc;
    }

    group.setName(name);
    friendGroupService.save(group);
  }

  @GetMapping("/add-user")
  public void createFriendGroup(@RequestParam Long groupId, @RequestParam String username) {
    User currentUser = UserDetails.getCurrentUser();
    FriendGroup group = friendGroupService.getById(groupId);

    if (!currentUser.equals(group.getOwner())) {
      UnauthorizedGroupChangesException exc = new UnauthorizedGroupChangesException();
      log.warn(String.format("%nAttempt to change group settings by someone other than group owner:" +
          "%nGROUP_ID = %d", groupId), exc);
      throw exc;
    }

    User user = userService.getByUsername(username);
    if (user.getAnonymous()) {
      AnonymousUserGroupException exc = new AnonymousUserGroupException();
      log.warn("\nAttempt to add anonymous user to group", exc);
      throw exc;
    } else if (group.getUsers().contains(user)) {
      AddingExistingMemberToGroupException exc = new AddingExistingMemberToGroupException();
      log.warn("\nAttempt to add existing member to group", exc);
      throw exc;
    }
    group.getUsers().add(user);
    friendGroupService.save(group);
  }

  @GetMapping("/remove-user")
  public void removeUser(@RequestParam Long groupId, @RequestParam Long userId) {
    User currentUser = UserDetails.getCurrentUser();
    FriendGroup group = friendGroupService.getById(groupId);

    if (!currentUser.equals(group.getOwner())) {
      UnauthorizedGroupChangesException exc = new UnauthorizedGroupChangesException();
      log.warn(String.format("%nAttempt to remove user from group by someone other than group owner:" +
          "%nGROUP_ID = %d, USER_ID = %d", groupId, userId), exc);
      throw exc;
    }

    User user = userService.getById(userId);
    if (!group.getUsers().contains(user)) {
      UserNotGroupMemberException exc = new UserNotGroupMemberException();
      log.warn(String.format("%nAttempt to remove non-existing member from group:" +
          "%nGROUP_ID = %d, USER_ID = %d", groupId, userId), exc);
      throw exc;
    }

    if (checkEnoughRemainingMembers(group)) {
      group.getUsers().remove(user);
      friendGroupService.save(group);
    }
  }

  @GetMapping("/leave-owner")
  public void ownerLeaveGroup(@RequestParam Long groupId, @RequestParam Long newOwnerId) {
    User currentUser = UserDetails.getCurrentUser();
    FriendGroup group = friendGroupService.getById(groupId);

    if (!currentUser.equals(group.getOwner())) {
      UnauthorizedGroupChangesException exc = new UnauthorizedGroupChangesException();
      log.warn(String.format("%nAttempt to leave group as owner by someone other than group owner:" +
          "%nGROUP_ID = %d", groupId), exc);
      throw exc;
    }

    User newOwner = userService.getById(newOwnerId);
    if (!group.getUsers().contains(newOwner)) {
      UserNotGroupMemberException exc = new UserNotGroupMemberException();
      log.warn(String.format("%nAttempt to leave group as owner without selecting valid replacement:" +
          "%nGROUP_ID = %d, USER_ID = %d", groupId, newOwnerId), exc);
      throw exc;
    }

    if (checkEnoughRemainingMembers(group)) {
      group.getUsers().remove(currentUser);
      group.setOwner(newOwner);
      friendGroupService.save(group);
    }
  }

  @GetMapping("/leave")
  public void ownerLeaveGroup(@RequestParam Long groupId) {
    User user = UserDetails.getCurrentUser();
    FriendGroup group = friendGroupService.getById(groupId);

    if (!group.getUsers().contains(user)) {
      UnauthorizedGroupChangesException exc = new UnauthorizedGroupChangesException();
      log.warn(String.format("%nAttempt to leave group user is not a member of:" +
          "%nGROUP_ID = %d", groupId), exc);
      throw exc;
    }

    if (checkEnoughRemainingMembers(group)) {
      group.getUsers().remove(user);
      friendGroupService.save(group);
    }
  }

  @GetMapping("/delete")
  public void deleteFriendGroup(@RequestParam Long groupId) {
    User user = UserDetails.getCurrentUser();
    FriendGroup group = friendGroupService.getById(groupId);

    if (!user.equals(group.getOwner())) {
      UnauthorizedGroupChangesException exc = new UnauthorizedGroupChangesException();
      log.warn(String.format("%nAttempt to delete group by someone other than group owner:" +
          "%nGROUP_ID = %d", groupId), exc);
      throw exc;
    }

    friendGroupService.delete(group);
  }

  private boolean checkEnoughRemainingMembers(FriendGroup group) {
    if (group.getUsers().size() <= 2) {
      friendGroupService.delete(group);
      return false;
    }
    return true;
  }

}
