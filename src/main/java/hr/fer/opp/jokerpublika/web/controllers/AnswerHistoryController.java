package hr.fer.opp.jokerpublika.web.controllers;

import hr.fer.opp.jokerpublika.model.AnswerHistory;
import hr.fer.opp.jokerpublika.model.Poll;
import hr.fer.opp.jokerpublika.model.PollTypeEnum;
import hr.fer.opp.jokerpublika.model.User;
import hr.fer.opp.jokerpublika.model.util.UserDetails;
import hr.fer.opp.jokerpublika.services.AnswerHistoryService;
import hr.fer.opp.jokerpublika.services.PollService;
import hr.fer.opp.jokerpublika.web.exceptions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;

@RestController
@RequestMapping("/poll/answer")
public class AnswerHistoryController {

  private final Logger log = LoggerFactory.getLogger(AnswerHistoryController.class);

  private PollService pollService;

  private AnswerHistoryService answerHistoryService;

  public AnswerHistoryController(PollService pollService, AnswerHistoryService answerHistoryService) {
    this.pollService = pollService;
    this.answerHistoryService = answerHistoryService;
  }

  @GetMapping
  public void recordAnswer(@RequestParam Long pollId, @RequestParam Long answerId) {
    User user = UserDetails.getCurrentUser();
    Poll poll = pollService.getById(pollId);

    if (user.equals(poll.getUser())) {
      AnsweringOwnPollException exc = new AnsweringOwnPollException();
      log.warn(String.format("%nAttempt to answer own poll:" +
          "%nPOLL_ID = %d", pollId), exc);
      throw exc;
    } else if (poll.getPollType() == PollTypeEnum.FRIEND_GROUP && !poll.getFriendGroup().getUsers().contains(user)) {
      AnsweringRestrictedGroupPollException exc = new AnsweringRestrictedGroupPollException();
      log.warn(String.format("%nAttempt to answer restricted group poll:" +
          "%nPOLL_ID = %d", pollId), exc);
      throw exc;
    }

    AnswerHistory answerHistory = answerHistoryService.getByPollAndUser(poll, user);

    if (answerHistory != null && answerHistory.getAnswer() != null) {
      PollAlreadyAnsweredException exc = new PollAlreadyAnsweredException();
      log.warn(String.format("%nAttempt to answer already answered poll:" +
          "%nPOLL_ID = %d", pollId), exc);
      throw exc;
    }

    if (poll.getAnswer1().getId().equals(answerId)) {
      if (!poll.vote1()) {
        pollTimedOut(poll);
      }
      if (answerHistory == null) {
        answerHistory = new AnswerHistory(user, poll, poll.getAnswer1());
      } else {
        answerHistory.setAnswer(poll.getAnswer1());
        answerHistory.setAnswerTime(new Timestamp(System.currentTimeMillis()));
      }

    } else if (poll.getAnswer2().getId().equals(answerId)) {
      if (!poll.vote2()) {
        pollTimedOut(poll);
      }
      if (answerHistory == null) {
        answerHistory = new AnswerHistory(user, poll, poll.getAnswer2());
      } else {
        answerHistory.setAnswer(poll.getAnswer2());
        answerHistory.setAnswerTime(new Timestamp(System.currentTimeMillis()));
      }

    } else {
      InvalidPollAnswerException exc = new InvalidPollAnswerException();
      log.warn(String.format("%nAttempt to answer poll with invalid answer ID:" +
          "%nPOLL_ID = %d, ANSWER_ID = %d", pollId, answerId), exc);
      throw exc;
    }

    pollService.save(poll);
    answerHistoryService.save(answerHistory);
  }

  @GetMapping("/check")
  public Long checkPollForAnswer(@RequestParam Long pollId) {
    User user = UserDetails.getCurrentUser();
    Poll poll = pollService.getById(pollId);
    AnswerHistory answerHistory = answerHistoryService.getByPollAndUser(poll, user);
    return answerHistory != null && answerHistory.getAnswer() != null ? answerHistory.getAnswer().getId() : 0;
  }

  @GetMapping("/skip")
  public void skipPoll(@RequestParam Long pollId) {
    User user = UserDetails.getCurrentUser();
    Poll poll = pollService.getById(pollId);

    if (user.equals(poll.getUser())) {
      AnsweringOwnPollException exc = new AnsweringOwnPollException();
      log.warn(String.format("%nAttempt to skip own poll:" +
          "%nPOLL_ID = %d", pollId), exc);
      throw exc;
    }

    AnswerHistory answerHistory = answerHistoryService.getByPollAndUser(poll, user);

    if (answerHistory != null) {
      PollAlreadyAnsweredException exc = new PollAlreadyAnsweredException();
      log.warn(String.format("%nAttempt to skip already answered or skipped poll:" +
          "%nPOLL_ID = %d", pollId), exc);
      throw exc;
    }

    answerHistory = new AnswerHistory(user, poll, null);
    answerHistoryService.save(answerHistory);
  }

  private void pollTimedOut(Poll poll) {
    PollTimedOutException exc = new PollTimedOutException();
    log.warn(String.format("%nAttempt to answer poll after timeout:" +
        "%nPOLL_ID = %d", poll.getId()), exc);
    throw exc;
  }
}
