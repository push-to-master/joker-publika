package hr.fer.opp.jokerpublika.web.controllers;

import hr.fer.opp.jokerpublika.model.FriendGroup;
import hr.fer.opp.jokerpublika.model.Poll;
import hr.fer.opp.jokerpublika.model.User;
import hr.fer.opp.jokerpublika.model.UserRoleEnum;
import hr.fer.opp.jokerpublika.model.util.UserDetails;
import hr.fer.opp.jokerpublika.services.FriendGroupService;
import hr.fer.opp.jokerpublika.services.PollService;
import hr.fer.opp.jokerpublika.services.UserService;
import hr.fer.opp.jokerpublika.web.dto.PollDto;
import hr.fer.opp.jokerpublika.web.exceptions.UnauthorizedPollAccessException;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/poll")
public class PollController {

  private final Logger log = LoggerFactory.getLogger(PollController.class);

  private ModelMapper mapper;

  private PollService pollService;

  private UserService userService;

  private FriendGroupService friendGroupService;

  public PollController(ModelMapper mapper,
                        PollService pollService,
                        UserService userService,
                        FriendGroupService friendGroupService) {
    this.mapper = mapper;
    this.pollService = pollService;
    this.userService = userService;
    this.friendGroupService = friendGroupService;
  }

  @PostMapping
  public void postPoll(@RequestPart("info") PollDto pollDto,
                       @RequestPart(value = "image1", required = false) MultipartFile img1,
                       @RequestPart(value = "image2", required = false) MultipartFile img2) {
    Poll poll = mapper.map(pollDto, Poll.class);

    if (img1 != null && !img1.isEmpty()) {
      try {
        poll.setImage1(getBase64Src(img1));
      } catch (IOException e) {
        log.warn("IO error getting 1st image bytes", e);
      }
    }

    if (img2 != null && !img2.isEmpty()) {
      try {
        poll.setImage2(getBase64Src(img2));
      } catch (IOException e) {
        log.warn("IO error getting 2nd image bytes", e);
      }
    }

    poll = pollService.save(poll);
    log.info("Created new poll with id " + poll.getId());
  }

  private String getBase64Src(MultipartFile file) throws IOException {
    return String.format("data:%s;base64,%s", file.getContentType(), Base64.getEncoder().encodeToString(file.getBytes()));
  }

  @GetMapping("/get-answer-polls")
  public List<PollDto> getPollsToAnswer() {
    return pollService.getAllToAnswer(UserDetails.getCurrentUser()).stream()
        .map(poll -> mapper.map(poll, PollDto.class))
        .collect(Collectors.toList());
  }

  @GetMapping
  public PollDto getPoll(@RequestParam Long pollId) {
    return mapper.map(pollService.getById(pollId), PollDto.class);
  }

  @GetMapping("/get-current")
  public List<PollDto> getCurrentPolls() {
    return pollService.getAllPublicActive().stream().map(poll -> mapper.map(poll, PollDto.class)).collect(Collectors.toList());
  }

  @GetMapping("/user")
  public List<PollDto> getUserPolls(@RequestParam Long userId) {
    User user = userService.getById(userId);
    List<Poll> polls = pollService.getAllByUser(user);
    return polls.stream().map(poll -> mapper.map(poll, PollDto.class)).collect(Collectors.toList());
  }

  @GetMapping("/user/public")
  public List<PollDto> getPublicUserPolls(@RequestParam Long userId) {
    User user = userService.getById(userId);
    List<Poll> polls = pollService.getAllPublicByUser(user);
    return polls.stream().map(poll -> mapper.map(poll, PollDto.class)).collect(Collectors.toList());
  }

  @GetMapping("/user/active")
  public List<PollDto> getActiveUserPolls(@RequestParam Long userId) {
    User user = userService.getById(userId);
    List<Poll> polls = pollService.getAllActiveByUser(user);
    return polls.stream().map(poll -> mapper.map(poll, PollDto.class)).collect(Collectors.toList());
  }

  @GetMapping("/user/inactive")
  public List<PollDto> getInactiveUserPolls(@RequestParam Long userId) {
    User user = userService.getById(userId);
    List<Poll> polls = pollService.getAllInactiveByUser(user);
    return polls.stream().map(poll -> mapper.map(poll, PollDto.class)).collect(Collectors.toList());
  }

  @GetMapping("/user/answered")
  public List<PollDto> getAnsweredUserPolls(@RequestParam Long userId) {
    User user = userService.getById(userId);
    List<Poll> polls = pollService.getAllAnsweredByUser(user);
    return polls.stream().map(poll -> mapper.map(poll, PollDto.class)).collect(Collectors.toList());
  }

  @GetMapping("/group")
  public List<PollDto> getGroupPolls(@RequestParam Long groupId) {
    FriendGroup group = friendGroupService.getById(groupId);
    List<Poll> polls = pollService.getAllByFriendGroup(group);
    return polls.stream().map(poll -> mapper.map(poll, PollDto.class)).collect(Collectors.toList());
  }

  @DeleteMapping
  public void deletePoll(@RequestParam Long pollId) {
    User user = UserDetails.getCurrentUser();
    Poll poll = pollService.getById(pollId);

    if (!user.equals(poll.getUser()) && !user.getRoles().contains(UserRoleEnum.ADMIN)) {
      UnauthorizedPollAccessException exc = new UnauthorizedPollAccessException();
      log.warn(String.format("%nAttempt to delete restricted poll:" +
          "%nPOLL_ID = %d", pollId), exc);
      throw exc;
    }

    poll.setDeleted(true);
    pollService.save(poll);
  }
}
