package hr.fer.opp.jokerpublika.web.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class AnswerDto {

  private Long id;

  @NotEmpty
  @Size(max = 128)
  private String text;

  @PositiveOrZero
  private Long voteCount;

  public AnswerDto() {
  }

  public AnswerDto(@NotEmpty String text) {
    this.text = text;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public Long getVoteCount() {
    return voteCount;
  }

  public void setVoteCount(Long voteCount) {
    this.voteCount = voteCount;
  }
}
