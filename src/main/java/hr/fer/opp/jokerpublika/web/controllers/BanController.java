package hr.fer.opp.jokerpublika.web.controllers;


import hr.fer.opp.jokerpublika.model.Ban;
import hr.fer.opp.jokerpublika.model.FriendGroup;
import hr.fer.opp.jokerpublika.model.User;
import hr.fer.opp.jokerpublika.services.BanService;
import hr.fer.opp.jokerpublika.services.FriendGroupService;
import hr.fer.opp.jokerpublika.services.UserService;
import hr.fer.opp.jokerpublika.web.exceptions.UserNotBannedException;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/ban")
public class BanController {

  private UserService userService;

  private BanService banService;

  private FriendGroupService friendGroupService;

  public BanController(UserService userService, BanService banService, FriendGroupService friendGroupService) {
    this.userService = userService;
    this.banService = banService;
    this.friendGroupService = friendGroupService;
  }

  @GetMapping("/all")
  public List<Ban> getAllBans() {
    return banService.getAll();
  }

  @PostMapping
  public void adminAddBan(@RequestPart("username") String username, @RequestPart("interval") String interval) {

    User userToBan = userService.getByUsernameNotDeactivated(username);
    long intervalLong;
    if (interval.equals("permanent")) {
      Timestamp startTime = new Timestamp(System.currentTimeMillis());
      userToBan.setCurrentBanStartTime(startTime);
      userToBan.setCurrentBanEndTime(null);
      Ban ban = new Ban(userToBan, startTime, null);
      banService.save(ban);
      userService.save(userToBan);
      for (FriendGroup friendGroup : friendGroupService.getAllByUser(userToBan)) {
        //  removes group if there is only 2 users in it
        if (friendGroup.getUsers().size() == 2) {
          friendGroupService.delete(friendGroup);
        } else {
          friendGroup.removeUser(userToBan);
          if (friendGroup.getOwner().equals(userToBan)) {
            friendGroup.setOwner(friendGroup.getUsers().get(0));
          }
          friendGroupService.save(friendGroup);
        }
      }
    } else {
      // one minute is for testing
      if (interval.equals("one_minute")) {
        intervalLong = 60000L;
      } else {
        intervalLong = Integer.parseInt(interval) * 24L * 60 * 60000;
      }
      Timestamp startTime = new Timestamp(System.currentTimeMillis());
      Timestamp endTime = new Timestamp(System.currentTimeMillis() + intervalLong);
      Ban ban = new Ban(userToBan, startTime, endTime);
      if (userToBan.getBanned()) {
        removeActiveBans(userToBan);
      }
      userToBan.setCurrentBanStartTime(startTime);
      userToBan.setCurrentBanEndTime(endTime);
      userService.save(userToBan);
      banService.save(ban);

    }
  }

  private void removeActiveBans(User userToBan) {
    List<Ban> bans = banService.getAllByUser(userToBan);
    Timestamp myTime = new Timestamp(System.currentTimeMillis());
    if (bans == null) {
      throw new UserNotBannedException();
    }
    for (Ban ban : bans) {
      if (ban.getStartTime() != null && ban.getEndTime() == null) banService.delete(ban);
      else if (myTime.after(ban.getStartTime()) && myTime.before(ban.getEndTime()) || ban.getEndTime() == null) {
        banService.delete(ban);
      }
    }
//    The TRICK is William Potter not minding that its hurts.
    userToBan.setCurrentBanStartTime(null);
    userToBan.setCurrentBanEndTime(null);
    userService.save(userToBan);
  }

  @PostMapping("/remove_active_ban")
  public void adminRemoveActiveBan(@RequestBody String username) {
    User user = userService.getByUsernameNotDeactivated(username);
    if (!user.getBanned()) {
      throw new UserNotBannedException();
    }

    removeActiveBans(user);

  }


}
