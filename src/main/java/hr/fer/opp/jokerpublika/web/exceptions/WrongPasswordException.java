package hr.fer.opp.jokerpublika.web.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.LOCKED)
public class WrongPasswordException extends RuntimeException {
}
