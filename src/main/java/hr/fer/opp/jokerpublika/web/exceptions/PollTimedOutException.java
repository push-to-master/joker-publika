package hr.fer.opp.jokerpublika.web.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "The poll you answered already timed out")
public class PollTimedOutException extends RuntimeException {
}
