package hr.fer.opp.jokerpublika.web.dto;

import hr.fer.opp.jokerpublika.model.PollTypeEnum;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

public class PollDto {

  private Long id;

  private UserDto user;

  @NotEmpty
  @Size(max = 512)
  private String question;

  @NotNull
  private AnswerDto answer1;

  @NotNull
  private AnswerDto answer2;

  private Timestamp postTime;

  private Timestamp timeout;

  @NotNull
  @Positive
  private Long duration;

  @NotNull
  private PollTypeEnum pollType;

  private String image1;

  private String image2;

  private Boolean active;

  private FriendGroupDto friendGroup;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public UserDto getUser() {
    return user;
  }

  public void setUser(UserDto user) {
    this.user = user;
  }

  public String getQuestion() {
    return question;
  }

  public void setQuestion(String question) {
    this.question = question;
  }

  public AnswerDto getAnswer1() {
    return answer1;
  }

  public void setAnswer1(AnswerDto answer1) {
    this.answer1 = answer1;
  }

  public AnswerDto getAnswer2() {
    return answer2;
  }

  public void setAnswer2(AnswerDto answer2) {
    this.answer2 = answer2;
  }

  public Timestamp getPostTime() {
    return postTime;
  }

  public void setPostTime(Timestamp postTime) {
    this.postTime = postTime;
  }

  public Timestamp getTimeout() {
    return timeout;
  }

  public void setTimeout(Timestamp timeout) {
    this.timeout = timeout;
  }

  public Long getDuration() {
    return duration;
  }

  public void setDuration(Long duration) {
    this.duration = duration;
  }

  public PollTypeEnum getPollType() {
    return pollType;
  }

  public void setPollType(PollTypeEnum pollType) {
    this.pollType = pollType;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public FriendGroupDto getFriendGroup() {
    return friendGroup;
  }

  public void setFriendGroup(FriendGroupDto friendGroup) {
    this.friendGroup = friendGroup;
  }

  public String getImage1() {
    return image1;
  }

  public void setImage1(String image1) {
    this.image1 = image1;
  }

  public String getImage2() {
    return image2;
  }

  public void setImage2(String image2) {
    this.image2 = image2;
  }
}
