package hr.fer.opp.jokerpublika.web.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class PollDurationDto {

  @NotNull
  @Positive
  private Long value;

  private String label;

  public Long getValue() {
    return value;
  }

  public void setValue(Long value) {
    this.value = value;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public void createLabel() {
    long minutes = value / 60_000;
    if (minutes < 60) {
      label = String.format("%2d min", minutes);
      return;
    }
    long hours = minutes / 60;
    minutes %= 60;
    String minutesString = (minutes == 0 ? "" : String.format(" %2d min", minutes));
    if (hours < 24) {
      label = String.format("%2d h  ", hours) + minutesString;
      return;
    }
    long days = hours / 24;
    hours %= 24;
    label = String.format("%2d d  ", days) + (hours == 0 ? "" : String.format(" %2d h  ", hours))
        + minutesString;
  }
}
