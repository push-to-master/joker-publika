package hr.fer.opp.jokerpublika.web.controllers;


import hr.fer.opp.jokerpublika.model.FriendGroup;
import hr.fer.opp.jokerpublika.model.PollTypeEnum;
import hr.fer.opp.jokerpublika.model.User;
import hr.fer.opp.jokerpublika.model.UserRoleEnum;
import hr.fer.opp.jokerpublika.services.FriendGroupService;
import hr.fer.opp.jokerpublika.services.PollService;
import hr.fer.opp.jokerpublika.services.UserService;
import hr.fer.opp.jokerpublika.web.dto.UserDto;
import hr.fer.opp.jokerpublika.web.dto.UserRegistrationDto;
import hr.fer.opp.jokerpublika.web.exceptions.*;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user")
public class UserController {

  private final Logger log = LoggerFactory.getLogger(UserController.class);

  private ModelMapper mapper;

  private UserService userService;

  private PollService pollService;

  private FriendGroupService friendGroupService;

  private PasswordEncoder passwordEncoder;


  public UserController(ModelMapper mapper, UserService userService, PasswordEncoder passwordEncoder, PollService pollService, FriendGroupService friendGroupService) {
    this.mapper = mapper;
    this.userService = userService;
    this.passwordEncoder = passwordEncoder;
    this.pollService = pollService;
    this.friendGroupService = friendGroupService;
  }

  @PostMapping("/register")
  public void registerUser(@RequestBody UserRegistrationDto userDto) {
    userDto.addRole(UserRoleEnum.USER);
    User user = mapper.map(userDto, User.class);
    userService.save(user);
  }

  @RequestMapping(value = "/email", method = RequestMethod.POST)
  public void changeEmailOfCurrentUser(@RequestBody String email) {
    User user = userService.getCurrentUser();
    if (userService.getByEmail(email) != null) {
      throw new EmailTakenException();
    }
    user.setEmail(email);
    userService.save(user);
  }

  @RequestMapping(value = "/username", method = RequestMethod.POST)
  public void changeUsernameOfCurrentUser(@RequestBody String username) {
    User user = userService.getCurrentUser();
    try {
      userService.getByUsername(username);
      throw new UsernameTakenException();
    } catch (UserNotFoundException e) {
      user.setUsername(username);
      userService.save(user);
    }

  }

  @RequestMapping(value = "/anonymous", method = RequestMethod.POST)
  public void changeAnonymityOfCurrentUser(@RequestBody Boolean newAnonymity) {
    User current = userService.getCurrentUser();
    userService.changeAnonymity(current, newAnonymity);
  }

  @RequestMapping(value = "/deactivate_current", method = RequestMethod.POST)
  public void deactivateCurrentUser() {
    User user = userService.getCurrentUser();
    user.setDeactivated(true);
    userService.save(user);
    // all his polls are deleted
    pollService.getAllByUser(user).forEach(poll -> {
      poll.setDeleted(true);
      pollService.save(poll);
    });

    // user is removed from every group
    for (FriendGroup friendGroup : friendGroupService.getAllByUser(user)) {
      //  removes group if there is only 2 users in it
      if (friendGroup.getUsers().size() == 2) {
        friendGroupService.delete(friendGroup);
      } else {
        friendGroup.removeUser(user);
        if (friendGroup.getOwner().equals(user)) {
          friendGroup.setOwner(friendGroup.getUsers().get(0));
        }
        friendGroupService.save(friendGroup);
      }
    }

  }

  @RequestMapping(value = "/current", method = RequestMethod.POST)
  public void updateCurrentUser(@RequestBody UserDto userDto) {
    userDto.addRole(UserRoleEnum.USER);
    User user = userService.getCurrentUser();
    if (!user.getId().equals(userDto.getId())) return;
    User userUpdated = mapper.map(userDto, User.class);
    userService.save(userUpdated);
  }

  @RequestMapping(value = "/polltype", method = RequestMethod.POST)
  public void updateCurrentUserDefaultPollType(@RequestBody String pollType) {
    User user = userService.getCurrentUser();
    user.setDefaultPollType(PollTypeEnum.valueOf(pollType));
    userService.save(user);
  }

  @RequestMapping(value = "/polltimeout", method = RequestMethod.POST)
  public void updateCurrentUserDefaultPollDuration(@RequestBody Long duration) {
    User user = userService.getCurrentUser();
    user.setDefaultPollDuration(duration);
    userService.save(user);
  }

  @GetMapping("/current")
  public UserDto getCurrentUser() {
    User user = userService.getCurrentUser();
    return mapper.map(user, UserDto.class);
  }

  @GetMapping("/username")
  public UserDto getByUsername(@RequestBody String username) {
    User user = userService.getByUsername(username);
    if (user.getDeactivated()) {
      UserDeactivatedException exc = new UserDeactivatedException();
      log.warn(String.format("%nAttempt to fetch deactivated user:" +
          "%nUSERNAME = %s", username), exc);
      throw exc;
    }
    return mapper.map(user, UserDto.class);
  }

  @GetMapping("/passwordHash")
  public String getPasswordHash(@RequestParam String password) {
    return passwordEncoder.encode(password);
  }

  @RequestMapping(value = "/password", method = RequestMethod.POST)
  public void checkPasswordOfCurrentUser(@RequestBody String password) {
    User user = userService.getCurrentUser();
    if (!passwordEncoder.matches(password, user.getPasswordHash())) {
      throw new WrongPasswordException();
    }
  }

  //  NESIGURNA METODA PREPRAVITI (na frontendu joj prethodi provjera sifre ---------------
  //  ali sto ako netko pristupi direktno ovoj metodi :( ----------------------------------
  //  -------------------------------------------------------------------------------------
  @RequestMapping(value = "/newpassword", method = RequestMethod.POST)
  public void setNewPasswordOfCurrentUser(@RequestBody String newpassword) {
    User user = userService.getCurrentUser();
    user.setPasswordHash(passwordEncoder.encode(newpassword));
    userService.save(user);
  }
  //  -------------------------------------------------------------------------------------

  @GetMapping("/all-public")
  public List<UserDto> getAllPublicUsers() {
    List<User> users = userService.getAllPublic();
    return users.stream().map(user -> mapper.map(user, UserDto.class)).collect(Collectors.toList());
  }

  @GetMapping("/all-anonymous")
  public List<UserDto> getAllAnonymousUsers() {
    List<User> users = userService.getAllAnonymous();
    return users.stream().map(user -> mapper.map(user, UserDto.class)).collect(Collectors.toList());
  }

  @GetMapping("/all")
  public List<UserDto> getAllUsers() {
    List<User> users = userService.getAll();
    return users.stream().map(user -> mapper.map(user, UserDto.class)).collect(Collectors.toList());
  }
}
