package hr.fer.opp.jokerpublika.web.dto;

import hr.fer.opp.jokerpublika.model.PollTypeEnum;
import hr.fer.opp.jokerpublika.model.UserRoleEnum;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class UserDto {

  private Long id;

  @NotEmpty
  private String username;

  @NotEmpty
  @Email
  private String email;

  @NotNull
  private Boolean anonymous;

  private List<UserRoleEnum> roles;

  private PollTypeEnum defaultPollType;

  private Long defaultPollDuration;

  private Boolean deactivated;

  private Boolean banned;

  private Timestamp currentBanStartTime;

  private Timestamp currentBanEndTime;

  private Integer scoreCounter;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Boolean getAnonymous() {
    return anonymous;
  }

  public void setAnonymous(Boolean anonymous) {
    this.anonymous = anonymous;
  }

  public List<UserRoleEnum> getRoles() {
    return roles;
  }

  public void setRoles(List<UserRoleEnum> roles) {
    this.roles = roles;
  }

  public PollTypeEnum getDefaultPollType() {
    return defaultPollType;
  }

  public void setDefaultPollType(PollTypeEnum defaultPollType) {
    this.defaultPollType = defaultPollType;
  }

  public Long getDefaultPollDuration() {
    return defaultPollDuration;
  }

  public void setDefaultPollDuration(Long defaultPollDuration) {
    this.defaultPollDuration = defaultPollDuration;
  }

  public Boolean getDeactivated() {
    return deactivated;
  }

  public void setDeactivated(Boolean deactivated) {
    this.deactivated = deactivated;
  }

  public Boolean getBanned() {
    return banned;
  }

  public void setBanned(Boolean banned) {
    this.banned = banned;
  }

  public Integer getScoreCounter() {
    return scoreCounter;
  }

  public void setScoreCounter(Integer scoreCounter) {
    this.scoreCounter = scoreCounter;
  }

  public Timestamp getCurrentBanStartTime() {
    return currentBanStartTime;
  }

  public void setCurrentBanStartTime(Timestamp currentBanStartTime) {
    this.currentBanStartTime = currentBanStartTime;
  }

  public Timestamp getCurrentBanEndTime() {
    return currentBanEndTime;
  }

  public void setCurrentBanEndTime(Timestamp currentBanEndTime) {
    this.currentBanEndTime = currentBanEndTime;
  }

  public void addRole(UserRoleEnum role) {
    if (roles == null) {
      roles = new ArrayList<>();
    }
    roles.add(role);
  }
}
