package hr.fer.opp.jokerpublika.web.controllers.util;

import hr.fer.opp.jokerpublika.model.*;
import hr.fer.opp.jokerpublika.model.util.UserDetails;
import hr.fer.opp.jokerpublika.services.FriendGroupService;
import hr.fer.opp.jokerpublika.services.PollDurationService;
import hr.fer.opp.jokerpublika.services.UserService;
import hr.fer.opp.jokerpublika.web.dto.*;
import hr.fer.opp.jokerpublika.web.exceptions.*;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class ModelMapperConfiguration {

  private PasswordEncoder passwordEncoder;

  private PollDurationService pollDurationService;

  private FriendGroupService friendGroupService;

  private UserService userService;

  public ModelMapperConfiguration(PasswordEncoder passwordEncoder,
                                  PollDurationService pollDurationService,
                                  FriendGroupService friendGroupService,
                                  UserService userService) {
    this.passwordEncoder = passwordEncoder;
    this.pollDurationService = pollDurationService;
    this.friendGroupService = friendGroupService;
    this.userService = userService;
  }

  @Bean
  public ModelMapper modelMapper() {
    Logger log = LoggerFactory.getLogger(ModelMapper.class);

    ModelMapper mapper = new ModelMapper();

    mapper.getConfiguration().setSkipNullEnabled(true);

    Converter<UserRegistrationDto, User> registrationDtoUserConverter = context -> {
      UserRegistrationDto dto = context.getSource();

      if (dto == null) {
        return null;
      }
      if (!dto.getPassword().equals(dto.getConfirmPassword())) {
        log.warn("Confirmation password mismatch during user registration");
        throw new PasswordConfirmationMismatchException();
      }

      User user = new User();
      user.setUsername(dto.getUsername());
      user.setEmail(dto.getEmail());
      user.setPasswordHash(passwordEncoder.encode(dto.getPassword()));
      user.setAnonymous(dto.getAnonymous());
      user.setRoles(dto.getRoles());
      if (!dto.getAnonymous()) {
        user.setDefaultPollType(dto.getDefaultPollType() == null ? PollTypeEnum.PUBLIC : dto.getDefaultPollType());
      } else {
        user.setDefaultPollType(PollTypeEnum.ANONYMOUS);
      }
      if (dto.getDefaultPollDuration() != null) {
        user.setDefaultPollDuration(dto.getDefaultPollDuration());
      } else {
        List<PollDuration> pollDuration = pollDurationService.getAll();
        user.setDefaultPollDuration(pollDuration.get(pollDuration.size() - 1).getValue());
      }
      return user;
    };
    mapper.typeMap(UserRegistrationDto.class, User.class).setConverter(registrationDtoUserConverter);

    Converter<AnswerDto, Answer> dtoAnswerConverter = context -> {
      AnswerDto dto = context.getSource();

      if (dto == null) {
        return null;
      }

      Answer answer = new Answer();
      if (dto.getId() != null) {
        answer.setId(dto.getId());
      }
      answer.setText(dto.getText());
      answer.setVoteCount(dto.getVoteCount() == null ? 0 : dto.getVoteCount());
      return answer;
    };
    mapper.typeMap(AnswerDto.class, Answer.class).setConverter(dtoAnswerConverter);

    Converter<PollDto, Poll> dtoPollConverter = context -> {
      PollDto dto = context.getSource();
      User user = UserDetails.getCurrentUser();

      if (dto == null) {
        return null;
      } else if (user.getAnonymous() && dto.getPollType() != PollTypeEnum.ANONYMOUS) {
        AnonymousUserPollTypeException exc = new AnonymousUserPollTypeException();
        log.warn("\nAttempt to create non-anonymous poll by anonymous user", exc);
        throw exc;
      } else if (dto.getPollType() == PollTypeEnum.FRIEND_GROUP
          && (dto.getFriendGroup() == null || dto.getFriendGroup().getId() == null)) {
        GroupPollUndefinedGroupException exc = new GroupPollUndefinedGroupException();
        log.warn("\nAttempt to create group poll with undefined group", exc);
        throw exc;
      }

      Poll poll = new Poll();
      poll.setUser(UserDetails.getCurrentUser());
      poll.setQuestion(dto.getQuestion());
      poll.setAnswer1(mapper.map(dto.getAnswer1(), Answer.class));
      poll.setAnswer2(mapper.map(dto.getAnswer2(), Answer.class));
      poll.setTimeout(new Timestamp(poll.getPostTime().getTime() + dto.getDuration()));
      poll.setPollType(dto.getPollType());

      if (dto.getFriendGroup() != null && dto.getFriendGroup().getId() != null) {
        FriendGroup friendGroup = friendGroupService.getById(dto.getFriendGroup().getId());
        poll.setFriendGroup(friendGroup);
      }
      return poll;
    };
    mapper.typeMap(PollDto.class, Poll.class).setConverter(dtoPollConverter);

    Converter<Poll, PollDto> pollDtoConverter = context -> {
      Poll poll = context.getSource();

      if (poll == null) {
        return null;
      }

      PollDto dto = new PollDto();
      dto.setId(poll.getId());
      dto.setUser(mapper.map(poll.getUser(), UserDto.class));
      dto.setQuestion(poll.getQuestion());
      dto.setAnswer1(mapper.map(poll.getAnswer1(), AnswerDto.class));
      dto.setAnswer2(mapper.map(poll.getAnswer2(), AnswerDto.class));
      dto.setPostTime(poll.getPostTime());
      dto.setTimeout(poll.getTimeout());
      dto.setDuration(poll.getTimeout().getTime() - poll.getPostTime().getTime());
      dto.setPollType(poll.getPollType());
      dto.setImage1(poll.getImage1());
      dto.setImage2(poll.getImage2());
      if (poll.getFriendGroup() != null) {
        dto.setFriendGroup(mapper.map(poll.getFriendGroup(), FriendGroupDto.class));
      }
      return dto;
    };
    mapper.typeMap(Poll.class, PollDto.class).setConverter(pollDtoConverter);

    Converter<FriendGroupDto, FriendGroup> dtoFriendGroupConverter = context -> {
      FriendGroupDto dto = context.getSource();
      User user = UserDetails.getCurrentUser();

      if (dto == null) {
        return null;
      } else if (user.getAnonymous()) {
        AnonymousUserGroupException exc = new AnonymousUserGroupException();
        log.warn("\nAttempt to create group by anonymous user", exc);
        throw exc;
      } else if (dto.getUsers() == null || dto.getUsers().isEmpty()
          || dto.getUsers().size() == 1 && user.getUsername().equals(dto.getUsers().get(0).getUsername())) {
        NotEnoughUsersInGroupException exc = new NotEnoughUsersInGroupException();
        log.warn("\nAttempt to create group with less than two users", exc);
        throw exc;
      }

      FriendGroup friendGroup = new FriendGroup();
      friendGroup.setName(dto.getName());
      friendGroup.setOwner(user);
      List<User> users = new ArrayList<>();
      users.add(user);
      dto.getUsers().forEach(userDto -> {
        User friend = userService.getByUsername(userDto.getUsername());
        if (friend.getAnonymous()) {
          AnonymousUserGroupException exc = new AnonymousUserGroupException();
          log.warn("\nAttempt to create group containing anonymous user", exc);
          throw exc;
        } else if (friend.equals(user)) {
          AddingSelfToGroupException exc = new AddingSelfToGroupException();
          log.warn("\nAttempt to add self to created group", exc);
          throw exc;
        } else if (users.contains(friend)) {
          AddingExistingMemberToGroupException exc = new AddingExistingMemberToGroupException();
          log.warn("\nAttempt to add existing member to group", exc);
          throw exc;
        }
        users.add(friend);
      });
      friendGroup.setUsers(users);
      return friendGroup;
    };
    mapper.typeMap(FriendGroupDto.class, FriendGroup.class).setConverter(dtoFriendGroupConverter);

    return mapper;
  }
}
