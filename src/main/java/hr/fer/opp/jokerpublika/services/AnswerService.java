package hr.fer.opp.jokerpublika.services;


import hr.fer.opp.jokerpublika.model.Answer;

import java.util.List;

public interface AnswerService {

// Basic

  Answer getById(Long id);

  List<Answer> getAll();

  Answer save(Answer answer);

  void delete(Answer answer);

  void deleteById(Long id);
}
