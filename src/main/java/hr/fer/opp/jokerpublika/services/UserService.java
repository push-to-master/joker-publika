package hr.fer.opp.jokerpublika.services;

import hr.fer.opp.jokerpublika.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

// Basic

  User getById(Long id);

  List<User> getAll();

  User save(User user);

  void delete(User user);

  void deleteById(Long id);

// Specific

  List<User> getAllPublic();

  List<User> getAllAnonymous();

  User getByUsername(String username);

  User getByUsernameNotDeactivated(String username);

  User getCurrentUser();

  User getByEmail(String email);

  void changeAnonymity(User user, Boolean newAnonymity);
}
