package hr.fer.opp.jokerpublika.services.impl;

import hr.fer.opp.jokerpublika.model.FriendGroup;
import hr.fer.opp.jokerpublika.model.User;
import hr.fer.opp.jokerpublika.repositories.FriendGroupRepository;
import hr.fer.opp.jokerpublika.services.FriendGroupService;
import hr.fer.opp.jokerpublika.services.PollService;
import hr.fer.opp.jokerpublika.web.exceptions.FriendGroupNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FriendGroupServiceImpl implements FriendGroupService {

  private FriendGroupRepository friendGroupRepository;

  private PollService pollService;

  public FriendGroupServiceImpl(FriendGroupRepository friendGroupRepository,
                                PollService pollService) {
    this.friendGroupRepository = friendGroupRepository;
    this.pollService = pollService;
  }

  @Override
  public FriendGroup getById(Long id) {
    Optional<FriendGroup> friendGroup = friendGroupRepository.findById(id);
    if (friendGroup.isEmpty()) {
      throw new FriendGroupNotFoundException();
    } else return friendGroup.get();
  }

  @Override
  public List<FriendGroup> getAll() {
    return friendGroupRepository.findAll();
  }

  @Override
  public FriendGroup save(FriendGroup friendGroup) {
    return friendGroupRepository.save(friendGroup);
  }

  @Override
  public void delete(FriendGroup friendGroup) {
    pollService.getAllByFriendGroup(friendGroup).forEach(poll -> {
      poll.setDeleted(true);
      poll.setFriendGroup(null);
      pollService.save(poll);
    });
    friendGroupRepository.delete(friendGroup);
  }

  @Override
  public void deleteById(Long id) {
    FriendGroup friendGroup = getById(id);
    pollService.getAllByFriendGroup(friendGroup).forEach(poll -> {
      poll.setDeleted(true);
      poll.setFriendGroup(null);
      pollService.save(poll);
    });
    friendGroupRepository.deleteById(id);
  }

  @Override
  public List<FriendGroup> getAllByName(String name) {
    return friendGroupRepository.findAllByName(name);
  }

  @Override
  public List<FriendGroup> getAllByOwner(User owner) {
    return friendGroupRepository.findAllByOwner(owner);
  }

  @Override
  public List<FriendGroup> getAllByUser(User user) {
    return friendGroupRepository.findAllByUsersContains(user);
  }
}
