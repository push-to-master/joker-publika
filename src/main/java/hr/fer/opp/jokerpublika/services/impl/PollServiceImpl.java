package hr.fer.opp.jokerpublika.services.impl;

import hr.fer.opp.jokerpublika.appsettings.PollScoring;
import hr.fer.opp.jokerpublika.model.FriendGroup;
import hr.fer.opp.jokerpublika.model.Poll;
import hr.fer.opp.jokerpublika.model.PollTypeEnum;
import hr.fer.opp.jokerpublika.model.User;
import hr.fer.opp.jokerpublika.repositories.PollRepository;
import hr.fer.opp.jokerpublika.services.AnswerHistoryService;
import hr.fer.opp.jokerpublika.services.PollService;
import hr.fer.opp.jokerpublika.web.exceptions.PollNotFoundException;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class PollServiceImpl implements PollService {

  private PollRepository pollRepository;

  private AnswerHistoryService answerHistoryService;

  public PollServiceImpl(PollRepository pollRepository, AnswerHistoryService answerHistoryService) {
    this.pollRepository = pollRepository;
    this.answerHistoryService = answerHistoryService;
  }

// Basic

  @Override
  public Poll getById(Long id) {
    Optional<Poll> optional = pollRepository.findById(id);
    if (optional.isEmpty()) {
      throw new PollNotFoundException();
    }
    return optional.get();
  }

  @Override
  public List<Poll> getAll() {
    return pollRepository.findAll();
  }

  @Override
  public Poll save(Poll poll) {
    return pollRepository.save(poll);
  }

  @Override
  public void delete(Poll poll) {
    pollRepository.delete(poll);
  }

  @Override
  public void deleteById(Long id) {
    pollRepository.deleteById(id);
  }

// Specific

  @Override
  public List<Poll> getAllPublicActive() {
    return pollRepository.findAllByTimeoutAfterAndPollTypeNotAndDeletedFalse(new Timestamp(System.currentTimeMillis()),
        PollTypeEnum.FRIEND_GROUP, sortByPostTimeDesc());
  }

  @Override
  public List<Poll> getAllByUser(User user) {
    return pollRepository.findAllByUserAndDeletedFalse(user, sortByPostTimeDesc());
  }

  @Override
  public List<Poll> getAllDeletedByUser(User user) {
    return pollRepository.findAllByUserAndDeletedTrue(user);
  }

  @Override
  public List<Poll> getAllPublicByUser(User user) {
    return pollRepository.findAllByUserAndPollTypeAndDeletedFalse(user, PollTypeEnum.PUBLIC, sortByPostTimeDesc());
  }

  @Override
  public List<Poll> getAllActiveByUser(User user) {
    return pollRepository.findAllByUserAndTimeoutAfterAndDeletedFalse(user, new Timestamp(System.currentTimeMillis()),
        sortByPostTimeDesc());
  }

  @Override
  public List<Poll> getAllInactiveByUser(User user) {
    return pollRepository.findAllByUserAndTimeoutBeforeAndDeletedFalse(user, new Timestamp(System.currentTimeMillis()),
        sortByTimeoutDesc());
  }

  @Override
  public List<Poll> getAllByFriendGroup(FriendGroup friendGroup) {
    return pollRepository.findAllByFriendGroupAndDeletedFalse(friendGroup, sortByPostTimeDesc());
  }

  @Override
  public List<Poll> getAllAnsweredByUser(User user) {
    List<Poll> polls = pollRepository.findAllAnsweredByUser(user);
    polls.sort(Comparator.comparing((Poll poll) -> answerHistoryService.getByPollAndUser(poll, user).getAnswerTime()).reversed());
    return polls;
  }

  @Override
  public List<Poll> getAllToAnswer(User user) {
    List<Poll> polls = pollRepository.findAllToAnswer(user);
    polls.removeIf(poll -> {
      if (poll.getFriendGroup() != null && poll.getFriendGroup().getUsers().contains(user)) {
        return false;
      } else return poll.getFriendGroup() != null;
    });
    polls.sort(Comparator.comparingDouble(PollScoring::getPollScore));
    return polls;
  }

  private Sort sortByPostTimeDesc() {
    return Sort.by(Sort.Direction.DESC, "postTime");
  }

  private Sort sortByTimeoutDesc() {
    return Sort.by(Sort.Direction.DESC, "timeout");
  }
}
