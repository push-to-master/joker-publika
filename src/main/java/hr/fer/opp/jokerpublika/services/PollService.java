package hr.fer.opp.jokerpublika.services;

import hr.fer.opp.jokerpublika.model.FriendGroup;
import hr.fer.opp.jokerpublika.model.Poll;
import hr.fer.opp.jokerpublika.model.User;

import java.util.List;

public interface PollService {

// Basic

  Poll getById(Long id);

  List<Poll> getAll();

  Poll save(Poll poll);

  void delete(Poll poll);

  void deleteById(Long id);

// Specific

  List<Poll> getAllPublicActive();

  List<Poll> getAllByUser(User user);

  List<Poll> getAllDeletedByUser(User user);

  List<Poll> getAllPublicByUser(User user);

  List<Poll> getAllActiveByUser(User user);

  List<Poll> getAllInactiveByUser(User user);

  List<Poll> getAllByFriendGroup(FriendGroup friendGroup);

  List<Poll> getAllAnsweredByUser(User user);

  List<Poll> getAllToAnswer(User user);
}
