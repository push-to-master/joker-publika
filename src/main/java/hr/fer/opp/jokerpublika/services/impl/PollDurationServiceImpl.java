package hr.fer.opp.jokerpublika.services.impl;

import hr.fer.opp.jokerpublika.model.PollDuration;
import hr.fer.opp.jokerpublika.repositories.PollDurationRepository;
import hr.fer.opp.jokerpublika.services.PollDurationService;
import hr.fer.opp.jokerpublika.web.exceptions.PollDurationAlreadyExistsException;
import hr.fer.opp.jokerpublika.web.exceptions.PollNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PollDurationServiceImpl implements PollDurationService {

  private final Logger log = LoggerFactory.getLogger(PollDurationService.class);

  PollDurationRepository pollDurationRepository;

  public PollDurationServiceImpl(PollDurationRepository pollDurationRepository) {
    this.pollDurationRepository = pollDurationRepository;
  }

// Basic

  @Override
  public PollDuration getById(Long id) {
    Optional<PollDuration> optional = pollDurationRepository.findById(id);
    if (optional.isEmpty()) {
      throw new PollNotFoundException();
    }
    return optional.get();
  }

  @Override
  public List<PollDuration> getAll() {
    return pollDurationRepository.findAll(sortByValueAsc());
  }

  @Override
  public PollDuration save(PollDuration pollDuration) {
    if (pollDurationRepository.findByValue(pollDuration.getValue()) != null) {
      PollDurationAlreadyExistsException exc = new PollDurationAlreadyExistsException();
      log.warn("\nAttempt to add existing poll duration to database", exc);
      throw exc;
    }
    return pollDurationRepository.save(pollDuration);
  }

  @Override
  public void delete(PollDuration pollDuration) {
    pollDurationRepository.delete(pollDuration);
  }

  @Override
  public void deleteById(Long id) {
    pollDurationRepository.deleteById(id);
  }

// Specific

  @Override
  public void deleteByValue(Long value) {
    PollDuration duration = pollDurationRepository.findByValue(value);
    pollDurationRepository.delete(duration);
  }

  private Sort sortByValueAsc() {
    return Sort.by(Sort.Direction.ASC, "value");
  }
}
