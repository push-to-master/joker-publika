package hr.fer.opp.jokerpublika.services;

import hr.fer.opp.jokerpublika.model.PollDuration;

import java.util.List;

public interface PollDurationService {

// Basic

  PollDuration getById(Long id);

  List<PollDuration> getAll();

  PollDuration save(PollDuration pollDuration);

  void delete(PollDuration pollDuration);

  void deleteById(Long id);

// Specific

  void deleteByValue(Long value);
}
