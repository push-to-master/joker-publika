package hr.fer.opp.jokerpublika.services.impl;

import hr.fer.opp.jokerpublika.model.AnswerHistory;
import hr.fer.opp.jokerpublika.model.Poll;
import hr.fer.opp.jokerpublika.model.User;
import hr.fer.opp.jokerpublika.repositories.AnswerHistoryRepository;
import hr.fer.opp.jokerpublika.services.AnswerHistoryService;
import hr.fer.opp.jokerpublika.web.exceptions.AnswerHistoryNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AnswerHistoryServiceImpl implements AnswerHistoryService {

  private AnswerHistoryRepository answerHistoryRepository;

  public AnswerHistoryServiceImpl(AnswerHistoryRepository answerHistoryRepository) {
    this.answerHistoryRepository = answerHistoryRepository;
  }

// Basic

  @Override
  public AnswerHistory getById(Long id) {
    Optional<AnswerHistory> optional = answerHistoryRepository.findById(id);
    if (optional.isEmpty()) {
      throw new AnswerHistoryNotFoundException();
    }
    return optional.get();
  }

  @Override
  public List<AnswerHistory> getAll() {
    return answerHistoryRepository.findAll();
  }

  @Override
  public AnswerHistory save(AnswerHistory answerHistory) {
    return answerHistoryRepository.save(answerHistory);
  }

  @Override
  public void delete(AnswerHistory answerHistory) {
    answerHistoryRepository.delete(answerHistory);
  }

  @Override
  public void deleteById(Long id) {
    answerHistoryRepository.deleteById(id);
  }

// Specific

  @Override
  public List<AnswerHistory> getAllByPoll(Poll poll) {
    return answerHistoryRepository.findAllByPoll(poll);
  }

  @Override
  public List<AnswerHistory> getAllByUser(User user) {
    return answerHistoryRepository.findAllByUser(user);
  }

  @Override
  public AnswerHistory getByPollAndUser(Poll poll, User user) {
    return answerHistoryRepository.findByPollAndUser(poll, user);
  }
}
