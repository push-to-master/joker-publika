package hr.fer.opp.jokerpublika.services;


import hr.fer.opp.jokerpublika.model.FriendGroup;
import hr.fer.opp.jokerpublika.model.User;

import java.util.List;

public interface FriendGroupService {

  FriendGroup getById(Long id);

  List<FriendGroup> getAll();

  FriendGroup save(FriendGroup friendGroup);

  void delete(FriendGroup friendGroup);

  void deleteById(Long id);

  List<FriendGroup> getAllByName(String name);

  List<FriendGroup> getAllByOwner(User owner);

  List<FriendGroup> getAllByUser(User user);
}
