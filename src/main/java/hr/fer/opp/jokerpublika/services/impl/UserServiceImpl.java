package hr.fer.opp.jokerpublika.services.impl;

import hr.fer.opp.jokerpublika.model.User;
import hr.fer.opp.jokerpublika.model.util.UserDetails;
import hr.fer.opp.jokerpublika.repositories.UserRepository;
import hr.fer.opp.jokerpublika.services.UserService;
import hr.fer.opp.jokerpublika.web.exceptions.UserNotFoundException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

  private UserRepository userRepository;

  public UserServiceImpl(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public User getById(Long id) {
    Optional<User> optional = userRepository.findById(id);
    if (optional.isEmpty()) {
      throw new UserNotFoundException();
    }
    return optional.get();
  }

  @Override
  public List<User> getAll() {
    return userRepository.findAll();
  }

  @Override
  public User save(User user) {
    return userRepository.save(user);
  }

  @Override
  public void delete(User user) {
    userRepository.delete(user);
  }

  @Override
  public void deleteById(Long id) {
    User user = getById(id);
    delete(user);
  }

  @Override
  public List<User> getAllPublic() {
    return userRepository.findAllByAnonymous(false);
  }

  @Override
  public List<User> getAllAnonymous() {
    return userRepository.findAllByAnonymous(true);
  }

  @Override
  public User getByUsername(String username) {
    User user = userRepository.findByUsername(username);
    if (user == null) {
      throw new UserNotFoundException();
    }
    return user;
  }

  @Override
  public User getByUsernameNotDeactivated(String username) {
    User user = userRepository.findByUsernameAndDeactivatedFalse(username);
    if (user == null) {
      throw new UserNotFoundException();
    }
    return user;
  }


  @Override
  public User getCurrentUser() {
    return UserDetails.getCurrentUser();
  }

  @Override
  public User getByEmail(String email) {
    return userRepository.findByEmail(email);
  }

  @Override
  public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
    User user = userRepository.findByUsername(s);
    if (user == null) {
      throw new UsernameNotFoundException(s);
    }
    return new hr.fer.opp.jokerpublika.model.util.UserDetails(user);
  }

  @Override
  public void changeAnonymity(User user, Boolean newAnonymity) {
    if (user.getAnonymous() && !newAnonymity) {
      user.setAnonymous(false);
    }
  }
}
