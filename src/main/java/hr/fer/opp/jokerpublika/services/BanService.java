package hr.fer.opp.jokerpublika.services;

import hr.fer.opp.jokerpublika.model.Ban;
import hr.fer.opp.jokerpublika.model.User;

import java.util.List;
import java.util.Optional;

public interface BanService {
  Ban getById(Long id);

  List<Ban> getAll();

  Ban save(Ban ban);

  List<Ban>getAllByUser(User user);

  void delete(Ban ban);

  void deleteById(Long id);
}
