package hr.fer.opp.jokerpublika.services.impl;

import hr.fer.opp.jokerpublika.model.Answer;
import hr.fer.opp.jokerpublika.repositories.AnswerRepository;
import hr.fer.opp.jokerpublika.services.AnswerService;
import hr.fer.opp.jokerpublika.web.exceptions.AnswerNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AnswerServiceImpl implements AnswerService {

  private AnswerRepository answerRepository;

  public AnswerServiceImpl(AnswerRepository answerRepository) {
    this.answerRepository = answerRepository;
  }

// Basic

  @Override
  public Answer getById(Long id) {
    Optional<Answer> optional = answerRepository.findById(id);
    if (optional.isEmpty()) {
      throw new AnswerNotFoundException();
    }
    return optional.get();
  }

  @Override
  public List<Answer> getAll() {
    return answerRepository.findAll();
  }

  @Override
  public Answer save(Answer answer) {
    return answerRepository.save(answer);
  }

  @Override
  public void delete(Answer answer) {
    answerRepository.delete(answer);
  }

  @Override
  public void deleteById(Long id) {
    answerRepository.deleteById(id);
  }
}
