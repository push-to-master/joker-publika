package hr.fer.opp.jokerpublika.services;

import hr.fer.opp.jokerpublika.model.AnswerHistory;
import hr.fer.opp.jokerpublika.model.Poll;
import hr.fer.opp.jokerpublika.model.User;

import java.util.List;

public interface AnswerHistoryService {

// Basic

  AnswerHistory getById(Long id);

  List<AnswerHistory> getAll();

  AnswerHistory save(AnswerHistory answerHistory);

  void delete(AnswerHistory answerHistory);

  void deleteById(Long id);

// Specific

  List<AnswerHistory> getAllByPoll(Poll poll);

  List<AnswerHistory> getAllByUser(User user);

  AnswerHistory getByPollAndUser(Poll poll, User user);
}
