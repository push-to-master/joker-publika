package hr.fer.opp.jokerpublika.services.impl;

import hr.fer.opp.jokerpublika.model.Ban;
import hr.fer.opp.jokerpublika.model.User;
import hr.fer.opp.jokerpublika.repositories.BanRepository;
import hr.fer.opp.jokerpublika.services.BanService;
import hr.fer.opp.jokerpublika.web.exceptions.BanNotFoundException;
import hr.fer.opp.jokerpublika.web.exceptions.UserNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BanServiceImpl implements BanService {

  private BanRepository banRepository;

  public BanServiceImpl(BanRepository banRepository) {
    this.banRepository = banRepository;
  }

  @Override
  public Ban getById(Long id) {
    Optional<Ban> optional = banRepository.findById(id);
    if (optional.isEmpty()) {
      throw new BanNotFoundException();
    }
    return optional.get();
  }

  @Override
  public List<Ban> getAll() {
    return banRepository.findAll();
  }

  @Override
  public Ban save(Ban ban) {
    return banRepository.save(ban);
  }

  @Override
  public List<Ban> getAllByUser(User user) {
    return banRepository.findAllByUser(user);
  }

  @Override
  public void delete(Ban ban) {
    banRepository.delete(ban);
  }

  @Override
  public void deleteById(Long id) {
    Ban temp = getById(id);
    delete(temp);
  }
}
