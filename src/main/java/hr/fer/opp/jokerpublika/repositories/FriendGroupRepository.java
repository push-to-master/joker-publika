package hr.fer.opp.jokerpublika.repositories;

import hr.fer.opp.jokerpublika.model.FriendGroup;
import hr.fer.opp.jokerpublika.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FriendGroupRepository extends JpaRepository<FriendGroup, Long> {

  List<FriendGroup> findAllByName(String name);

  List<FriendGroup> findAllByOwner(User owner);

  List<FriendGroup> findAllByUsersContains(User user);
}
