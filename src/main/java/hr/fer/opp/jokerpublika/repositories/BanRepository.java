package hr.fer.opp.jokerpublika.repositories;

import hr.fer.opp.jokerpublika.model.Ban;
import hr.fer.opp.jokerpublika.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface BanRepository extends JpaRepository<Ban, Long> {

  List<Ban> findAllByUser(User user);
}
