package hr.fer.opp.jokerpublika.repositories;

import hr.fer.opp.jokerpublika.model.PollDuration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PollDurationRepository extends JpaRepository<PollDuration, Long> {

  PollDuration findByValue(Long value);
}
