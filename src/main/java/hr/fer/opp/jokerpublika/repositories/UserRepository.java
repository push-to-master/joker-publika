package hr.fer.opp.jokerpublika.repositories;

import hr.fer.opp.jokerpublika.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

  List<User> findAllByAnonymous(boolean anonymous);

  User findByUsername(String username);

  User findByEmail(String email);

  User findByUsernameAndDeactivatedFalse(String username);
}
