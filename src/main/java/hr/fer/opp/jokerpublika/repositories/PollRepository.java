package hr.fer.opp.jokerpublika.repositories;

import hr.fer.opp.jokerpublika.model.FriendGroup;
import hr.fer.opp.jokerpublika.model.Poll;
import hr.fer.opp.jokerpublika.model.PollTypeEnum;
import hr.fer.opp.jokerpublika.model.User;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface PollRepository extends JpaRepository<Poll, Long> {

  List<Poll> findAllByTimeoutAfterAndPollTypeNotAndDeletedFalse(Timestamp now, PollTypeEnum pollType, Sort sort);

  List<Poll> findAllByUserAndDeletedFalse(User user, Sort sort);

  List<Poll> findAllByUserAndDeletedTrue(User user);

  List<Poll> findAllByUserAndPollTypeAndDeletedFalse(User user, PollTypeEnum pollType, Sort sort);

  List<Poll> findAllByUserAndTimeoutAfterAndDeletedFalse(User user, Timestamp now, Sort sort);

  List<Poll> findAllByUserAndTimeoutBeforeAndDeletedFalse(User user, Timestamp now, Sort sort);

  List<Poll> findAllByFriendGroupAndDeletedFalse(FriendGroup friendGroup, Sort sort);

  @Query("SELECT DISTINCT p FROM Poll p LEFT JOIN p.answerHistory a " +
      "WHERE p.deleted = false AND a.answer IS NOT null AND a.user = ?1")
  List<Poll> findAllAnsweredByUser(User user);

  @Query("SELECT DISTINCT p FROM Poll p LEFT JOIN p.user user " +
      "WHERE p.deleted = false AND p.timeout > current_timestamp AND user <> ?1 " +
      "AND ?1 NOT IN (SELECT a.user FROM AnswerHistory a WHERE a.poll = p)")
  List<Poll> findAllToAnswer(User user);
}
