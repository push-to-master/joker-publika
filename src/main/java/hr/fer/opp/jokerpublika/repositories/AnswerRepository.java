package hr.fer.opp.jokerpublika.repositories;

import hr.fer.opp.jokerpublika.model.Answer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnswerRepository extends JpaRepository<Answer, Long> {

}
