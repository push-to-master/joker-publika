package hr.fer.opp.jokerpublika.repositories;

import hr.fer.opp.jokerpublika.model.AnswerHistory;
import hr.fer.opp.jokerpublika.model.Poll;
import hr.fer.opp.jokerpublika.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnswerHistoryRepository extends JpaRepository<AnswerHistory, Long> {

  List<AnswerHistory> findAllByPoll(Poll poll);

  List<AnswerHistory> findAllByUser(User user);

  AnswerHistory findByPollAndUser(Poll poll, User user);
}
