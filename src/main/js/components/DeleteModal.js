import React from 'react';
import Modal from "react-bootstrap/Modal";
import ModalHeader from "react-bootstrap/ModalHeader";
import ModalTitle from "react-bootstrap/ModalTitle";
import ModalBody from "react-bootstrap/ModalBody";
import ModalFooter from "react-bootstrap/ModalFooter";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

function DeleteModal(props) {

  return (
      <Modal show={props.show} onHide={props.onHide}>
        <ModalHeader className="px-3 py-2">
          <ModalTitle className="h4">Delete poll</ModalTitle>
        </ModalHeader>
        <ModalBody className="text-lg">
          Are you sure you want to delete this poll?
        </ModalBody>
        <ModalFooter className="d-block px-3 py-2">
          <Row>
            <Col>
              <button className="btn btn-block btn-secondary" onClick={props.onHide}>Cancel</button>
            </Col>
            <Col>
              <button className="btn btn-block btn-danger" onClick={props.deletePoll}>Delete</button>
            </Col>
          </Row>
        </ModalFooter>
      </Modal>
  );
}

export default DeleteModal;
