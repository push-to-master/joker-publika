import PageContentWrapper from "./PageContentWrapper";
import React from "react";
import Ban from "./Ban";

function SeparateBanComponent() {

  return (
      <PageContentWrapper title={"Issue And Cancel Bans"}>
        <Ban usernameInQuestion=""/>
      </PageContentWrapper>
  );
}
export default SeparateBanComponent;
