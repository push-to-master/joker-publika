import React from 'react';
import "../../resources/static/css/login-home.css"
import ResponsiveColumn from "./ResponsiveColumn";
import Col from "react-bootstrap/Col";
import Alert from "react-bootstrap/Alert";
import Row from "react-bootstrap/Row";

function LoginHome() {

  return (
      <div className="bgi p-2 d-flex align-items-center">
        <ResponsiveColumn>
          <Row className="mb-5">
            <Col>
              <Alert className="alert-violet mb-5">
                <div className="text-lg">
                  <span className="h3">Let's get started!</span>
                </div>
                <div className="mt-3 mb-1">
                  <span className="h5">Please log in or register 😃</span>
                </div>
              </Alert>
            </Col>
          </Row>
        </ResponsiveColumn>
      </div>
  );
}

export default LoginHome;
