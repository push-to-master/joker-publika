import React, {Component} from "react";
import Row from "react-bootstrap/Row";
import ResponsiveColumn from "./ResponsiveColumn";
import FormGroup from "react-bootstrap/FormGroup";
import FormLabel from "react-bootstrap/FormLabel";
import InputGroup from "react-bootstrap/InputGroup";
import Button from "react-bootstrap/Button";
import ResponsiveForm from "./ResponsiveForm";
import CustomAlert from "./CustomAlert";

class Ban extends Component {

  constructor(props) {
    super(props);
    this.state = {
      success: "",
      currentStyle: "",
      profileStyle: "mb-1 text-left",
      separatePageStyle: "mb-1 text-right",
      errorMessage: "",
      showError: false,
      username: "",
      usernameInQuestion: this.props.usernameInQuestion,
      usernameToRemove: "",
      currentUsername: JSON.parse(localStorage.getItem("currentUser")).username,
      months: "",
      weeks: "",
      days: "",
      durations: {
        "1 day": "1",
        "1 week": "7",
        "1 month": "30",
        "3 months": "90",
        "6 months": "180",
        "1 year": "360",
        "PERMANENT": "permanent"
      },
      duration: ""
    }
  }

  componentDidMount() {
    if (this.state.usernameInQuestion === "") {
      this.setState({currentStyle: this.state.separatePageStyle})
    } else {
      this.setState({currentStyle: this.state.profileStyle})
    }
  }

  handleSubmitBan(event) {
    event.preventDefault();
    let toAdd;
    if (this.state.usernameInQuestion !== "") {
      toAdd = this.state.usernameInQuestion;
    } else {
      toAdd = this.state.username;
    }
    const data = new FormData();
    data.append('username', toAdd);
    if (toAdd !== this.state.currentUsername) {
      // let interval = String(this.state.months) + ":" + String(this.state.weeks) + ":" + String(this.state.days)
      let interval = this.state.duration;
      data.append('interval', interval);
      console.log("Posting new ban", data);
      fetch('/jokerpublika/ban', {
        method: 'POST',
        body: data
      }).then(result => {
        if (result.ok) {
          console.log("ban added");
          this.setState({showError: false});
          this.setState({success: "User banned"});
        } else if (result.status === 404) {
          console.log("user not found");
          this.setState({errorMessage: "User not found"});
          this.setState({showError: true});
        }
      });
    } else {
      this.setState({errorMessage: "You cannot ban yourself..."});
      this.setState({showError: true});
    }
  }

  handleSubmitRemoveBan(event) {
    event.preventDefault();
    let toRemove;
    if (this.state.usernameInQuestion !== "") {
      toRemove = this.state.usernameInQuestion;
    } else {
      toRemove = this.state.usernameToRemove;
    }
    if (this.state.usernameToRemove !== this.state.currentUsername) {
      fetch('/jokerpublika/ban/remove_active_ban', {
        method: 'POST',
        body: toRemove
      }).then(result => {
        if (result.ok) {
          console.log("ban removed");
          this.setState({showError: false});
          this.setState({success: "Active ban removed"});
        } else if (result.status === 404) {
          console.log("user not found");
          this.setState({errorMessage: "User not found"});
          this.setState({showError: true});
        } else {
          this.setState({errorMessage: "User doesn't have active ban"});
          this.setState({showError: true});
        }
      });
    }
  }

  onUsernameChange(event) {
    this.setState({username: event.target.value});
  }

  onUsernameToRemoveChange(event) {
    this.setState({usernameToRemove: event.target.value});
  }

  onDurationChange(event) {
    this.setState({duration: event.target.value});
  }

  render() {
    return (
        <div>
          <ResponsiveColumn>
            {this.state.success && <CustomAlert color="success" header="Success!"
                                                body={this.state.success}/>}
            {this.state.showError && <CustomAlert color="danger" header="Error!"
                                                  body={this.state.errorMessage}/>}
          </ResponsiveColumn>
          <ResponsiveForm onSubmit={this.handleSubmitBan.bind(this)}>
            <Row>
              <FormGroup className="col mb-2">
                <FormLabel className="text-lg text-violet" htmlFor="add ban">Ban user</FormLabel>
                {this.state.usernameInQuestion === "" && <InputGroup className="mb-1">
                  <input type="text" className="form-control" id="username"
                         maxLength="32" minLength="2" required
                         name="username" onChange={this.onUsernameChange.bind(this)}
                         placeholder="Username"/>
                </InputGroup>}
                <InputGroup>
                  <select className="form-control custom-select" id="duration"
                          name="duration" required
                          onChange={this.onDurationChange.bind(this)}>
                    <option hidden value="">Select duration...</option>
                    {Object.entries(this.state.durations).map(([k, v]) =>
                        <option key={k} value={v}>{k}</option>
                    )}
                  </select>
                </InputGroup>
              </FormGroup>
            </Row>
            <Row className={this.state.currentStyle}>
              <FormGroup className="col">
                <Button className="btn-info" type="submit">Submit ban</Button>
              </FormGroup>
            </Row>
          </ResponsiveForm>

          <ResponsiveForm onSubmit={this.handleSubmitRemoveBan.bind(this)}>
            <Row>
              <FormGroup className="col mb-2">
                <FormLabel className="text-lg text-violet" htmlFor="remove ban">Remove active ban</FormLabel>
                {this.state.usernameInQuestion === "" && <InputGroup>
                  <input type="text" className="form-control" id="usernameToRemove"
                         maxLength="32" minLength="2" required
                         name="username" onChange={this.onUsernameToRemoveChange.bind(this)}
                         placeholder="Username"/>
                </InputGroup>}
              </FormGroup>
            </Row>
            <Row className={this.state.currentStyle}>
              <FormGroup className="col">
                <Button className="btn-info" type="submit">Remove ban</Button>
              </FormGroup>
            </Row>
          </ResponsiveForm>
        </div>
    );

  }
}

export default Ban;
