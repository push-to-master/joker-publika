import React from 'react';
import Col from "react-bootstrap/Col";
import Alert from "react-bootstrap/Alert";
import Row from "react-bootstrap/Row";

function CustomAlert(props) {

  return (
      <Row>
        <Col>
          <Alert className={"alert-" + props.color}>
            <div className="text-lg">{props.header}</div>
            <div className="mt-3 mb-1">{props.body}</div>
          </Alert>
        </Col>
      </Row>
  );
}

export default CustomAlert;
