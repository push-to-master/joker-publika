import React from 'react';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import 'react-bootstrap-typeahead/css/Typeahead-bs4.css';
import List from '@material-ui/core/List';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import Collapse from '@material-ui/core/Collapse';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import {makeStyles} from '@material-ui/core/styles';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {Link, useHistory} from "react-router-dom";
import {GroupContext, UserContext} from "../App";
import {Typeahead} from "react-bootstrap-typeahead";

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  drawerPaper: {
    width: drawerWidth,
    backgroundColor: "#d1afef",
    backgroundImage: "linear-gradient(180deg, #d1afef 10%, var(--color-lavender) 100%)",
    backgroundSize: "cover"
  }
}));

function Sidebar(props) {

  const history = useHistory();

  const {container} = props;
  const classes = useStyles();
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [openGroups, setOpenGroups] = React.useState(false);
  const currentUser = JSON.parse(localStorage.getItem("currentUser"));
  const groups = React.useContext(GroupContext).groups;
  const publicUsers = React.useContext(UserContext).publicUsers;
  const anonymousUsers = React.useContext(UserContext).anonymousUsers;
  const fetchGroups = React.useContext(GroupContext).fetchGroupData;
  const fetchPublicUsers = React.useContext(UserContext).fetchAllPublicUsers;
  const fetchAnonymousUsers = React.useContext(UserContext).fetchAllAnonymousUsers;
  const users = currentUser.roles.includes("ADMIN") ? publicUsers.concat(anonymousUsers) : publicUsers;

  const onOpenSidebar = () => {
    fetchGroups();
    fetchPublicUsers();
    handleDrawerToggle();
    if (currentUser.roles.includes("ADMIN")) {
      fetchAnonymousUsers();
    }
  };

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  function getMenuArrow() {
    return openGroups ? "angle-down" : "angle-right";
  }

  function mapFriendGroups() {
    return (
        groups.map(group => (
            <React.Fragment key={group.id}>
              <Link onClick={handleDrawerToggle} key={group.id} to={"/poll/group/" + group.id}
                    className="sidebar-item text-wrap text-break">
                {group.name}
              </Link>
            </React.Fragment>
        ))
    );
  }

  function displayFriendGroupList() {
    return (
        <React.Fragment>
          <List>
            <ListItem button key="myGroups" onClick={() => setOpenGroups(!openGroups)}
                      aria-expanded={openGroups} aria-controls="groupPages">
              <ListItemIcon>
                <FontAwesomeIcon icon="users" className="text-amethyst fa-fw mr-1"/>
              </ListItemIcon>
              <ListItemText className="sidebar-menu">My Groups</ListItemText>
              <div className="ml-auto">
                <FontAwesomeIcon className="text-amethyst" icon={getMenuArrow()}/>
              </div>
            </ListItem>
            <Collapse in={openGroups}>
              <div id="groupPages" className="text-violet bg-white rounded collapse-inner mx-2 p-2">
                <Link onClick={handleDrawerToggle} className="btn btn-block btn-secondary p-2"
                      to='/group/new'>
                  <FontAwesomeIcon icon="plus" className="fa-fw mr-2"/>
                  Create Group
                </Link>
                {groups.length > 0 && <div className="mt-2">{mapFriendGroups()}</div>}
              </div>
            </Collapse>
          </List>

          <Divider/>
        </React.Fragment>
    );
  }

  const drawer = (
      <React.Fragment>

        <div>
          <div className="bg-gradient-violet text-center py-2" style={{height: "50px"}}>
            <span className="font-italic align-middle text-white">
              Bad decisions make good stories
            </span>
          </div>

          <Divider/>

          <Typeahead
              id="user-search"
              className="m-1"
              minLength={1}
              selectHintOnEnter={true}
              labelKey="name"
              options={users.map(user => user.username)}
              placeholder="Search users..."
              onChange={(selected) => {
                if (selected.length !== 0) {
                  history.push("/user/" + selected);
                }
              }}
          />

          <Divider/>

          {!currentUser.anonymous && displayFriendGroupList()}

          <List>
            <Link to="/poll/results" className="sidebar-menu">
              <ListItem button onClick={handleDrawerToggle} key="newResults">
                <ListItemIcon>
                  <FontAwesomeIcon icon="exclamation" className="text-amethyst fa-fw mr-1"/>
                </ListItemIcon>
                <ListItemText>New Results</ListItemText>
              </ListItem>
            </Link>

            <Link to="/poll/active" className="sidebar-menu">
              <ListItem button onClick={handleDrawerToggle} key="myActivePolls">
                <ListItemIcon>
                  <FontAwesomeIcon icon={["far", "clock"]} className="text-amethyst fa-fw mr-1"/>
                </ListItemIcon>
                <ListItemText>My Active Polls</ListItemText>
              </ListItem>
            </Link>
          </List>

          <Divider/>

          <List>
            <Link to="/poll/followed" className="sidebar-menu">
              <ListItem button onClick={handleDrawerToggle} key="followedPolls">
                <ListItemIcon>
                  <FontAwesomeIcon icon={["far", "star"]} className="text-amethyst fa-fw mr-1"/>
                </ListItemIcon>
                <ListItemText>Followed Polls</ListItemText>
              </ListItem>
            </Link>

            <Link to="/poll/current" className="sidebar-menu">
              <ListItem button onClick={handleDrawerToggle} key="currentPublicPolls">
                <ListItemIcon>
                  <FontAwesomeIcon icon={["far", "list-alt"]} className="text-amethyst fa-fw mr-1"/>
                </ListItemIcon>
                <ListItemText>Current Public Polls</ListItemText>
              </ListItem>
            </Link>
          </List>
        </div>

        <footer className="footer mt-auto p-2">
          <Link onClick={handleDrawerToggle} className="btn btn-block text-lg text-white bg-gradient-violet mt-2 p-2"
                to='/poll/new'>
            <FontAwesomeIcon icon="pencil-alt" className="fa-fw mr-3"/>
            New Poll
          </Link>
          <Link onClick={handleDrawerToggle} className="btn btn-block text-lg text-white bg-gradient-violet mt-2 p-2"
                to='/poll/answer-polls'>
            <FontAwesomeIcon icon="tasks" className="fa-fw mr-3"/>
            Answer Polls
          </Link>
        </footer>

      </React.Fragment>
  );

  return (
      <div className="d-flex">
        <FontAwesomeIcon onClick={onOpenSidebar} style={{cursor: "pointer"}}
                         className="fa-lg text-white" icon="bars"/>

        {/*<nav className={classes.drawer}>*/}
        {/*  <div>*/}
        <Hidden>
          <Drawer
              container={container}
              variant="temporary"
              anchor={'left'}
              open={mobileOpen}
              onClose={handleDrawerToggle}
              classes={{
                paper: classes.drawerPaper,
              }}
              ModalProps={{
                keepMounted: true, // Better open performance on mobile.
              }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        {/*<Hidden xsDown implementation="css">*/}
        {/*  <Drawer*/}
        {/*      classes={{*/}
        {/*        paper: classes.drawerPaper,*/}
        {/*      }}*/}
        {/*      variant="permanent"*/}
        {/*      open*/}
        {/*  >*/}
        {/*    {drawer}*/}
        {/*  </Drawer>*/}
        {/*</Hidden>*/}
        {/*</div>*/}
        {/*</nav>*/}
      </div>
  );
}

export default Sidebar;
