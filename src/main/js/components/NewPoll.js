import React from 'react';
import {withRouter} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import FormGroup from "react-bootstrap/FormGroup";
import FormLabel from "react-bootstrap/FormLabel";
import InputGroup from "react-bootstrap/InputGroup";
import Row from "react-bootstrap/Row";
import Button from "react-bootstrap/Button";
import PageContentWrapper from "./PageContentWrapper";
import ResponsiveForm from "./ResponsiveForm";
import imageCompression from 'browser-image-compression';
import CustomAlert from "./CustomAlert";

class NewPoll extends React.Component {

  constructor(props) {
    super(props);
    this.durationSelect = React.createRef();
    this.alertRef = React.createRef();
    this.state = {
      user: JSON.parse(localStorage.getItem("currentUser")),
      durations: [],
      friendGroups: [],
      friendGroupType: JSON.parse(localStorage.getItem("currentUser")).defaultPollType === "FRIEND_GROUP",
      imageFile1: "Choose first image file",
      imageFile2: "Choose second image file",
      errorMessage: "",
      displayLoading: false,

      question: "",
      answer1: "",
      answer2: "",
      duration: "",
      pollType: JSON.parse(localStorage.getItem("currentUser")).defaultPollType,
      friendGroup: "",
      image1: "",
      image2: ""
    };
    this.checkPollType = this.checkPollType.bind(this);
    this.displayImage1Filename = this.displayImage1Filename.bind(this);
    this.displayImage2Filename = this.displayImage2Filename.bind(this);
    this.onImage1Change = this.onImage1Change.bind(this);
    this.onImage2Change = this.onImage2Change.bind(this);
    this.saveImageFile = this.saveImageFile.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  scrollToAlert() {
    this.alertRef.current.scrollIntoView({behavior: "smooth"});
  }

  componentDidMount() {
    fetch('/jokerpublika/poll/duration')
        .then(response => response.json())
        .then(response => this.setState({durations: response}));
    fetch('/jokerpublika/group/user?userId=' + this.state.user.id)
        .then(response => response.json())
        .then(response => this.setState({friendGroups: response}));
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (!this.state.duration && this.state.durations.length > 0) {
      this.durationSelect.current.value = this.state.user.defaultPollDuration || "";
      this.state.duration = this.state.user.defaultPollDuration;
    }
  }

  checkPollType(event) {
    if (event.target.value === "FRIEND_GROUP") {
      this.setState({friendGroupType: true});
    } else {
      this.setState({friendGroupType: false});
    }
    this.onChange(event);
  }

  displayImage1Filename(path) {
    if (path) {
      this.setState({imageFile1: path.split('\\').pop().split('/').pop()});
    } else {
      this.setState({imageFile1: "Choose first image file"});
    }
  }

  displayImage2Filename(path) {
    if (path) {
      this.setState({imageFile2: path.split('\\').pop().split('/').pop()});
    } else {
      this.setState({imageFile2: "Choose second image file"});
    }
  }

  onImage1Change(event) {
    this.saveImageFile(event, this.displayImage1Filename);
  }

  onImage2Change(event) {
    this.saveImageFile(event, this.displayImage2Filename);
  }

  saveImageFile(event, displayImageFilename) {
    const image = event.target.files[0];
    if (image) {
      this.setState({displayLoading: true});
      this.setState({errorMessage: ""});
      const {name} = event.target;
      const path = event.target.value.toString();
      if (image.type === "image/gif") {
        if (image.size <= 1024 * 1024) {
          this.setState({displayLoading: false});
          displayImageFilename(path);
          this.setState({
            [name]: image
          });
        } else {
          this.setState({displayLoading: false});
          displayImageFilename(null);
          this.setState({errorMessage: "Your GIF is too large and cannot be compressed"});
          this.scrollToAlert();
        }
      } else {
        const options = {
          maxSizeMB: 1,
          maxWidthOrHeight: 1920,
          useWebWorker: true
        };
        imageCompression(image, options)
            .then((compressedImage) => {
              this.setState({displayLoading: false});
              displayImageFilename(path);
              this.setState({
                [name]: compressedImage
              });
            })
            .catch((error) => {
              console.log(error.message);
              this.setState({displayLoading: false});
              displayImageFilename(null);
              this.setState({errorMessage: "Error while compressing image file"});
              this.scrollToAlert();
            });
      }
    } else {
      displayImageFilename(null);
    }
  }

  onChange(event) {
    const {name, value} = event.target;
    this.setState({
      [name]: value
    });
  }

  onSubmit(e) {
    e.preventDefault();

    const data = new FormData();
    const info = {
      question: this.state.question,
      answer1: {text: this.state.answer1},
      answer2: {text: this.state.answer2},
      duration: this.state.duration,
      friendGroup: {id: this.state.friendGroup},
      pollType: this.state.pollType
    };

    data.append('image1', this.state.image1);
    data.append('image2', this.state.image2);
    data.append('info', new Blob([JSON.stringify(info)], {type: "application/json"}));

    console.log("Posting new poll", data);
    this.setState({errorMessage: ""});

    fetch('/jokerpublika/poll', {
      method: "POST",
      body: data
    }).then(result => {
      if (result.ok) {
        this.props.history.push("/");
      }
    });
  }

  render() {
    window.onbeforeunload = function () {
      return false;
    };

    return (
        <PageContentWrapper title={"New Poll"}>
          <ResponsiveForm onSubmit={this.onSubmit}>

            <div ref={this.alertRef}>
              {this.state.errorMessage && <CustomAlert color="danger" header="Error!"
                                                       body={this.state.errorMessage}/>}
            </div>

            <Row>
              <FormGroup className="col">
                <FormLabel className="text-lg text-violet" htmlFor="question">Question</FormLabel>
                <InputGroup>
                  <textarea className="form-control" id="question"
                            maxLength="512" rows="4" style={{resize: "none"}}
                            name="question" required onChange={this.onChange}
                            placeholder="What decision do you need help with?">
                  </textarea>
                  <div className="input-group-append">
                    <span className="input-group-text rounded-right text-violet">
                      <FontAwesomeIcon icon="question"/>
                    </span>
                  </div>
                </InputGroup>
              </FormGroup>
            </Row>

            <Row>
              <FormGroup className="col-sm-6">
                <FormLabel className="text-lg text-violet" htmlFor="answer1">First answer</FormLabel>
                <InputGroup>
                  <textarea className="form-control" id="answer1"
                            maxLength="128" rows="2" style={{resize: "none"}}
                            name="answer1" required onChange={this.onChange}
                            placeholder="First answer">
                  </textarea>
                  <div className="input-group-append">
                    <span className="input-group-text rounded-right text-violet">
                      <FontAwesomeIcon icon="comment-dots"/>
                    </span>
                  </div>
                </InputGroup>
              </FormGroup>

              <FormGroup className="col-sm-6">
                <FormLabel className="text-lg text-violet" htmlFor="answer2">Second answer</FormLabel>
                <InputGroup>
                  <textarea className="form-control" id="answer2"
                            maxLength="128" rows="2" style={{resize: "none"}}
                            name="answer2" required onChange={this.onChange}
                            placeholder="Second answer">
                  </textarea>
                  <div className="input-group-append">
                    <span className="input-group-text rounded-right text-violet">
                      <FontAwesomeIcon style={{transform: "scaleX(-1)"}} icon="comment-dots"/>
                    </span>
                  </div>
                </InputGroup>
              </FormGroup>
            </Row>

            <Row>
              <FormGroup className="col-sm-6">
                <FormLabel className="text-lg text-violet" htmlFor="duration">Poll duration</FormLabel>
                <InputGroup>
                  <select className="form-control custom-select" id="duration"
                          name="duration" required ref={this.durationSelect}
                          defaultValue={this.state.user.defaultPollDuration}
                          onChange={this.onChange}>
                    <option hidden value="">Select duration...</option>
                    {this.state.durations.map(duration =>
                        <option key={duration.value} value={duration.value}>{duration.label}</option>
                    )}
                  </select>
                  <div className="input-group-append">
                    <span className="input-group-text rounded-right text-violet">
                      <FontAwesomeIcon icon={["far", "clock"]}/>
                    </span>
                  </div>
                </InputGroup>
              </FormGroup>

              <FormGroup className="col-sm-6">
                <FormLabel className="text-lg text-violet" htmlFor="poll-type">Poll type</FormLabel>
                <InputGroup>
                  <select className="form-control custom-select" id="poll-type"
                          name="pollType" required
                          defaultValue={this.state.user.defaultPollType}
                          onChange={this.checkPollType}>
                    <option hidden value="">Select poll type...</option>
                    {!this.state.user.anonymous && <option key="PUBLIC" value="PUBLIC">Public</option>}
                    <option key="ANONYMOUS" value="ANONYMOUS">Anonymous</option>
                    {!this.state.user.anonymous &&
                    <option key="FRIEND_GROUP" value="FRIEND_GROUP">Friend group</option>}
                  </select>
                  <div className="input-group-append">
                    <span className="input-group-text rounded-right text-violet">
                      <FontAwesomeIcon icon="user-friends"/>
                    </span>
                  </div>
                </InputGroup>
              </FormGroup>
            </Row>

            {this.state.friendGroupType && <Row>
              <FormGroup className="col">
                <FormLabel className="text-lg text-violet" htmlFor="friend-group">Friend group</FormLabel>
                <InputGroup>
                  <select className="form-control custom-select" id="friend-group"
                          name="friendGroup" required onChange={this.onChange}>
                    <option hidden value="">Select friend group...</option>
                    {this.state.friendGroups.map(friendGroup =>
                        <option key={friendGroup.id} value={friendGroup.id}>{friendGroup.name}</option>
                    )}
                  </select>
                  <div className="input-group-append">
                    <span className="input-group-text rounded-right text-violet">
                      <FontAwesomeIcon icon="users"/>
                    </span>
                  </div>
                </InputGroup>
              </FormGroup>
            </Row>}

            <h4 className="mt-3 mb-2">
              <span className="text-lavender">Optional*</span>
            </h4>

            <Row>
              <FormGroup className="col-sm-6">
                <FormLabel className="text-lg text-violet" htmlFor="image1">First image</FormLabel>
                <InputGroup>
                  <div className="custom-file">
                    <input type="file" id="image1" className="custom-file-input"
                           name="image1" accept="image/*"
                           onChange={this.onImage1Change}/>
                    <label className="custom-file-label" htmlFor="image1">{this.state.imageFile1}</label>
                  </div>
                  <div className="input-group-append">
                  <span className="input-group-text rounded-right text-violet">
                    <FontAwesomeIcon icon="file-image"/>
                  </span>
                  </div>
                </InputGroup>
              </FormGroup>

              <FormGroup className="col-sm-6">
                <FormLabel className="text-lg text-violet" htmlFor="image2">Second image</FormLabel>
                <InputGroup>
                  <div className="custom-file">
                    <input type="file" id="image2" className="custom-file-input"
                           name="image2" accept="image/*"
                           onChange={this.onImage2Change}/>
                    <label className="custom-file-label" htmlFor="image2">{this.state.imageFile2}</label>
                  </div>
                  <div className="input-group-append">
                    <span className="input-group-text rounded-right text-violet">
                      <FontAwesomeIcon icon="file-image"/>
                    </span>
                  </div>
                </InputGroup>
              </FormGroup>
            </Row>

            {this.state.displayLoading
            && <Row className="mt-2">
              <img alt="Loading..." src={"/jokerpublika/img/loading.gif"}
                   className="img-fluid mx-auto"
                   style={{height: "100px"}}/>
            </Row>}

            <Row className="my-4">
              <FormGroup className="col-8 col-sm-7 col-md-6 col-lg-5 col-xl-4 mx-auto">
                <Button className="btn-block btn-info btn-lg" type="submit"
                        disabled={this.state.displayLoading}>
                  Submit Poll!
                </Button>
              </FormGroup>
            </Row>

          </ResponsiveForm>
        </PageContentWrapper>
    );
  }

}

export default withRouter(NewPoll);
