import React from "react";
import PageContentWrapper from "./PageContentWrapper";
import ResponsiveForm from "./ResponsiveForm";
import FormGroup from "react-bootstrap/FormGroup";
import FormLabel from "react-bootstrap/FormLabel";
import InputGroup from "react-bootstrap/InputGroup";
import Row from "react-bootstrap/Row";
import Button from "react-bootstrap/Button";
import ResponsiveColumn from "./ResponsiveColumn";
import CustomAlert from "./CustomAlert";

function ApplicationSettings() {

  const [durations, setDurations] = React.useState([]);
  const [duration, setDuration] = React.useState(null);
  const [success, setSuccess] = React.useState("");
  const [errorMessage, setErrorMessage] = React.useState("");
  const [showError, setShowError] = React.useState(false);
  const [days, setDays] = React.useState(null);
  const [hours, setHours] = React.useState(null);
  const [minutes, setMinutes] = React.useState(null);

  React.useEffect(() => {
    fetchDurations();
  }, []);

  function fetchDurations() {
    fetch('/jokerpublika/poll/duration')
        .then(response => response.json())
        .then(data => setDurations(data));
  }

  function onChangeRemove(event) {
    setDuration(event.target.value);
  }

  function onChangeDays(event) {
    setDays(event.target.value);
  }

  function onChangeHours(event) {
    setHours(event.target.value);
  }

  function onChangeMinutes(event) {
    setMinutes(event.target.value);
  }

  function submitRemoval(e) {
    e.preventDefault();
    const options = {
      method: "DELETE",
    };
    fetch('/jokerpublika/poll/duration?duration=' + duration, options)
        .then(result => {
          if (result.ok) {
            fetchDurations();
            setSuccess("Removed selected poll duration option from app");
            setShowError(false);
          } else {
            setSuccess("");
            setErrorMessage("An error occurred while removing the poll duration option");
            setShowError(true);
          }
        });
  }

  function submitNewDuration(e) {
    e.preventDefault();
    const duration = ((+days * 24 + +hours) * 60 + +minutes) * 60000;
    const options = {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(duration)
    };
    fetch('/jokerpublika/poll/duration', options)
        .then(result => {
          if (result.ok) {
            fetchDurations();
            setSuccess("Added new poll duration option to app");
            setShowError(false);
          } else {
            setSuccess("");
            setErrorMessage("Poll duration already exists");
            setShowError(true);
          }
        });
  }

  return (
      <PageContentWrapper title={"Application Settings"}>

        <ResponsiveColumn>
          {success && <CustomAlert color="success" header="Success!"
                                   body={success}/>}
          {showError && <CustomAlert color="danger" header="Error!"
                                     body={errorMessage}/>}
        </ResponsiveColumn>

        <ResponsiveForm onSubmit={submitRemoval}>
          <Row>
            <FormGroup className="col mb-2">
              <FormLabel className="text-lg text-violet" htmlFor="duration">Remove poll duration option</FormLabel>
              <InputGroup>
                <select className="form-control custom-select" id="duration"
                        name="duration" required onChange={onChangeRemove}>
                  <option hidden value="">Select duration...</option>
                  {durations.map(duration =>
                      <option key={duration.value} value={duration.value}>{duration.label}</option>
                  )}
                </select>
              </InputGroup>
            </FormGroup>
          </Row>
          <Row className="mb-1 text-right">
            <FormGroup className="col">
              <Button className="btn-info" type="submit">Remove duration</Button>
            </FormGroup>
          </Row>
        </ResponsiveForm>

        <ResponsiveForm onSubmit={submitNewDuration}>
          <Row>
            <FormGroup className="col mb-2">
              <FormLabel className="text-lg text-violet" htmlFor="duration">Add poll duration option</FormLabel>
              <InputGroup>
                <input className="form-control" type="number" id="days" name="days"
                       min="0" max="365" required
                       placeholder="Days" onChange={onChangeDays}/>
                <input className="form-control" type="number" id="hours" name="hours"
                       min="0" max="23" required
                       placeholder="Hours" onChange={onChangeHours}/>
                <input className="form-control" type="number" id="minutes" name="minutes"
                       min="0" max="59" required
                       placeholder="Minutes" onChange={onChangeMinutes}/>
              </InputGroup>
            </FormGroup>
          </Row>
          <Row className="mb-1 text-right">
            <FormGroup className="col">
              <Button className="btn-info" type="submit">Add duration</Button>
            </FormGroup>
          </Row>
        </ResponsiveForm>

      </PageContentWrapper>
  );
}

export default ApplicationSettings;
