import React from "react";
import PageContentWrapper from "./PageContentWrapper";
import ResponsiveColumn from "./ResponsiveColumn";
import PollCard from "./PollCard";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import CustomAlert from "./CustomAlert";
import GroupSettings from "./GroupSettings";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

function GroupPolls(props) {

  const [polls, setPolls] = React.useState([]);
  const [displayLoading, setDisplayLoading] = React.useState(true);
  const group = props.group;
  const user = JSON.parse(localStorage.getItem("currentUser"));

  React.useEffect(() => {
    const get = () => {
      fetch('/jokerpublika/poll/group?groupId=' + group.id)
          .then(response => response.json())
          .then(response => {
            setPolls(response);
            setDisplayLoading(false);
          });
    };
    get();
    const interval = setInterval(get, 10000);
    return () => clearInterval(interval);
  }, [group]);

  function deletePoll(pollId) {
    setPolls((oldPolls) => oldPolls.filter(poll => poll.id !== pollId));
  }

  function getGroupName() {
    return (
        <React.Fragment>
          <FontAwesomeIcon icon="users" className="fa-fw"/>
          <span className="ml-2 font-italic">{group.name}</span>
        </React.Fragment>
    );
  }

  return (
      <PageContentWrapper title={getGroupName()}>
        <ResponsiveColumn>
          <Row className="text-center">
            <Col>
              <GroupSettings group={group} user={user}/>
            </Col>
          </Row>
          <hr/>
          {polls.length === 0
          && <CustomAlert color={"warning"} header={"It's quiet here..."}
                          body={"No one from the group posted a poll yet 😶"}/>}
          {displayLoading
          && <Row className="mt-4">
            <img alt="Loading..." src={"/jokerpublika/img/loading.gif"}
                 className="img-fluid mx-auto"
                 style={{height: "100px"}}/>
          </Row>}
          {polls.map(poll =>
              <div key={poll.id} className="my-3">
                <PollCard key={poll.id} poll={poll} hideGroup={true} deletePoll={deletePoll}/>
              </div>
          )}
        </ResponsiveColumn>
      </PageContentWrapper>
  );

}

export default GroupPolls;
