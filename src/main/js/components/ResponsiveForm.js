import React from "react";

function ResponsiveForm(props) {

  return (
      <form className="col-md-10 col-lg-8 col-xl-6 mx-auto" onSubmit={props.onSubmit}>

        {props.children}

      </form>
  );
}

export default ResponsiveForm;
