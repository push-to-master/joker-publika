import React from "react";

function ResponsiveColumn(props) {

  return (
      <div className="col-md-10 col-lg-8 col-xl-6 mx-auto">

        {props.children}

      </div>
  );
}

export default ResponsiveColumn;
