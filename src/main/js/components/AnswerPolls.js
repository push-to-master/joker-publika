import React from 'react';
import ResponsiveColumn from "./ResponsiveColumn";
import PollCard from "./PollCard";
import Row from "react-bootstrap/Row";
import PageContentWrapper from "./PageContentWrapper";
import FormGroup from "react-bootstrap/FormGroup";
import Button from "react-bootstrap/Button";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import CustomAlert from "./CustomAlert";

function AnswerPolls() {

  const [poll, setPoll] = React.useState(null);
  const [displayLoading, setDisplayLoading] = React.useState(true);

  React.useEffect(() => {
    fetchPolls();
  }, []);

  function fetchPolls() {
    fetch('/jokerpublika/poll/get-answer-polls')
        .then(response => response.json(), console.error)
        .then(data => {
          setPoll(data[0]);
          setDisplayLoading(false);
        });
  }

  function delayNextPoll() {
    setTimeout(fetchPolls, 3000);
  }

  function skipPoll() {
    fetch('/jokerpublika/poll/answer/skip?pollId=' + poll.id)
        .then(response => console.log(response.ok ? "Skipped poll" : "Failed to skip poll"))
        .then(fetchPolls);
  }

  function displayPollCard() {
    return (
        <React.Fragment>
          <div key={poll.id} className="my-3">
            <PollCard key={poll.id} poll={poll} fetchPolls={delayNextPoll}/>
          </div>
          <Row className="my-4">
            <FormGroup className="col-8 col-sm-7 col-md-6 col-lg-5 col-xl-4 mx-auto">
              <Button className="btn-block btn-secondary" onClick={skipPoll}>
                Skip poll
                <FontAwesomeIcon icon="forward" className="fa-fw ml-3"/>
              </Button>
            </FormGroup>
          </Row>
        </React.Fragment>
    );
  }

  function displayNoPollsMessage() {
    return (
        <CustomAlert color={"info"} header={"Thank you!"}
                     body={"There are no more polls left to answer 😊"}/>
    );
  }

  return (
      <PageContentWrapper title={"Answer Polls"}>
        <ResponsiveColumn>
          {poll && displayPollCard()}
          {!poll && displayNoPollsMessage()}
          {displayLoading
          && <Row className="mt-4">
            <img alt="Loading..." src={"/jokerpublika/img/loading.gif"}
                 className="img-fluid mx-auto"
                 style={{height: "100px"}}/>
          </Row>}
        </ResponsiveColumn>
      </PageContentWrapper>
  );

}

export default AnswerPolls;
