import Modal from "react-bootstrap/Modal";
import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import ModalHeader from "react-bootstrap/ModalHeader";
import ModalTitle from "react-bootstrap/ModalTitle";
import ModalBody from "react-bootstrap/ModalBody";
import ModalFooter from "react-bootstrap/ModalFooter";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import {Link, useHistory} from "react-router-dom";
import InputGroup from "react-bootstrap/InputGroup";
import FormGroup from "react-bootstrap/FormGroup";
import CustomAlert from "./CustomAlert";
import {GroupContext} from "../App";
import FormLabel from "react-bootstrap/FormLabel";

function GroupSettings(props) {

  const history = useHistory();

  const [isOpen, setIsOpen] = React.useState(false);
  const group = props.group;
  const currentUser = props.user;
  const alertRef = React.useRef();

  const [name, setName] = React.useState("");
  const [newUser, setNewUser] = React.useState("");
  const [newOwner, setNewOwner] = React.useState("");
  const [error, setError] = React.useState(false);

  const fetchGroupData = React.useContext(GroupContext).fetchGroupData;

  const showModal = () => {
    setIsOpen(true);
  };

  const hideModal = () => {
    setIsOpen(false);
  };

  function scrollToAlert() {
    alertRef.current.scrollIntoView({behavior: "smooth"});
  }

  function onNameChange(event) {
    setName(event.target.value);
  }

  function onUsersUsernameChange(event) {
    setNewUser(event.target.value);
  }

  function onNewOwnerChange(event) {
    setNewOwner(event.target.value);
  }

  function onRename(e) {
    e.preventDefault();
    fetch('/jokerpublika/group/rename?groupId=' + group.id + "&name=" + name)
        .then(result => {
          if (result.ok) {
            fetchGroupData();
            setError(false);
          }
        });
  }

  function onAddUser(e) {
    e.preventDefault();
    fetch('/jokerpublika/group/add-user?groupId=' + group.id + "&username=" + newUser)
        .then(result => {
          if (result.ok) {
            fetchGroupData();
            setError(false);
          } else {
            setError(true);
            scrollToAlert();
          }
        });
  }

  function onRemoveUser(event) {
    const userId = event.currentTarget.getAttribute('data-id');
    let numberOfUsers = group.users.length;
    fetch('/jokerpublika/group/remove-user?groupId=' + group.id + "&userId=" + userId)
        .then(result => {
          if (result.ok) {
            if (numberOfUsers <= 2) {
              history.push("/");
            }
            fetchGroupData();
            setError(false);
          }
        });
  }

  function onLeave() {
    if (currentUser.id === group.owner.id) {
      fetch('/jokerpublika/group/leave-owner?groupId=' + group.id + "&newOwnerId=" + newOwner)
          .then(result => {
            if (result.ok) {
              history.push("/");
              fetchGroupData();
            }
          });
    } else {
      fetch('/jokerpublika/group/leave?groupId=' + group.id)
          .then(result => {
            if (result.ok) {
              history.push("/");
              fetchGroupData();
            }
          });
    }
  }

  function onDeleteGroup() {
    fetch('/jokerpublika/group/delete?groupId=' + group.id)
        .then(result => {
          if (result.ok) {
            history.push("/");
            fetchGroupData();
          }
        });
  }

  function getUserSideElement(user) {
    if (currentUser.id === group.owner.id && user.id !== group.owner.id) {
      return (
          <button className="btn btn-circle btn-sm btn-google"
                  onClick={onRemoveUser} data-id={user.id}>
            <FontAwesomeIcon icon="user-minus"/>
          </button>
      );
    } else if (user.id === group.owner.id) {
      return (
          <FontAwesomeIcon className="mr-1 text-warning" icon="crown"/>
      );
    }
  }

  return (
      <React.Fragment>

        <button className="btn btn-primary" onClick={showModal}>
          <FontAwesomeIcon className="mr-2" icon="cog"/>
          Group settings
        </button>

        <Modal show={isOpen} onHide={hideModal}>
          <ModalHeader className="px-3 py-2">
            <ModalTitle className="h4 text-break">Settings for
              <span className="font-italic">&nbsp;{group.name}</span>
            </ModalTitle>
          </ModalHeader>
          <ModalBody>

            <div ref={alertRef}>
              {error
              && <CustomAlert color={"danger"} header={"Error while adding user!"}
                              body={"Make sure that the added user exists and is not anonymous 🙄"}/>}
            </div>

            {currentUser.id === group.owner.id
            && <React.Fragment>
              <form onSubmit={onRename}>
                <Row className="align-items-center">
                  <FormGroup className="col pr-2">
                    <FormLabel className="text-lg text-violet" htmlFor="name">Change group name</FormLabel>
                    <InputGroup>
                      <input className="form-control" id="name"
                             name="name" required maxLength="128"
                             placeholder="New group name"
                             onChange={onNameChange}/>
                    </InputGroup>
                  </FormGroup>
                  <Col className="col-auto pl-1">
                    <button type="submit" className="mt-3 btn btn-circle btn-info">
                      <FontAwesomeIcon icon="pencil-alt"/>
                    </button>
                  </Col>
                </Row>
              </form>
              <hr/>
            </React.Fragment>}

            <Row>
              <Col>
                <span className="h4 text-violet">Users:</span>
              </Col>
            </Row>
            <ul>
              {group.users.map(user =>
                  <li key={user.id} className="mt-1 text-lg font-italic text-lavender">
                    <Row style={{height: "32px"}}>
                      <Col>
                        <Link className="text-truncate link-lavender" to={"/user/" + user.username}>
                          {user.username}
                        </Link>
                      </Col>
                      <Col className="col-auto">
                        {getUserSideElement(user)}
                      </Col>
                    </Row>
                  </li>
              )}
              {currentUser.id === group.owner.id
              && <form onSubmit={onAddUser}>
                <Row className="mt-2">
                  <FormGroup className="col pr-2">
                    <InputGroup>
                      <input className="form-control" id="username"
                             name="username" required maxLength="32"
                             placeholder="Add user"
                             onChange={onUsersUsernameChange}/>
                    </InputGroup>
                  </FormGroup>
                  <Col className="col-auto pl-1">
                    <button type="submit" className="mt-1 btn btn-circle btn-sm btn-success">
                      <FontAwesomeIcon icon="user-plus"/>
                    </button>
                  </Col>
                </Row>
              </form>}
            </ul>

            <hr/>

            {currentUser.id === group.owner.id
            && <Row>
              <FormGroup className="col-sm-10 mx-auto">
                <InputGroup>
                  <select className="form-control custom-select" id="new-owner"
                          name="newOwner" required onChange={onNewOwnerChange}>
                    <option hidden value="">Select new owner...</option>
                    {group.users.map(user => {
                          if (user.id !== group.owner.id) {
                            return <option key={user.id} value={user.id}>{user.username}</option>;
                          }
                        }
                    )}
                  </select>
                </InputGroup>
              </FormGroup>
            </Row>}
            <Row className="text-center">
              <FormGroup className="col">
                <button className="btn btn-warning"
                        onClick={onLeave}>
                  Leave group
                </button>
              </FormGroup>
            </Row>

            {currentUser.id === group.owner.id
            && <React.Fragment>
              <hr/>
              <Row className="text-center">
                <FormGroup className="col-sm-8 col-10 mx-auto">
                  <button className="btn btn-block btn-google"
                          onClick={onDeleteGroup}>
                    DELETE GROUP
                  </button>
                </FormGroup>
              </Row>
            </React.Fragment>}

          </ModalBody>
          <ModalFooter className="px-3 py-2">
            <button className="btn btn-secondary" onClick={hideModal}>Close</button>
          </ModalFooter>
        </Modal>

      </React.Fragment>
  );
}

export default GroupSettings;
