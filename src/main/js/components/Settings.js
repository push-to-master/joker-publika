import React, {Component} from 'react';
import PageContentWrapper from "./PageContentWrapper";
import FormGroup from "react-bootstrap/FormGroup";
import FormLabel from "react-bootstrap/FormLabel";
import InputGroup from "react-bootstrap/InputGroup";
import Row from "react-bootstrap/Row";
import ResponsiveForm from "./ResponsiveForm";
import CustomAlert from "./CustomAlert";
import Button from "react-bootstrap/Button";
import ResponsiveColumn from "./ResponsiveColumn";
import DeactivateAccountButton from "./DeactivateAccountButton";

class Settings extends Component {

  constructor(props) {
    super(props);
    this.alertRef = React.createRef();
    this.state = {
      durations: [],

      id: "",
      newEmail: "",
      newUsername: "",
      newAnonymity: !JSON.parse(localStorage.getItem("currentUser")).anonymous,
      oldPassword: "",
      newPassword: "",
      newPasswordConfirmation: "",
      newPollType: "",
      duration: "",
      errorMessage: "",
      showError: false,
      emailError: false,
      success: "",
      emailRegex: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    };
    this.getPollTypeForm = this.getPollTypeForm.bind(this);
    this.getAnonymityForm = this.getAnonymityForm.bind(this);
  }

  scrollToAlert() {
    this.alertRef.current.scrollIntoView({behavior: "smooth"});
  }

  onEmailChange(event) {
    if (!this.state.emailRegex.test(event.target.value)) {
      this.setState({errorMessage: "Not a valid e-mail address"});
      this.setState({emailError: true});
    } else {
      this.setState({errorMessage: ""});
      this.setState({emailError: false});
    }
    this.setState({newEmail: event.target.value});
  }

  onUsernameChange(event) {
    this.setState({newUsername: event.target.value});
  }

  onOldPasswordChange(event) {
    this.setState({oldPassword: event.target.value});
  }

  onNewPasswordChange(event) {
    this.setState({newPassword: event.target.value});
  }

  onNewPasswordConfirmationChange(event) {
    this.setState({newPasswordConfirmation: event.target.value});
  }

  onPollTypeChange(event) {
    this.setState({newPollType: event.target.value});
  }

  onDurationChange(event) {
    this.setState({duration: event.target.value});
  }

  handleSubmitEmail(event) {
    event.preventDefault();
    if (this.state.newEmail && !this.state.emailError) {
      const options = {
        method: "POST",
        headers: {
          'Content-Type': 'application/json'
        },
        body: this.state.newEmail
      };
      fetch('/jokerpublika/user/email', options)
          .then(result => {
            if (result.ok) {
              console.log("Changed e-mail");
              this.setState({success: "New e-mail submitted"});
              this.setState({showError: false});
              this.scrollToAlert();
              this.props.fetchUserData();
            } else if (result.status === 409) {
              this.setState({errorMessage: "E-mail already taken"});
              this.setState({showError: true});
              this.scrollToAlert();
            } else {
              this.setState({errorMessage: "Error while submitting e-mail"});
              this.setState({showError: true});
              this.scrollToAlert();
            }
          });
    } else {
      this.setState({showError: true});
      this.scrollToAlert();
    }
  }

  handleSubmitUsername(event) {
    event.preventDefault();
    const options = {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: this.state.newUsername
    };
    fetch('/jokerpublika/user/username', options)
        .then(result => {
          if (result.ok) {
            console.log("Changed username");
            this.setState({success: "New username submitted"});
            this.setState({showError: false});
            this.scrollToAlert();
            this.props.fetchUserData();
          } else if (result.status === 409) {
            this.setState({errorMessage: "Username already taken"});
            this.setState({showError: true});
            this.scrollToAlert();
          }
        });
  }

  handleSubmitAnonymity(event) {
    event.preventDefault();
    let check = confirm("Are you sure?");
    if (check) {
      const options = {
        method: "POST",
        headers: {
          'Content-Type': 'application/json'
        },
        body: this.state.newAnonymity
      };
      fetch('/jokerpublika/user/anonymous', options)
          .then(result => {
            if (result.ok) {
              console.log("Changed anonymity");
              this.setState({success: "Anonymity changed"});
              this.setState({showError: false});
              this.scrollToAlert();
              this.props.fetchUserData();
              this.setState({newAnonymity: !this.state.newAnonymity});
            }
          });
    }
  }

  handleSubmitPassword(event) {
    event.preventDefault();
    const wrongPasswordStatus = 423;
    if (this.state.newPassword !== this.state.newPasswordConfirmation) {
      this.setState({errorMessage: "New passwords don't match"});
      this.setState({showError: true});
      this.scrollToAlert();
    } else {
      const options = {
        method: "POST",
        headers: {
          'Content-Type': 'application/json'
        },
        body: this.state.oldPassword
      };
      fetch('/jokerpublika/user/password', options)
          .then(result => {
            if (result.ok) {
              console.log("Correct old password");
              const options = {
                method: "POST",
                headers: {
                  'Content-Type': 'application/json'
                },
                body: this.state.newPassword
              };
              fetch('/jokerpublika/user/newpassword', options).then(result2 => {
                if (result2.ok) {
                  console.log("Changed password");
                  this.setState({success: "New password submitted"});
                  this.setState({showError: false});
                  this.scrollToAlert();
                  this.props.fetchUserData();
                }
              });
            } else if (result.status === wrongPasswordStatus) {
              console.log("Incorrect old password");
              this.setState({errorMessage: "Old password is incorrect"});
              this.setState({showError: true});
              this.scrollToAlert();
            }
          });
    }
  }

  handleSubmitNewPollType(event) {
    event.preventDefault();
    const options = {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: this.state.newPollType
    };
    fetch('/jokerpublika/user/polltype', options)
        .then(response => {
          if (response.ok) {
            console.log("Default poll type changed");
            this.setState({success: "New default poll type submitted"});
            this.setState({showError: false});
            this.scrollToAlert();
            this.props.fetchUserData();
          }
        });
  }

  handleSubmitNewDuration(event) {
    event.preventDefault();
    const options = {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: this.state.duration
    };
    fetch('/jokerpublika/user/polltimeout', options)
        .then(result => {
          if (result.ok) {
            console.log("Changed poll timeout");
            this.setState({success: "New default poll duration submitted"});
            this.setState({showError: false});
            this.scrollToAlert();
            this.props.fetchUserData();
          }
        });
  }

  componentDidMount() {
    fetch('/jokerpublika/poll/duration')
        .then(response => response.json())
        .then(response => this.setState({durations: response}));
  }

  getPollTypeForm() {
    if (!JSON.parse(localStorage.getItem("currentUser")).anonymous) {
      return (
          <ResponsiveForm onSubmit={this.handleSubmitNewPollType.bind(this)}>
            <Row>
              <FormGroup className="col mb-2">
                <FormLabel className="text-lg text-violet" htmlFor="poll-type">Change default poll type</FormLabel>
                <InputGroup>
                  <select className="form-control custom-select" id="poll-type"
                          name="pollType" required
                          onChange={this.onPollTypeChange.bind(this)}>
                    <option hidden value="">Select poll type...</option>
                    <option key="PUBLIC" value="PUBLIC">Public</option>
                    <option key="ANONYMOUS" value="ANONYMOUS">Anonymous</option>
                    <option key="FRIEND_GROUP" value="FRIEND_GROUP">Friend group</option>
                  </select>
                </InputGroup>
              </FormGroup>
            </Row>
            <Row className="mb-1 text-right">
              <FormGroup className="col">
                <Button className="btn-info" type="submit">Submit changes</Button>
              </FormGroup>
            </Row>
          </ResponsiveForm>
      );
    }
  }

  getAnonymityForm() {
    if (JSON.parse(localStorage.getItem("currentUser")).anonymous) {
      return (
          <ResponsiveForm onSubmit={this.handleSubmitAnonymity.bind(this)}>
            <Row className="mb-1 text-center">
              <FormGroup className="col-sm-7 col mx-auto">
                <FormLabel className="text-lg text-violet" htmlFor="poll-type">
                  Change anonymity
                  <span className="badge badge-warning ml-2">IRREVERSIBLE</span>
                </FormLabel>
                <InputGroup>
                  <Button className="btn-block btn-primary" type="submit">Become a public user</Button>
                </InputGroup>
              </FormGroup>
            </Row>
          </ResponsiveForm>
      );
    }
  }

  render() {
    return (
        <PageContentWrapper title={"User Settings"}>
          <div ref={this.alertRef}>
            <ResponsiveColumn>
              {this.state.showError && <CustomAlert color="danger" header="Error!"
                                                    body={this.state.errorMessage}/>}
              {this.state.success && <CustomAlert color="success" header="Success!"
                                                  body={this.state.success}/>}
            </ResponsiveColumn>
          </div>

          <ResponsiveForm onSubmit={this.handleSubmitUsername.bind(this)}>
            <Row>
              <FormGroup className="col mb-2">
                <FormLabel className="text-lg text-violet" htmlFor="username">Change username</FormLabel>
                <InputGroup>
                  <input type="text" className="form-control" id="username"
                         maxLength="32" minLength="2" required
                         name="username" onChange={this.onUsernameChange.bind(this)}
                         placeholder="New username"/>
                </InputGroup>
              </FormGroup>
            </Row>
            <Row className="mb-1 text-right">
              <FormGroup className="col">
                <Button className="btn-info" type="submit">Submit changes</Button>
              </FormGroup>
            </Row>
          </ResponsiveForm>

          <ResponsiveForm onSubmit={this.handleSubmitEmail.bind(this)}>
            <Row>
              <FormGroup className="col mb-2">
                <FormLabel className="text-lg text-violet" htmlFor="email">Change e-mail</FormLabel>
                <InputGroup>
                  <input type="email" className="form-control" id="email"
                         maxLength="256" required
                         name="email" onChange={this.onEmailChange.bind(this)}
                         placeholder="New e-mail"/>
                </InputGroup>
              </FormGroup>
            </Row>
            <Row className="mb-1 text-right">
              <FormGroup className="col">
                <Button className="btn-info" type="submit">Submit changes</Button>
              </FormGroup>
            </Row>
          </ResponsiveForm>

          <ResponsiveForm onSubmit={this.handleSubmitPassword.bind(this)}>
            <Row>
              <FormGroup className="col mb-2">
                <FormLabel className="text-lg text-violet" htmlFor="old-password">Change password</FormLabel>
                <InputGroup className="mb-1">
                  <input type="password" className="form-control" id="old-password"
                         maxLength="64" minLength="4" required
                         name="old-password" onChange={this.onOldPasswordChange.bind(this)}
                         placeholder="Old password"/>
                </InputGroup>
                <InputGroup className="mb-1">
                  <input type="password" className="form-control" id="new-password"
                         maxLength="64" minLength="4" required
                         name="new-password" onChange={this.onNewPasswordChange.bind(this)}
                         placeholder="New password"/>
                </InputGroup>
                <InputGroup>
                  <input type="password" className="form-control" id="new-password-confirm"
                         maxLength="64" minLength="4" required
                         name="new-password-confirm" onChange={this.onNewPasswordConfirmationChange.bind(this)}
                         placeholder="Confirm new password"/>
                </InputGroup>
              </FormGroup>
            </Row>
            <Row className="mb-1 text-right">
              <FormGroup className="col">
                <Button className="btn-info" type="submit">Submit changes</Button>
              </FormGroup>
            </Row>
          </ResponsiveForm>

          <ResponsiveForm onSubmit={this.handleSubmitNewDuration.bind(this)}>
            <Row>
              <FormGroup className="col mb-2">
                <FormLabel className="text-lg text-violet" htmlFor="duration">Change default poll duration</FormLabel>
                <InputGroup>
                  <select className="form-control custom-select" id="duration"
                          name="duration" required
                          onChange={this.onDurationChange.bind(this)}>
                    <option hidden value="">Select duration...</option>
                    {this.state.durations.map(duration =>
                        <option key={duration.value} value={duration.value}>{duration.label}</option>
                    )}
                  </select>
                </InputGroup>
              </FormGroup>
            </Row>
            <Row className="mb-1 text-right">
              <FormGroup className="col">
                <Button className="btn-info" type="submit">Submit changes</Button>
              </FormGroup>
            </Row>
          </ResponsiveForm>

          {this.getPollTypeForm()}

          {this.getAnonymityForm()}

          <ResponsiveColumn>
            <Row className="mt-5 mb-3">
              <FormGroup className="col-10 col-sm-9 col-md-8 col-lg-7 col-xl-6 mx-auto">
                <DeactivateAccountButton fetchUserData={this.props.fetchUserData}/>
              </FormGroup>
            </Row>
          </ResponsiveColumn>
        </PageContentWrapper>
    );
  }

}

export default Settings;
