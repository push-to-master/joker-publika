import Modal from "react-bootstrap/Modal";
import React from "react";
import {useHistory} from 'react-router-dom';

function DeactivateAccountButton(props) {
  const [isOpen, setIsOpen] = React.useState(false);
  const history = useHistory();

  const showModal = () => {
    setIsOpen(true);
  };

  const hideModal = () => {
    setIsOpen(false);
  };

  function handleDeactivateUser(event) {
    event.preventDefault();
    fetch('/jokerpublika/user/deactivate_current', {
      method: 'POST'
    }).then(result => {
          if (result.ok) {
            fetch("/jokerpublika/logout")
                .then(() => {
                  console.log("Deactivated");
                  props.fetchUserData();
                  history.push('/');
                });
          }
        }
    );
  }

  return (
      <>
        <button className="btn btn-block btn-lg btn-google" onClick={showModal}>DEACTIVATE ACCOUNT</button>
        <Modal show={isOpen} onHide={hideModal}>
          <Modal.Header>
            <Modal.Title>Are you sure?</Modal.Title>
          </Modal.Header>
          <Modal.Body>This action cannot be undone!</Modal.Body>
          <Modal.Footer>
            <button onClick={hideModal} className="btn btn-secondary">Cancel</button>
            <button onClick={handleDeactivateUser.bind(this)} className="btn btn-danger m-1">DEACTIVATE</button>
          </Modal.Footer>
        </Modal>
      </>
  );
}

export default DeactivateAccountButton;
