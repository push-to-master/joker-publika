import React from 'react';

function Footer() {

  return (
      <footer className="sticky-footer shadow-sm bg-white p-3 text-center">
        <span>Copyright © PushToMaster @ FER 2020</span>
      </footer>
  );
}

export default Footer;
