import React, {Component} from 'react';
import { Link } from "react-router-dom";
import NavBar from "react-bootstrap/Navbar";

class LoginHeader extends Component {

  render() {
    return (
        <NavBar className="shadow bg-gradient-violet" style={{height: "50px"}}>
          <Link to="/" style={{display: "flex", textDecoration: "none"}}>
            <img src={"/jokerpublika/img/app_logo.png"} alt="Logo" height="34px"/>
            {/*<h3 className="text-white" style={{marginLeft: "2px", color: "black"}}>PollPal</h3>*/}
            <img src={"/jokerpublika/img/app_name.png"} alt="Name" height="34px" width="100px"/>
          </Link>

          <div style={{marginLeft: "auto", display: "flex"}}>
            <Link className="btn text-lg text-white" to='/register'>
              Register
            </Link>
            <Link className="btn text-lg text-white" to='/login'>
              Login
            </Link>
          </div>
        </NavBar>
    );
  }
}

export default LoginHeader;
