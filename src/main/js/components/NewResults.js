import React from "react";
import PageContentWrapper from "./PageContentWrapper";
import ResponsiveColumn from "./ResponsiveColumn";
import PollCard from "./PollCard";
import CustomAlert from "./CustomAlert";
import Row from "react-bootstrap/Row";

function NewResults() {

  const [polls, setPolls] = React.useState([]);
  const [displayLoading, setDisplayLoading] = React.useState(true);
  const currentUser = JSON.parse(localStorage.getItem("currentUser"));

  React.useEffect(() => {
    const get = () => {
      fetch('/jokerpublika/poll/user/inactive?userId=' + currentUser.id)
          .then(response => response.json())
          .then(response => {
            setPolls(response);
            setDisplayLoading(false);
          }, () => {
            console.log("Failed to fetch new results");
            setDisplayLoading(false);
          });
    };
    get();
    const interval = setInterval(get, 10000);
    return () => clearInterval(interval);
  }, []);

  function deletePoll(pollId) {
    setPolls((oldPolls) => oldPolls.filter(poll => poll.id !== pollId));
  }

  return (
      <PageContentWrapper title={"New Results"}>
        <ResponsiveColumn>
          {polls.length === 0
          && <CustomAlert color={"warning"} header={"Come back later!"}
                          body={"You have no finished polls... yet 😉"}/>}
          {displayLoading
          && <Row className="mt-4">
            <img alt="Loading..." src={"/jokerpublika/img/loading.gif"}
                 className="img-fluid mx-auto"
                 style={{height: "100px"}}/>
          </Row>}
          {polls.map(poll =>
              <div key={poll.id} className="my-3">
                <PollCard key={poll.id} poll={poll} deletePoll={deletePoll}/>
              </div>
          )}
        </ResponsiveColumn>
      </PageContentWrapper>
  );

}

export default NewResults;
