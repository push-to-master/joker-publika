import React from 'react';
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import {Link, useHistory} from "react-router-dom";
import {Typography} from "@material-ui/core";

function Login(props) {

  // za navigaciju
  const history = useHistory();

  const [form, setForm] = React.useState({username: '', password: ''});
  const [errors, setErrors] = React.useState({});
  const [failedLogin, setFailedLogin] = React.useState(false);

  React.useEffect(() => {
    setErrors(() => getErrors());
  }, [form]);

  //promjena u formi
  function onChange(event) {
    const {name, value} = event.target;
    setForm(oldForm => ({...oldForm, [name]: value}))
  }

  //napisi sve errore
  function getErrors() {
    const newErrors = {};
    const requiredFields = ['username', 'password'];

    requiredFields.forEach(field => {
      if (!form[field]) {
        errors[field] = "Required";
      }
    });

    return newErrors;
  }

  //ima li gresaka u formi
  function isValid() {
    return Object.keys(getErrors()).length === 0;
  }

  //pritisak na login
  function onSubmit(e) {
    e.preventDefault();

    //The URLSearchParams interface defines utility methods to work with the query string of a URL.
    const data = new URLSearchParams();
    data.append("username", form.username);
    data.append("password", form.password);

    const options = {
      method: "POST",
      body: data
    };

    //fetch dohvaća resurs sa mreže te vraća promise koji izvršava kada je odgovor vraćen
    fetch('/jokerpublika/login', options)
        .then(result => {
          if (!result.url.endsWith("?error")) {
            //ovo ulogirava korisnika
            props.onLogin();
            //history.push sluzi da programski odnavigiras na neku stranicu u reactu
            history.push('/');
          } else {
            setFailedLogin(true);
          }
        }).catch(() => console.warn("dogodio se error!"))
  }

  return (
      <Container maxWidth="sm" className="pt-4">
        {/*kada se stisne submit*/}
        <form onSubmit={onSubmit}>
          <Typography variant="h5">Login</Typography>
          <div hidden={!failedLogin} className="alert alert-danger custom-login-alert" role="alert">Username or password
            is incorrect
          </div>
          {/*username text field*/}
          <TextField
              variant="standard"
              margin="normal"
              required
              fullWidth
              id="username"
              label="Username"
              name="username"
              autoComplete="username"
              onChange={onChange}
              value={form.username}
              autoFocus/>
          {/*password text field*/}
          <TextField
              variant="standard"
              margin="normal"
              required
              fullWidth
              error={!!errors.password}
              helperText={errors.password || ""}
              name="password"
              label="Password"
              type="password"
              id="password"
              onChange={onChange}
              value={form.password}
              autoComplete="current-password"/>
          {/*Sign in gumb*/}
          <Button className="mt-4" type="submit" fullWidth variant="contained" color="primary">Log in</Button>
        </form>
        <br/>
        {/*Registriranje ako korisnik nema račun - na dnu logina*/}
        <Grid container>
          <Grid item>
            <Link to="/register"><Typography variant="body2">Don't have an account? Sign Up</Typography></Link>
          </Grid>
        </Grid>
      </Container>
  )
}

export default Login;
