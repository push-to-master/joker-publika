import React from 'react';
import NavBar from "react-bootstrap/Navbar";
import Dropdown from "react-bootstrap/Dropdown";
import DropdownToggle from "react-bootstrap/DropdownToggle"
import DropdownMenu from "react-bootstrap/DropdownMenu";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Link, useHistory} from "react-router-dom";
import Sidebar from "./Sidebar";

function MainHeader(props) {

  const history = useHistory();
  const currentUser = JSON.parse(localStorage.getItem("currentUser"));

  function logout() {
    props.onLogout();
    history.push("/");
  }

  return (
      <NavBar className="shadow bg-gradient-violet" style={{height: "50px"}}>

        <Sidebar style={{top: 0, left: 0}}/>

        <div className="mx-auto">
          <Link to="/" style={{display: "flex", textDecoration: "none"}}>
            <img src={"/jokerpublika/img/app_logo.png"} alt="Logo" height="34px"/>
            <img className="d-none d-sm-inline" src={"/jokerpublika/img/app_name.png"} alt="Name" height="34px"
                 width="100px"/>
          </Link>
        </div>

        <div style={{display: "flex"}}>
          <Dropdown className="no-arrow">
            <DropdownToggle id="user-dropdown" as="button" className="btn nav-link px-0 btn-no-focus" role="none">
                <span id="username" className="mr-3 d-none d-sm-inline text-white font-italic">
                  {JSON.parse(localStorage.getItem("currentUser")).username}
                </span>
              <img src={"/jokerpublika/img/default_avatar.png"} alt="Avatar" className="rounded-circle img-profile"
                   height="34px"/>
            </DropdownToggle>

            <DropdownMenu className="dropdown-menu-right shadow animated--grow-in">
              <div id="username-small" className="text-center text-lg d-block d-sm-none font-italic">
                {JSON.parse(localStorage.getItem("currentUser")).username}
              </div>
              <div className="dropdown-divider d-block d-sm-none"/>
              <Link to={"/user/" + currentUser.username} className="text-lg dropdown-item">
                <FontAwesomeIcon className="fa-sm fa-fw mr-2 text-gray-400" icon="user"/>
                <span>Profile</span>
              </Link>
              <Link to="/settings" className="text-lg dropdown-item">
                <FontAwesomeIcon className="fa-sm fa-fw mr-2 text-gray-400" icon="cog"/>
                <span>Settings</span>
              </Link>
              {currentUser.roles.includes("ADMIN")
              && <Link to="/ban" className="text-lg dropdown-item">
                <FontAwesomeIcon className="fa-sm fa-fw mr-2 text-gray-400" icon="ban"/>
                <span>Ban users</span>
              </Link>}
              {currentUser.roles.includes("ADMIN")
              && <Link to="/admin" className="text-lg dropdown-item">
                <FontAwesomeIcon className="fa-sm fa-fw mr-2 text-gray-400" icon="hourglass-half"/>
                <span>App settings</span>
              </Link>}
              <div className="dropdown-divider"/>
              <button className="text-lg dropdown-item btn-no-focus" onClick={logout}>
                <FontAwesomeIcon className="fa-sm fa-fw mr-2 text-gray-400" icon="sign-out-alt"/>
                <span>Logout</span>
              </button>
            </DropdownMenu>

          </Dropdown>
        </div>

      </NavBar>
  );

}

export default MainHeader;
