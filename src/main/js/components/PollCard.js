import React from 'react';
import {Link} from 'react-router-dom';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import DeleteModal from "./DeleteModal";
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";

function PollCard(props) {

  const [poll, setPoll] = React.useState(props.poll);
  const user = poll.user;
  const [answerId, setAnswerId] = React.useState(0);
  const currentUser = JSON.parse(localStorage.getItem("currentUser"));
  const [modal, setModal] = React.useState(false);
  const [firstLightBoxOpen, setFirstLightBoxOpen] = React.useState(false);
  const [secondLightBoxOpen, setSecondLightBoxOpen] = React.useState(false);

  React.useEffect(() => {
    fetch('/jokerpublika/poll/answer/check?pollId=' + poll.id)
        .then(response => response.json())
        .then(response => {
          setAnswerId(response);
        });
  }, []);

  function getUser() {
    if (user.anonymous || poll.pollType === "ANONYMOUS") {
      return user.id === currentUser.id ? "Me" : "Anonymous";
    } else {
      return (
          <Link className="link-lavender" to={"/user/" + user.username}>
            {user.username}
          </Link>
      );
    }
  }

  function simplifyTimeDifference(first, second) {
    let seconds = Math.floor((first - second) / 1000);
    if (seconds < 60) return "less than a minute";
    let minutes = Math.floor(seconds / 60);
    if (minutes < 60) return minutes + (minutes === 1 ? " minute" : " minutes");
    let hours = Math.floor(minutes / 60);
    if (hours < 24) return hours + (hours === 1 ? " hour" : " hours");
    let days = Math.floor(hours / 24);
    if (days < 30) return days + (days === 1 ? " day" : " days");
    let months = Math.floor(days / 30);
    if (months < 12) return months + (months === 1 ? " month" : " months");
    let years = Math.floor(months / 12);
    return years + (years === 1 ? " year" : " years");
  }

  function getPostTime() {
    return simplifyTimeDifference(Date.now(), Date.parse(poll.postTime));
  }

  function getTimeout() {
    return simplifyTimeDifference(Date.parse(poll.timeout), Date.now());
  }

  function getTimeoutMessage() {
    if (Date.parse(poll.timeout) > Date.now()) {
      return "Ends in " + getTimeout();
    } else {
      return "Ended";
    }
  }

  function getPercentage1() {
    if (noVotes()) {
      return 0;
    }
    return Math.round(poll.answer1.voteCount * 100 / (poll.answer1.voteCount + poll.answer2.voteCount));
  }

  function getPercentage2() {
    if (noVotes()) {
      return 0;
    }
    return 100 - getPercentage1();
  }

  function noVotes() {
    return poll.answer1.voteCount === 0 && poll.answer2.voteCount === 0;
  }

  function answer1() {
    answer(poll.answer1.id);
    if (props.fetchPolls) {
      props.fetchPolls();
    }
  }

  function answer2() {
    answer(poll.answer2.id);
    if (props.fetchPolls) {
      props.fetchPolls();
    }
  }

  function answer(id) {
    fetch('/jokerpublika/poll/answer?pollId=' + poll.id + '&answerId=' + id)
        .then(response => {
          if (response.ok) {
            fetch('/jokerpublika/poll/?pollId=' + poll.id)
                .then(result => result.json())
                .then(response => {
                  setPoll(response);
                });
          } else {
            console.log("Poll " + poll.id + " couldn't be answered");
          }
        })
        .then(() => {
          setAnswerId(id);
        });
  }

  function showModal() {
    setModal(true);
  }

  function hideModal() {
    setModal(false);
  }

  function deletePoll() {
    hideModal();
    const options = {
      method: "DELETE"
    };
    fetch('/jokerpublika/poll?pollId=' + poll.id, options)
        .then(response => {
          if (response.ok) {
            props.deletePoll(poll.id);
          } else {
            alert("Poll couldn't be deleted");
            console.log("Poll " + poll.id + " couldn't be deleted");
          }
        });
  }

  function renderFirstLightBox() {
    return (
        <Lightbox mainSrc={poll.image1} imageTitle={poll.question}
                  onCloseRequest={() => setFirstLightBoxOpen(false)}/>
    );
  }

  function renderSecondLightBox() {
    return (
        <Lightbox mainSrc={poll.image2} imageTitle={poll.question}
                  onCloseRequest={() => setSecondLightBoxOpen(false)}/>
    );
  }

  return (
      <React.Fragment>
        {firstLightBoxOpen && renderFirstLightBox()}
        {secondLightBoxOpen && renderSecondLightBox()}
        <Card className="shadow">

          <div className="card-header px-2 px-sm-4 py-2 d-flex flex-row align-items-baseline justify-content-between">
            <h5 className="py-1 text-truncate m-0 font-italic">{getUser()}</h5>
            <span className="ml-1 text-nowrap">{getPostTime()} ago</span>
          </div>

          <div className="card-body px-2 px-sm-4 py-2">

            {poll.pollType === "ANONYMOUS" && currentUser.roles.includes("ADMIN") && currentUser.id !== user.id
            && <p className="mt-1 mb-2 card-text text-gray-500">By:&nbsp;
              <Link className="font-italic link-lavender" to={"/user/" + user.username}>
                {user.username}
              </Link>
            </p>}

            {poll.pollType === "FRIEND_GROUP" && !props.hideGroup
            && <p className="mt-1 mb-2 card-text text-gray-500">In group:&nbsp;
              <span className="font-italic">{poll.friendGroup.name}</span>
            </p>}

            <p className="mt-1 card-text text-lg text-violet">{poll.question}</p>

            {(poll.image1 || poll.image2)
            && <Row className="mb-3 align-items-center justify-content-center">
              {poll.image1
              && <Col className={poll.image2 ? "col-sm-6 mb-sm-0 mb-1 col-12 pr-sm-1 text-center" : "text-center"}>
                <img style={{maxHeight: "400px", cursor: "pointer"}} onClick={() => setFirstLightBoxOpen(true)}
                     src={poll.image1} className="img-fluid rounded"/>
              </Col>}
              {poll.image2
              && <Col className={poll.image1 ? "col-sm-6 mt-sm-0 mt-1 col-12 pl-sm-1 text-center" : "text-center"}>
                <img style={{maxHeight: "400px", cursor: "pointer"}} onClick={() => setSecondLightBoxOpen(true)}
                     src={poll.image2} className="img-fluid rounded"/>
              </Col>}
            </Row>}

            {(answerId !== 0 || currentUser.id === user.id && !noVotes()
                || Date.parse(poll.timeout) < Date.now() && !noVotes())
            && <Row className="align-items-center mb-3">
              <OverlayTrigger placement="top" overlay={
                <Tooltip id="voteCount1">{poll.answer1.voteCount}</Tooltip>}>
                <div className="col-auto">
                  {getPercentage1() + "%"}
                </div>
              </OverlayTrigger>
              <Col className="px-0">
                <div className="progress progress-sm bg-gradient-red">
                  <div className="progress-bar bg-gradient-green" role="progressbar"
                       style={{width: getPercentage1() + "%"}}
                       aria-valuenow={getPercentage1()} aria-valuemin="0" aria-valuemax="100"/>
                </div>
              </Col>
              <OverlayTrigger placement="top" overlay={
                <Tooltip id="voteCount2">{poll.answer2.voteCount}</Tooltip>}>
                <div className="col-auto">
                  {getPercentage2() + "%"}
                </div>
              </OverlayTrigger>
            </Row>}

            <Row className="pb-0 pb-sm-2">
              <div className="col-sm-6 pl-sm-2 pr-sm-1 px-2 mb-1 mb-sm-0">
                <button className="btn btn-block btn-block-height btn-gradient-green"
                        disabled={answerId !== 0 || currentUser.id === user.id || Date.parse(poll.timeout) < Date.now()}
                        onClick={answer1}>
                  {poll.answer1.text}
                  {answerId === poll.answer1.id
                  && <FontAwesomeIcon className="ml-3 text-lg text-green" icon={["far", "check-circle"]}/>}
                </button>
              </div>
              <div className="col-sm-6 pl-sm-1 pr-sm-2 px-2 mt-1 mt-sm-0">
                <button className="btn btn-block btn-block-height btn-gradient-red"
                        disabled={answerId !== 0 || currentUser.id === user.id || Date.parse(poll.timeout) < Date.now()}
                        onClick={answer2}>
                  {poll.answer2.text}
                  {answerId === poll.answer2.id
                  && <FontAwesomeIcon className="ml-3 text-lg text-green" icon={["far", "check-circle"]}/>}
                </button>
              </div>
            </Row>

          </div>

          <div className="card-footer px-2 px-sm-4 py-2 d-flex flex-row align-items-center justify-content-between">
            <span>{getTimeoutMessage()}</span>
            {(user.id === currentUser.id || currentUser.roles.includes("ADMIN"))
            && <button className="btn btn-circle btn-google btn-sm" onClick={showModal}>
              <FontAwesomeIcon icon="trash-alt"/>
            </button>}
          </div>

          <DeleteModal show={modal} onHide={hideModal} deletePoll={deletePoll}/>

        </Card>
      </React.Fragment>
  );

}

export default PollCard;
