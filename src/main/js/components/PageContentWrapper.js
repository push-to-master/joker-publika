import React from "react";

function PageContentWrapper(props) {

  return (
      <div className="p-2">
        <h2 className="text-center py-1 text-break mt-2 mb-4 font-weight-bold text-lavender">
          {props.title}
        </h2>

        {props.children}

      </div>
  );
}

export default PageContentWrapper;
