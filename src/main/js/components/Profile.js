import React from 'react';
import ResponsiveColumn from "./ResponsiveColumn";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import CustomAlert from "./CustomAlert";
import PollCard from "./PollCard";
import Ban from "./Ban";

function Profile(props) {

  const currentUser = JSON.parse(localStorage.getItem("currentUser"));
  const user = props.user;
  const [polls, setPolls] = React.useState([]);
  const [displayLoading, setDisplayLoading] = React.useState(true);

  React.useEffect(() => {
    if (currentUser.id !== user.id && !currentUser.roles.includes("ADMIN")) {
      const get = () => {
        fetch('/jokerpublika/poll/user/public?userId=' + user.id)
            .then(response => response.json())
            .then(response => {
              setPolls(response);
              setDisplayLoading(false);
            });
      };
      get();
      const interval = setInterval(get, 10000);
      return () => clearInterval(interval);
    } else if (currentUser.id !== user.id && currentUser.roles.includes("ADMIN")) {
      const get = () => {
        fetch('/jokerpublika/poll/user/?userId=' + user.id)
            .then(response => response.json())
            .then(response => {
              setPolls(response);
              setDisplayLoading(false);
            });
      };
      get();
      const interval = setInterval(get, 10000);
      return () => clearInterval(interval);
    } else {
      fetch('/jokerpublika/poll/user?userId=' + currentUser.id)
          .then(response => response.json())
          .then(response => {
            setPolls(response);
            setDisplayLoading(false);
          });
    }
  }, []);

  function deletePoll(pollId) {
    setPolls((oldPolls) => oldPolls.filter(poll => poll.id !== pollId));
  }

  function getEmptyProfileAlert() {
    return currentUser.id !== user.id ?
        <CustomAlert color={"warning"} header={"I couldn't find anything."}
                     body={"This user hasn't posted any public polls yet 🤨"}/> :
        <CustomAlert color={"warning"} header={"I couldn't find anything."}
                     body={"You haven't posted any polls yet 🤨"}/>;
  }


  return (
      <div className="px-2 pt-3 pb-2 ">
        <ResponsiveColumn>

          <Row className="align-items-center justify-content-center flex-nowrap">
            <Col className="col-auto">
              <img src={"/jokerpublika/img/default_avatar.png"} alt="Avatar"
                   className="rounded-circle img-profile" height="80px"/>
            </Col>
            <Col className="pl-1 col-9">
              <div className="h3 py-1 mb-0 text-truncate text-amethyst font-italic font-weight-bold">
                {user.username}
              </div>
              <div className="h6 py-1 text-truncate text-gray-600">{user.email}</div>
            </Col>
          </Row>

          <hr/>

          {polls.length === 0 && getEmptyProfileAlert()}
          {displayLoading
          && <Row className="mt-4">
            <img alt="Loading..." src={"/jokerpublika/img/loading.gif"}
                 className="img-fluid mx-auto"
                 style={{height: "100px"}}/>
          </Row>}
          {polls.map(poll =>
              <div key={poll.id} className="my-3">
                <PollCard key={poll.id} poll={poll} deletePoll={deletePoll}/>
              </div>
          )}

        </ResponsiveColumn>
        {currentUser.roles.includes("ADMIN") && currentUser.id !== user.id
        && <React.Fragment>
          <hr/>
          <Ban usernameInQuestion={user.username}/>
        </React.Fragment>}
      </div>
  );
}

export default Profile;
