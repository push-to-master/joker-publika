import React from 'react';
import Container from "@material-ui/core/Container";
import {Checkbox, FormControlLabel, Typography} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import {Link, useHistory} from "react-router-dom";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Popover from "react-bootstrap/Popover";
import PopoverTitle from "react-bootstrap/PopoverTitle";
import PopoverContent from "react-bootstrap/PopoverContent";

function Register(props) {

  const settings = {
    minPasswordLength: 4,
    maxPasswordLength: 64,
    minUsernameLength: 2,
    maxUsernameLength: 32,
    maxEmailLength: 256,
    emailRegex: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
    userNameRegex: /^\S*$/
  };

  const history = useHistory();
  //errori
  const [errors, setErrors] = React.useState({});
  //podatci o formi
  const [form, setForm] = React.useState({
    username: '',
    email: '',
    password: '',
    confirmPassword: '',
    anonymous: false
  });
//podatci o tome je li element bio u fokusu
  const [fieldSelectedAtLeastOnce, setSelectedField] = React.useState({
    usernameB: false,
    emailB: false,
    passwordB: false,
    confirmPasswordB: false,
  });

  //prikazati alert?
  const [failedRegister, setFailedRegister] = React.useState(false);

  //ovo se poziva nakon rendera
  React.useEffect(() => {
    setErrors(() => getErrors());
  }, [form]);

  //nakon promjene bilokojeg fielda
  function onChange(event) {
    const {name, value} = event.target;
    setForm(oldForm => ({...oldForm, [name]: value}));
  }

  //izvodi se kada se neki textField fokusira/oznaci
  function onFocus(event) {
    const {name} = event.target;
    //postavi svojstvo na oznaceno
    setSelectedField((old) => ({...old, [name + "B"]: true}));
  }

  //nakon promjene anonimnog checkboxa
  function onCheckChange(event) {
    const {name, checked} = event.target;
    setForm(oldForm => ({...oldForm, [name]: checked}));
  }

  //jesu li sve zadovoljavajuce ispunjeno
  function isValid() {
    return Object.keys(getErrors()).length === 0;
  }

  //For checking if a string is blank, null or undefined I use:
  function isBlank(str) {
    return (!str || /^\s*$/.test(str));
  }

  //poziva se prilikom postavljanja vrijednost errora
  function getErrors() {
    const newErrors = {};
    const requiredFields = ['username', 'email', 'password', 'confirmPassword'];

    //za svako polje
    requiredFields.forEach(field => {
      //ako je polje prazno i bilo je oznaceno => stavi upozorenje
      if (isBlank(form[field]) && fieldSelectedAtLeastOnce[field + "B"]) {
        newErrors[field] = "Required";
      }
    });

    //ako field ne zadovoljava regex za username adresu
    if (!settings.userNameRegex.test(form.username)) {
      newErrors.username = "Username can't contain spaces";
    }
    //ako je username predug
    else if (form.username.length > settings.maxUsernameLength) {
      newErrors.username = "Maximum username length is " + settings.maxUsernameLength;
    }
    //ako username nije dovoljno dug
    else if (form.username.length < settings.minUsernameLength && form.username.length !== 0) {
      newErrors.username = "Minimum username length is " + settings.minUsernameLength;
    }

    if (form.email.length > settings.maxEmailLength && !isBlank(form.email)) {
      newErrors.email = "Maximum e-mail length is " + settings.maxEmailLength;
    }

    //ako field ne zadovoljava regex za email adresu i nije prazno
    if (!settings.emailRegex.test(form.email.toLowerCase()) && !isBlank(form.email)) {
      newErrors.email = "Not a valid e-mail address";
    }

    if (form.password.length > settings.maxPasswordLength && fieldSelectedAtLeastOnce.passwordB && !isBlank(form.password)) {
      newErrors.password = "Maximum password length is " + settings.maxPasswordLength;
    }

    //ako je pass prekratak i polje je selektirano barem jednom i polje nije prazno
    if (form.password.length < settings.minPasswordLength && fieldSelectedAtLeastOnce.passwordB && !isBlank(form.password)) {
      newErrors.password = "Minimum password length is " + settings.minPasswordLength;
    }

    //ako su lozinke razlicite i polje za unos lozinke je bilo selektirano
    if (form.password !== form.confirmPassword && fieldSelectedAtLeastOnce.confirmPasswordB) {
      newErrors.confirmPassword = "Password doesn't match";
    }

    return newErrors;
  }

  function onSubmit(e) {
    e.preventDefault();

    const loginData = new URLSearchParams();
    loginData.append("username", form.username);
    loginData.append("password", form.password);

    const options = {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(form)
    };

    const loginOptions = {
      method: "POST",
      body: loginData
    };

    fetch('/jokerpublika/user/register', options)
        .then(result => {
          if (result.ok) {
            fetch('/jokerpublika/login', loginOptions)
                .then(result => {
                  if (result.ok) {
                    props.onRegister();
                    history.push('/');
                  }
                })
          } else {
            setFailedRegister(true);
          }
        }).catch(() => console.warn("Error while registering!"))
  }

  const popover = (
      <Popover id="popover-basic">
        <PopoverTitle as="h3">What's an <strong>anonymous account</strong>?</PopoverTitle>
        <PopoverContent>
          <ul className="pl-3">
            <li>Your username will not be displayed on your polls</li>
            <li>Your profile will not be reachable by other users</li>
            <li>You can't create or be a part of any friend groups</li>
          </ul>
          <strong>Regular users</strong> can also hide their username from specific polls,
          but their profile is always reachable.
        </PopoverContent>
      </Popover>
  );

  return (
      <Container maxWidth="sm" className="pt-4">
        {/*kada se forma submita*/}
        <form onSubmit={onSubmit}>
          <Typography variant="h5">Register</Typography>
          <div hidden={!failedRegister} className="alert alert-danger custom-login-alert" role="alert">User with the
            same email or username already registered
          </div>
          <TextField
              variant="standard"
              margin="normal"
              required
              fullWidth
              error={!!errors.username}
              helperText={errors.username || ""}
              id="username"
              label="Username"
              name="username"
              onChange={onChange}
              onFocus={onFocus}
              value={form.username}
              autoFocus/>
          <TextField
              variant="standard"
              margin="normal"
              required
              fullWidth
              error={!!errors.email}
              helperText={errors.email || ""}
              id="email"
              label="E-mail"
              name="email"
              type="email"
              onChange={onChange}
              onFocus={onFocus}
              value={form.email}/>
          <TextField
              variant="standard"
              margin="normal"
              required
              fullWidth
              error={!!errors.password}
              helperText={errors.password || ""}
              name="password"
              label="Password"
              type="password"
              id="password"
              onChange={onChange}
              onFocus={onFocus}
              value={form.password}/>
          <TextField
              variant="standard"
              margin="normal"
              required
              fullWidth
              error={!!errors.confirmPassword}
              helperText={errors.confirmPassword || ""}
              name="confirmPassword"
              label="Confirm password"
              type="password"
              id="confirm-password"
              onChange={onChange} value={form.confirmPassword}
              onFocus={onFocus}/>
          <FormControlLabel
              control={<Checkbox name="anonymous" id="anonymous" value="anonymous" checked={form.anonymous}
                                 color="primary" onChange={onCheckChange}/>}
              label="Anonymous account"/>
          <OverlayTrigger trigger="click" placement="top" overlay={popover}>
            <span className="badge badge-pill badge-primary align-text-bottom font-weight-bold text-lg px-2"
                  style={{cursor: "pointer", lineHeight: "0.8"}}>?</span>
          </OverlayTrigger>
          <Button type="submit" fullWidth variant="contained" color="primary" disabled={!isValid()}>Sign up</Button>
        </form>

        <br/>

        <Grid container>
          <Grid item>
            <Link to="/login"><Typography variant="body2">Already have an account? Log in</Typography></Link>
          </Grid>
        </Grid>
      </Container>
  )

}

export default Register;
