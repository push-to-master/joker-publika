import React from 'react';
import FormGroup from "react-bootstrap/FormGroup";
import FormLabel from "react-bootstrap/FormLabel";
import InputGroup from "react-bootstrap/InputGroup";
import Row from "react-bootstrap/Row";
import Button from "react-bootstrap/Button";
import {useHistory} from "react-router-dom";
import PageContentWrapper from "./PageContentWrapper";
import ResponsiveForm from "./ResponsiveForm";
import Col from "react-bootstrap/Col";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import CustomAlert from "./CustomAlert";
import {GroupContext} from "../App";

function CreateGroup() {

  const history = useHistory();

  const [name, setName] = React.useState("");
  const [users, setUsers] = React.useState([{username: ""}]);
  const [error, setError] = React.useState(false);
  const fetchGroupData = React.useContext(GroupContext).fetchGroupData;
  const alertRef = React.useRef();

  function scrollToAlert() {
    alertRef.current.scrollIntoView({behavior: "smooth"});
  }

  function onNameChange(event) {
    setName(event.target.value);
  }

  function addUserField() {
    setUsers(users.concat({username: ""}));
  }

  function removeMemberField(event) {
    let i = event.currentTarget.getAttribute('data-index');
    let newUsers = users.slice(0, i);
    if (i < users.length - 1) {
      newUsers = newUsers.concat(users.slice(+i + 1, users.length));
    }
    setUsers(newUsers);
  }

  function onUsersUsernameChange(event) {
    let newUsers = [...users];
    newUsers[event.target.getAttribute('data-index')].username = event.target.value;
    setUsers(newUsers);
  }

  function onSubmit(e) {
    e.preventDefault();
    const data = {
      name: name,
      users: users
    };
    const options = {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    };
    fetch('/jokerpublika/group', options)
        .then(result => {
          if (result.ok) {
            fetchGroupData();
            history.push('/');
          } else {
            setError(true);
            scrollToAlert();
          }
        });
  }

  return (
      <PageContentWrapper title={"Create group"}>
        <ResponsiveForm onSubmit={onSubmit}>

          <div ref={alertRef}>
            {error
            && <CustomAlert color={"danger"} header={"Error while submitting group!"}
                            body={"Make sure all added users exist and are not anonymous 🙄"}/>}
          </div>

          <Row>
            <FormGroup className="col">
              <FormLabel className="text-lg text-violet" htmlFor="name">Group name</FormLabel>
              <InputGroup>
                <input type="text" className="form-control" id="name"
                       name="name" required maxLength="128"
                       placeholder="What should the group be called?"
                       onChange={onNameChange}/>
              </InputGroup>
            </FormGroup>
          </Row>

          <h4 className="mt-3 mb-2">
            <span className="text-lavender">Users</span>
          </h4>

          {users.map((user, i) =>
              <Row key={i}>
                <FormGroup className="col">
                  <InputGroup>
                    <input className="form-control" id={i} data-index={i}
                           name={'username' + i} required maxLength="32"
                           placeholder="What's your friend's username?"
                           onChange={onUsersUsernameChange}
                           value={user.username}/>
                  </InputGroup>
                </FormGroup>
                {i !== 0
                && <Col className="col-auto">
                  <Button className="btn-block btn-danger" data-index={i} onClick={removeMemberField}>
                    <FontAwesomeIcon icon="user-minus"/>
                  </Button>
                </Col>}
              </Row>
          )}

          <Row>
            <Col>
              <Button className="ml-auto btn-success" onClick={addUserField}>
                <FontAwesomeIcon className="fa-fw mr-2" icon="user-plus"/>
                Add user
              </Button>
            </Col>
          </Row>

          <Row className="my-4">
            <FormGroup className="col-8 col-sm-7 col-md-6 col-lg-5 col-xl-4 mx-auto">
              <Button className="btn-block btn-info btn-lg" type="submit">Submit group!</Button>
            </FormGroup>
          </Row>

        </ResponsiveForm>
      </PageContentWrapper>
  )

}

export default CreateGroup;
