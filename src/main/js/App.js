import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Register from "./components/Register";
import Login from "./components/Login";
import Home from "./components/Home";
import LoginHome from "./components/LoginHome";
import MainHeader from "./components/MainHeader";
import LoginHeader from "./components/LoginHeader";
import Settings from "./components/Settings";
import NewPoll from "./components/NewPoll";
import CurrentPolls from "./components/CurrentPolls";
import Footer from "./components/Footer";
import CreateGroup from "./components/CreateGroup";
import NewResults from "./components/NewResults";
import MyActivePolls from "./components/MyActivePolls";
import FollowedPolls from "./components/FollowedPolls";
import GroupPolls from "./components/GroupPolls";
import Profile from "./components/Profile";
import AnswerPolls from "./components/AnswerPolls";
import SeparateBanComponent from "./components/SeparateBanComponent";
import ApplicationSettings from "./components/ApplicationSettings";

export const GroupContext = React.createContext([]);
export const UserContext = React.createContext([]);

function App() {

  const [currentUser, setCurrentUser] = React.useState(null);
  const [groups, setGroups] = React.useState([]);
  const [publicUsers, setPublicUsers] = React.useState([]);
  const [anonymousUsers, setAnonymousUsers] = React.useState([]);

  React.useEffect(() => {
    fetchUserData();
  }, []);

  React.useEffect(() => {
    if (currentUser) {
      fetchGroupData();
      fetchAllPublicUsers();
      if (currentUser.roles.includes("ADMIN")) {
        fetchAllAnonymousUsers();
      }
    }
  }, [currentUser]);

  function login() {
    fetchUserData();
  }

  function fetchUserData() {
    fetch('/jokerpublika/user/current')
        .then(data => data.json())
        .then(data => {
          localStorage.setItem("currentUser", JSON.stringify(data));
          console.log("Local storage set");
          console.log(data);
          setCurrentUser(data);
        }, () => {
          console.log("Failed to fetch current user");
          setCurrentUser(null);
          localStorage.clear();
          console.log("Local storage cleared");
        });
  }

  function fetchGroupData() {
    fetch('/jokerpublika/group/user?userId=' + currentUser.id)
        .then(data => data.json())
        .then(data => {
          setGroups(data);
        }, () => {
          console.log("Failed to fetch groups");
        });
  }

  function fetchAllPublicUsers() {
    fetch('/jokerpublika/user/all-public')
        .then(data => data.json())
        .then(data => {
          setPublicUsers(data);
        }, () => {
          console.log("Failed to fetch all public users");
        });
  }

  function fetchAllAnonymousUsers() {
    fetch('/jokerpublika/user/all-anonymous')
        .then(data => data.json())
        .then(data => {
          setAnonymousUsers(data);
        }, () => {
          console.log("Failed to fetch all anonymous users");
        });
  }

  function logout() {
    fetch("/jokerpublika/logout")
        .then(() => {
          setCurrentUser(null);
        })
        .then(() => {
          localStorage.clear();
          console.log("Local storage cleared");
        });
  }

  function getHomePage() {
    return currentUser ? <Home/> : <LoginHome/>;
  }

  return (
      <GroupContext.Provider value={{groups, fetchGroupData}}>
        <UserContext.Provider value={{publicUsers, anonymousUsers, fetchAllPublicUsers, fetchAllAnonymousUsers}}>
          <div className="content-wrapper d-flex flex-column vh-100">

            <div className="content">
              <BrowserRouter basename="/jokerpublika">
                {!currentUser && <LoginHeader key={{id: "LHeader"}}/>}
                {currentUser && <MainHeader key={{id: "MHeader"}} onLogout={logout}/>}

                <Switch>
                  <Route path='/' exact render={() => getHomePage()}/>
                  <Route path='/login' exact render={() => <Login onLogin={login}/>}/>
                  <Route path='/register' exact render={() => <Register onRegister={login}/>}/>
                  {currentUser && <Route path='/settings' exact render={() =>
                      <Settings fetchUserData={fetchUserData}/>}/>}
                  {currentUser && currentUser.roles.includes("ADMIN")
                  && <Route path='/ban' exact render={() => <SeparateBanComponent/>}/>}
                  {currentUser && currentUser.roles.includes("ADMIN")
                  && <Route path='/admin' exact render={() => <ApplicationSettings/>}/>}
                  {currentUser && <Route path='/poll/new' exact render={() => <NewPoll/>}/>}
                  {currentUser && <Route path='/poll/answer-polls' exact render={() => <AnswerPolls/>}/>}
                  {currentUser && <Route path='/poll/current' exact render={() => <CurrentPolls/>}/>}
                  {currentUser && <Route path='/poll/results' exact render={() => <NewResults/>}/>}
                  {currentUser && <Route path='/poll/active' exact render={() => <MyActivePolls/>}/>}
                  {currentUser && <Route path='/poll/followed' exact render={() => <FollowedPolls/>}/>}
                  {currentUser && <Route path='/group/new' exact render={() => <CreateGroup/>}/>}
                  {currentUser && groups && groups.map(group =>
                      <Route key={group.id} path={'/poll/group/' + group.id} exact
                             render={() => <GroupPolls group={group}/>}/>)}
                  {currentUser && publicUsers && publicUsers.map(user =>
                      <Route key={user.id} path={'/user/' + user.username} exact render={() => <Profile user={user}/>}/>
                  )}
                  {currentUser && currentUser.anonymous && !currentUser.roles.includes("ADMIN")
                  && <Route key={currentUser.id} path={'/user/' + currentUser.username} exact render={() =>
                      <Profile user={currentUser}/>}/>}
                  {currentUser && currentUser.roles.includes("ADMIN") && anonymousUsers.map(user =>
                      <Route key={user.id} path={'/user/' + user.username} exact render={() => <Profile user={user}/>}/>
                  )}
                </Switch>
              </BrowserRouter>
            </div>

            <Footer/>

          </div>
        </UserContext.Provider>
      </GroupContext.Provider>
  );
}

export default App;
