import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "../resources/theme/css/sb-admin-2.css";
import "../resources/static/css/app-theme.css";
import {library} from "@fortawesome/fontawesome-svg-core";
import {faCheckCircle, faClock, faListAlt, faStar} from "@fortawesome/free-regular-svg-icons";
import {
  faAngleDown,
  faAngleRight,
  faBan,
  faBars,
  faCog,
  faCommentDots,
  faCrown,
  faExclamation,
  faFileImage,
  faForward,
  faHourglassHalf,
  faPencilAlt,
  faPlus,
  faQuestion,
  faSignOutAlt,
  faTasks,
  faTrashAlt,
  faUser,
  faUserFriends,
  faUserMinus,
  faUserPlus,
  faUsers
} from "@fortawesome/free-solid-svg-icons";

library.add(faBars, faSignOutAlt, faCog, faUser, faUsers, faQuestion, faCommentDots, faUserFriends, faExclamation,
    faClock, faStar, faListAlt, faAngleRight, faAngleDown, faTasks, faPencilAlt, faFileImage, faPlus, faCheckCircle,
    faTrashAlt, faForward, faUserPlus, faUserMinus, faBan, faCrown, faHourglassHalf);

ReactDOM.render(<App/>, document.getElementById("react"));
