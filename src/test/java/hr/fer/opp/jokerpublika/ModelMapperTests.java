package hr.fer.opp.jokerpublika;

import hr.fer.opp.jokerpublika.model.FriendGroup;
import hr.fer.opp.jokerpublika.model.User;
import hr.fer.opp.jokerpublika.services.UserService;
import hr.fer.opp.jokerpublika.web.dto.FriendGroupDto;
import hr.fer.opp.jokerpublika.web.dto.UserDto;
import hr.fer.opp.jokerpublika.web.dto.UserRegistrationDto;
import hr.fer.opp.jokerpublika.web.exceptions.AnonymousUserGroupException;
import hr.fer.opp.jokerpublika.web.exceptions.PasswordConfirmationMismatchException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;

import javax.xml.bind.SchemaOutputResolver;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

@SpringBootTest
class ModelMapperTests {

  private UserDto anon;
  private UserDto notAnon;
  private UserRegistrationDto wrongConfirmPassword;

  @Autowired
  private ModelMapper mapper;

  @Autowired
  private UserService userService;

  @BeforeEach
  void setup() {
    notAnon = new UserDto();
    notAnon.setUsername("fmajetic");

    anon = new UserDto();
    anon.setUsername("dan");

    wrongConfirmPassword = new UserRegistrationDto();
    wrongConfirmPassword.setUsername("A");
    wrongConfirmPassword.setEmail("A@BBB.AA");
    wrongConfirmPassword.setPassword("OneOneOne");
    wrongConfirmPassword.setConfirmPassword("TwoTwoTwo");
    wrongConfirmPassword.setAnonymous(false);
  }

  @Test
  void passwordMismatchThrowsException() {
    assertThatThrownBy(() -> mapper.map(wrongConfirmPassword, User.class))
        .hasCauseExactlyInstanceOf(PasswordConfirmationMismatchException.class);
  }

  @Test
  @WithUserDetails(value = "fmajetic") // Not anonymous, in DatabaseLoader
  void anonymousUserInGroup() {
    FriendGroupDto anonMember = new FriendGroupDto();
    anonMember.setName("My second group");
    anonMember.setOwner(notAnon);
    anonMember.setUsers(List.of(anon));

    assertThatThrownBy(() -> mapper.map(anonMember, FriendGroup.class))
        .hasCauseExactlyInstanceOf(AnonymousUserGroupException.class);
  }

  @Test
  @WithUserDetails(value = "dan") // Is anonymous, in DatabaseLoader
  void anonymousGroupOwner() {
    FriendGroupDto anonOwner = new FriendGroupDto();
    anonOwner.setName("My second group");
    anonOwner.setOwner(anon);
    anonOwner.setUsers(List.of(notAnon));

    assertThatThrownBy(() -> mapper.map(anonOwner, FriendGroup.class))
        .hasCauseExactlyInstanceOf(AnonymousUserGroupException.class);
  }
}
