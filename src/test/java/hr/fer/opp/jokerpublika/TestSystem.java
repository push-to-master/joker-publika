package hr.fer.opp.jokerpublika;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("DuplicatedCode")
public class TestSystem {

  public static final String settingsByCss = "a:nth-of-type(2) > span";

  public static final String deactivateByCss = ".btn.btn-block.btn-google.btn-lg";

  public static final String confirmDeactivateByCss = " .btn.btn-danger.m-1";

  public static final int DEFAULT_TIMEOUT_TIME = 10;

  public static final String postPollByXpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[5]/div[1]/button[1]";

  public static final String myActivePollsByXpath = "//span[contains(text(),'My Active Polls')]";

  public static final String firstQuestionByCss = "div:nth-of-type(1) > .card.shadow > .card-body.px-2.px-sm-4.py-2 > " +
                                                  ".card-text.mt-1.text-lg.text-violet";

  public static final String myGroupsByXpath = "//span[contains(text(),'My Groups')]";

  public static final String addUserByCss = ".btn.btn-primary.btn-success.ml-auto";

  public static final String submitGroupByCss = ".btn.btn-block.btn-info.btn-lg.btn-primary";

  public static String TEST_EMAIL;

  public static String TEST_USERNAME;

  public static String TEST_PASSWORD;

  public final String dropdownWithSettings = "/html[1]/body[1]/div[1]/div[1]/div[1]/nav[1]/div[3]/div[1]/button[1]/span[1]";

  public final String sideBarByXpath = "//div[@id='react']//nav/div[@class='d-flex']";

  public WebDriver driver;

  public FirefoxOptions fo = new FirefoxOptions();

  @BeforeClass
  public void setup() {
    WebDriverManager.firefoxdriver().forceCache().setup();
    fo.addArguments("--headless");
    this.driver = new FirefoxDriver(fo);
    GenerateRandomUser u = new GenerateRandomUser();
    this.registerMe(u);
    System.out.println(TEST_USERNAME+" "+TEST_PASSWORD);
    TEST_USERNAME = u.username;
    TEST_EMAIL = u.email;
    TEST_PASSWORD = u.password;
    this.driver.quit();
  }

  private void registerMe(GenerateRandomUser user) {
    System.out.println("user "+user.username+"\nemail"+user.email+"\npass"+user.password);
    this.registerMe(user.username, user.email, user.password);
  }

  //pretpostavka => nalazimo se na stranici register
  private void registerMe(String username, String email, String pass) {
    this.driver.get("https://jokerpublika.herokuapp.com/jokerpublika/register");
    this.driver.findElement(By.id("username")).sendKeys(username);
    this.driver.findElement(By.id("email")).sendKeys(email);
    this.driver.findElement(By.id("password")).sendKeys(pass);
    this.driver.findElement(By.id("confirm-password")).sendKeys(pass);
    this.driver.findElement(By.xpath("//div[@id='react']//form/button[@type='submit']/span[@class='MuiButton-label']")).click();
  }

  @BeforeMethod
  public void beforeEach() {
    this.driver = new FirefoxDriver(fo);
    this.driver.manage().timeouts().implicitlyWait(DEFAULT_TIMEOUT_TIME, TimeUnit.SECONDS);
  }

  /**
   * Metoda provjerava funkciju prijave na stranicu
   */
  @Test
  public void testLogin() {
    this.logMeIn(TEST_USERNAME, TEST_PASSWORD);
    Assert.assertEquals(this.driver.findElement(By.xpath(this.dropdownWithSettings)).getText(), TEST_USERNAME);
  }

  /**
   * Pomoćna metoda za prijavu korisnika s danim korisničkim imenom i lozinkom
   * @param username korisničko ime
   * @param password zaporka
   */
  private void logMeIn(String username, String password) {
    this.driver.get("https://jokerpublika.herokuapp.com/jokerpublika/login");
    this.driver.findElement(By.id("username")).sendKeys(username);
    this.driver.findElement(By.id("password")).sendKeys(password);
    this.driver.findElement(By.xpath("//div[@id='react']//form/button[@type='submit']/span[@class='MuiButton-label']")).click();
  }

  /**
   * Provjera je li se moguće ulogirati s deaktiviranim računom
   */
  @Test
  public void deactivate() {
    GenerateRandomUser newRandomUser = new GenerateRandomUser();
    String randomUsername = newRandomUser.username;
    String email = newRandomUser.email;
    String randomPassword = newRandomUser.password;
    System.out.println("user: " + randomUsername + "\nemail: " + email + "\nrandom password: " + randomPassword);
    //REGISTRACIJA
    this.registerMe(randomUsername, email, randomPassword);
    //LOGIN automatski nakon registracije
    //DEAKTIVACIJA
    this.driver.findElement(By.xpath(this.dropdownWithSettings)).click();
    this.driver.findElement(By.cssSelector(settingsByCss)).click();
    this.driver.findElement(By.cssSelector(deactivateByCss)).click();
    this.driver.findElement(By.cssSelector(confirmDeactivateByCss)).click();
    this.logMeIn(randomUsername, randomPassword);

    //POJAVILA SE GRESKA
    Assert.assertTrue(this.driver.findElement(By.cssSelector("form > div[role='alert']")).isDisplayed());
  }

  @Test
  public void registerAndLogin() {
    GenerateRandomUser user = new GenerateRandomUser();
    String randomUsername = user.username;
    String email = user.email;
    String randomPassword = user.password;
    System.out.println("user: " + randomUsername + " email: " + email + " random password: " + randomPassword);
    //REGISTRACIJA
    this.registerMe(randomUsername, email, randomPassword);

    //LOGIN ; brisanje kolačića jer nas stranica automatski ulogira
    this.driver.manage().deleteAllCookies();
    this.driver.navigate().refresh();
    this.logMeIn(randomUsername, randomPassword);

    Assert.assertEquals(this.driver.findElement(By.xpath(this.dropdownWithSettings)).getText(),
        randomUsername);
  }

  /**
   * Provjera nalazi se zapravo pitanje na stranici dostupnih pitanja kada ju mi objavimo
   */
  @Test
  public void postPoll() {
    this.logMeIn(TEST_USERNAME, TEST_PASSWORD);
    //stisni sidebar
    this.driver.findElement(By.xpath(this.sideBarByXpath)).click();
    this.driver.findElement(By.linkText("New Poll")).click();
    int randomInt = new Random().nextInt();
    System.out.println(randomInt);
    this.driver.findElement(By.xpath("/html//textarea[@id='question']")).sendKeys("TEST_QUESTION?" + randomInt);
    this.driver.findElement(By.cssSelector("#answer1")).sendKeys("TEST_ANSWER_1");
    this.driver.findElement(By.cssSelector("#answer2")).sendKeys("TEST_ANSWER_2");
    this.driver.findElement(By.xpath(postPollByXpath)).click();
    this.driver.findElement(By.xpath(this.sideBarByXpath)).click();
    this.driver.findElement(By.xpath(myActivePollsByXpath)).click();
    WebElement firstQuestion = this.driver.findElement(By.cssSelector(firstQuestionByCss));
    Assert.assertEquals("TEST_QUESTION?" + randomInt, firstQuestion.getText());
  }

  @Test
  public void createGroup() {

    GenerateRandomUser user1 = new GenerateRandomUser();
    GenerateRandomUser user2 = new GenerateRandomUser();
    System.out.println(user1.username);
    System.out.println(user2.username);
    this.registerMe(user1);
    this.driver.manage().deleteAllCookies();
    this.registerMe(user2);
    this.driver.manage().deleteAllCookies();

    this.logMeIn(TEST_USERNAME, TEST_PASSWORD);
    this.driver.findElement(By.xpath(this.sideBarByXpath)).click();
    this.driver.findElement(By.xpath(myGroupsByXpath)).click();
    this.driver.findElement(By.linkText("Create Group")).click();
    String imeGrupe = "naša grupica" + new Random().nextInt();
    System.out.println(imeGrupe);
    this.driver.findElement(By.cssSelector("input#name")).sendKeys(imeGrupe);
    this.driver.findElement(By.cssSelector("input[name='username0']")).sendKeys(user1.username);
    this.driver.findElement(By.cssSelector(addUserByCss)).click(); //ADD USER
    this.driver.findElement(By.cssSelector("input[name='username1']")).sendKeys(user2.username);
    this.driver.findElement(By.cssSelector(submitGroupByCss)).click(); //SUBMIT GROUP
    this.driver.findElement(By.xpath(this.sideBarByXpath)).click();

    List<WebElement> element = this.driver.findElements(By.cssSelector("a.sidebar-item.text-wrap.text-truncate"));

    Assert.assertTrue(element.stream().anyMatch(webElement -> webElement.getText().equals(imeGrupe)));
  }

  /**
   * Nakon kraja svakog testa zatvara web preglednik.
   */
  @AfterMethod
  public void endIt() {
    this.driver.quit();
  }

  private static class GenerateRandomUser {
    private String username;

    private String password;

    private String email;

    public GenerateRandomUser() {
      this.username = "user" + (long) (Math.random() * 1000000000000000000L);
      this.email = this.username + "@email.com";
      this.password = UUID.randomUUID().toString();
    }
  }
}
