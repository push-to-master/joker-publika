package hr.fer.opp.jokerpublika;

import hr.fer.opp.jokerpublika.model.User;
import hr.fer.opp.jokerpublika.model.UserRoleEnum;
import hr.fer.opp.jokerpublika.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.TestPropertySource;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@TestPropertySource(properties = {"spring.jpa.hibernate.ddl-auto=create-drop"})
class PersistenceTests {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private DataSource dataSource;

  @Autowired
  private JdbcTemplate jdbcTemplate;

  @Autowired
  private EntityManager entityManager;

  @Test
  void springAutowireFunctionality() {
    assertThat(dataSource).isNotNull();
    assertThat(jdbcTemplate).isNotNull();
    assertThat(entityManager).isNotNull();
    assertThat(userRepository).isNotNull();
  }

  @Test
  void hibernateDDLSettings1() {
    assertThat(userRepository.findByUsername("user2")).isNull();
    userRepository.save(new User("user1", "mail1@mail.com", "0xff", false, List.of(UserRoleEnum.USER)));
  }

  @Test
  void hibernateDDLSettings2() {
    assertThat(userRepository.findByUsername("user1")).isNull();
    userRepository.save(new User("user2", "mail2@mail.com", "0xff", false, List.of(UserRoleEnum.USER)));
  }

  @Test
  void findSavedByUsername() {
    userRepository.save(new User(
        "zaphod368",
        "myemail@mail.org",
        "0xff",
        false,
        List.of(UserRoleEnum.USER)
    ));
    assertThat(userRepository.findByUsername("zaphod368")).isNotNull();
  }

}
