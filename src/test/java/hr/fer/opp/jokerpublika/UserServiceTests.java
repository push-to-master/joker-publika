package hr.fer.opp.jokerpublika;

import hr.fer.opp.jokerpublika.model.User;
import hr.fer.opp.jokerpublika.model.UserRoleEnum;
import hr.fer.opp.jokerpublika.repositories.UserRepository;
import hr.fer.opp.jokerpublika.services.impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.springframework.test.util.AssertionErrors.assertEquals;

@ExtendWith(MockitoExtension.class)
class UserServiceTests {

  @Mock
  private UserRepository userRepository;

  @InjectMocks
  private UserServiceImpl userService;

  @BeforeEach
  public void setup() {
    User notAnon = new User("fmajetic", "fmajetic@fer.hr", "", false, List.of(UserRoleEnum.USER));
    notAnon.setId(1L);

    User anon = new User("iskoric", "iskoric@fer.hr", "", true, List.of(UserRoleEnum.USER));
    anon.setId(2L);

    User admin = new User("admin", "admin@admin.hr", "", false, List.of(UserRoleEnum.ADMIN));
    admin.setId(3L);

    Mockito.lenient().when(userRepository.findByUsername(anon.getUsername())).thenReturn(anon);
    Mockito.lenient().when(userRepository.findByUsername(notAnon.getUsername())).thenReturn(notAnon);
    Mockito.lenient().when(userRepository.findByUsername(admin.getUsername())).thenReturn(admin);
  }

  @Test
  void cantChangeBackToAnonymous() {
    User u = userService.getByUsername("iskoric");

    userService.changeAnonymity(u, false);
    userService.changeAnonymity(u, true);

    assertEquals("User should not be anonymous", u.getAnonymous(), false);
  }

}
